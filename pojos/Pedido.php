<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pedidos
 *
 * @author guijarro
 */
class Pedido {
	//put your code here
	private $id;
	private $idPedido;
	private $idEmpleadoQueEmpaqueta;
	private $idEmpresaTransporte;
	private $fechaPedido;
	private $fechaEnvio;
	private $fechaEntrega;
	private $facturado;
	private $idFactura;
	private $fechaFactura;
	private $pagado;
	private $fechaPago;
	private $metodoPago;
	private $idCliente;
	private $activo;
	private $direccion;

	function __construct($id, $idPedido, $idEmpleadoQueEmpaqueta, $idEmpresaTransporte, $fechaPedido, $fechaEnvio, $fechaEntrega, $facturado, $idFactura, $fechaFactura, $pagado, $fechaPago, $metodoPago, $idCliente, $activo, $direccion) {
		$this->id = $id;
		$this->idPedido = $idPedido;
		$this->idEmpleadoQueEmpaqueta = $idEmpleadoQueEmpaqueta;
		$this->idEmpresaTransporte = $idEmpresaTransporte;
		$this->fechaPedido = $fechaPedido;
		$this->fechaEnvio = $fechaEnvio;
		$this->fechaEntrega = $fechaEntrega;
		$this->facturado = $facturado;
		$this->idFactura = $idFactura;
		$this->fechaFactura = $fechaFactura;
		$this->pagado = $pagado;
		$this->fechaPago = $fechaPago;
		$this->metodoPago = $metodoPago;
		$this->idCliente = $idCliente;
		$this->activo = $activo;
		$this->direccion = $direccion;
	}
	function getId() {
		return $this->id;
	}

	function getIdPedido() {
		return $this->idPedido;
	}

	function getIdEmpleadoQueEmpaqueta() {
		return $this->idEmpleadoQueEmpaqueta;
	}

	function getIdEmpresaTransporte() {
		return $this->idEmpresaTransporte;
	}

	function getFechaPedido() {
		return $this->fechaPedido;
	}

	function getFechaEnvio() {
		return $this->fechaEnvio;
	}

	function getFechaEntrega() {
		return $this->fechaEntrega;
	}

	function getFacturado() {
		return $this->facturado;
	}

	function getIdFactura() {
		return $this->idFactura;
	}

	function getFechaFactura() {
		return $this->fechaFactura;
	}

	function getPagado() {
		return $this->pagado;
	}

	function getFechaPago() {
		return $this->fechaPago;
	}

	function getMetodoPago() {
		return $this->metodoPago;
	}

	function getIdCliente() {
		return $this->idCliente;
	}

	function getActivo() {
		return $this->activo;
	}

	function setId($id) {
		$this->id = $id;
	}

	function setIdPedido($idPedido) {
		$this->idPedido = $idPedido;
	}

	function setIdEmpleadoQueEmpaqueta($idEmpleadoQueEmpaqueta) {
		$this->idEmpleadoQueEmpaqueta = $idEmpleadoQueEmpaqueta;
	}

	function setIdEmpresaTransporte($idEmpresaTransporte) {
		$this->idEmpresaTransporte = $idEmpresaTransporte;
	}

	function setFechaPedido($fechaPedido) {
		$this->fechaPedido = $fechaPedido;
	}

	function setFechaEnvio($fechaEnvio) {
		$this->fechaEnvio = $fechaEnvio;
	}

	function setFechaEntrega($fechaEntrega) {
		$this->fechaEntrega = $fechaEntrega;
	}

	function setFacturado($facturado) {
		$this->facturado = $facturado;
	}

	function setIdFactura($idFactura) {
		$this->idFactura = $idFactura;
	}

	function setFechaFactura($fechaFactura) {
		$this->fechaFactura = $fechaFactura;
	}

	function setPagado($pagado) {
		$this->pagado = $pagado;
	}

	function setFechaPago($fechaPago) {
		$this->fechaPago = $fechaPago;
	}

	function setMetorodoPago($metorodoPago) {
		$this->metodoPago = $metorodoPago;
	}

	function setIdCliente($idCliente) {
		$this->idCliente = $idCliente;
	}

	function setActivo($activo) {
		$this->activo = $activo;
	}

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }


}
