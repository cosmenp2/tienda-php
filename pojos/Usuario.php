<?php

class Usuario
{
    private $id;
    private $idUsuario;
    private $idRol;
    private $login;
    private $password;
    private $activo;

    /**
     * Usuario constructor.
     * @param $id
     * @param $idUsuario
     * @param $idRol
     * @param $login
     * @param $password
     * @param $activo
     */
    public function __construct($id, $idUsuario, $idRol, $login, $password, $activo)
    {
        $this->id = $id;
        $this->idUsuario = $idUsuario;
        $this->idRol = $idRol;
        $this->login = $login;
        $this->password = $password;
        $this->activo = $activo;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * @param mixed $idUsuario
     */
    public function setIdUsuario($idUsuario)
    {
        $this->idUsuario = $idUsuario;
    }

    /**
     * @return mixed
     */
    public function getIdRol()
    {
        return $this->idRol;
    }

    /**
     * @param mixed $idRol
     */
    public function setIdRol($idRol)
    {
        $this->idRol = $idRol;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * @param mixed $activo
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    }




}