<?php

class Deseo
{
    private $id;
    private $idCliente;
    private $idProducto;
    private $fecha;
    private $activo;

    /**
     * Deseo constructor.
     * @param $id
     * @param $idCliente
     * @param $idProducto
     * @param $fecha
     */
    public function __construct($id, $idCliente, $idProducto, $fecha, $activo)
    {
        $this->id = $id;
        $this->idCliente = $idCliente;
        $this->idProducto = $idProducto;
        $this->fecha = $fecha;
        $this->activo = $activo;

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdCliente()
    {
        return $this->idCliente;
    }

    /**
     * @param mixed $idCliente
     */
    public function setIdCliente($idCliente)
    {
        $this->idCliente = $idCliente;
    }

    /**
     * @return mixed
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * @param mixed $idProducto
     */
    public function setIdProducto($idProducto)
    {
        $this->idProducto = $idProducto;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * @param mixed $activo
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    }



}