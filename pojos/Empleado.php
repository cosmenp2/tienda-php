<?php
/**
 *
 */
class Empleado {
	private $id;
	private $idEmpleado;
	private $idUsuario;
	private $nif;
	private $nombre;
	private $numcta;
	private $movil;
	private $rutaFoto;
	private $activo;
	private $apellido2;
	private $apellido1;

	function __construct($id, $idEmpleado, $idUsuario, $nif, $nombre, $apellido1, $apellido2, $numcta, $movil, $rutaFoto, $activo) {

		$this->id = $id;
		$this->idEmpleado = $idEmpleado;
		$this->idUsuario = $idUsuario;
		$this->nif = $nif;
		$this->nombre = $nombre;
		$this->apellido1 = $apellido1;
		$this->apellido2 = $apellido2;
		$this->numcta = $numcta;
		$this->movil = $movil;
		$this->rutaFoto = $rutaFoto;
		$this->activo = $activo;
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 *
	 * @return self
	 */
	public function setId($id) {
		$this->id = $id;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getIdEmpleado() {
		return $this->idEmpleado;
	}

	/**
	 * @param mixed $idEmpleado
	 *
	 * @return self
	 */
	public function setIdEmpleado($idEmpleado) {
		$this->idEmpleado = $idEmpleado;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getIdUsuario() {
		return $this->idUsuario;
	}

	/**
	 * @param mixed $idUsuario
	 *
	 * @return self
	 */
	public function setIdUsuario($idUsuario) {
		$this->idUsuario = $idUsuario;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getNif() {
		return $this->nif;
	}

	/**
	 * @param mixed $nif
	 *
	 * @return self
	 */
	public function setNif($nif) {
		$this->nif = $nif;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getNombre() {
		return $this->nombre;
	}

	/**
	 * @param mixed $nombre
	 *
	 * @return self
	 */
	public function setNombre($nombre) {
		$this->nombre = $nombre;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getNumcta() {
		return $this->numcta;
	}

	/**
	 * @param mixed $numcta
	 *
	 * @return self
	 */
	public function setNumcta($numcta) {
		$this->numcta = $numcta;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getMovil() {
		return $this->movil;
	}

	/**
	 * @param mixed $movil
	 *
	 * @return self
	 */
	public function setMovil($movil) {
		$this->movil = $movil;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getRutaFoto() {
		return $this->rutaFoto;
	}

	/**
	 * @param mixed $rutaFoto
	 *
	 * @return self
	 */
	public function setRutaFoto($rutaFoto) {
		$this->rutaFoto = $rutaFoto;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getActivo() {
		return $this->activo;
	}

	/**
	 * @param mixed $activo
	 *
	 * @return self
	 */
	public function setActivo($activo) {
		$this->activo = $activo;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getApellido2() {
		return $this->apellido2;
	}

	/**
	 * @param mixed $apellido2
	 *
	 * @return self
	 */
	public function setApellido2($apellido2) {
		$this->apellido2 = $apellido2;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getApellido1() {
		return $this->apellido1;
	}

	/**
	 * @param mixed $apellido1
	 *
	 * @return self
	 */
	public function setApellido1($apellido1) {
		$this->apellido1 = $apellido1;

		return $this;
	}
}
?>