<?php
require_once('../../rutas.php');
require_once "../../" . PERSISTENCIA . "Productos.php";
require_once "../../" . PERSISTENCIA . "Pedidos.php";
require_once "../../" . PERSISTENCIA . "Usuarios.php";
require_once "../../" . PERSISTENCIA . "LineasPedidos.php";
require_once "../../" . PERSISTENCIA . "FamiliasProductos.php";
require_once "../../" . PERSISTENCIA . "DireccionesEmpleado.php";
require_once "../../" . PERSISTENCIA . "DireccionesCliente.php";
require_once "../../" . PERSISTENCIA . "Clientes.php";
require_once "../../" . PERSISTENCIA . "Deseos.php";
require_once "../../" . PERSISTENCIA . "Empleados.php";
require_once "../../" . PERSISTENCIA . "Roles.php";
require_once "../../" . POJOS . "Usuario.php";
require_once "../../" . POJOS . "Producto.php";
require_once "../../" . POJOS . "Pedido.php";
require_once "../../" . POJOS . "LineaPedido.php";
require_once "../../" . POJOS . "FamiliaProducto.php";
require_once "../../" . POJOS . "DireccionEmpleado.php";
require_once "../../" . POJOS . "DireccionCliente.php";
require_once "../../" . POJOS . "Cliente.php";
require_once "../../" . POJOS . "Deseo.php";
require_once "../../" . POJOS . "Empleado.php";
require_once "../../" . POJOS . "Rol.php";

function initSesion()
{
    session_start();
    $cerrada = false;
    // No permitir usar un ID de sesión demasiado antiguo
    if (!empty($_SESSION['deleted_time']) && $_SESSION['deleted_time'] < time() - 7200) {
        setcookie("idSesion", "", time() - 3600);
        session_destroy();
        $cerrada = true;
//        session_start();
    }
    if (empty($_SESSION['deleted_time'])) {
        setcookie("idSesion", "", time() - 3600);
        session_destroy();
        $cerrada = true;
//        session_start();
    }
    return $cerrada;
}

ini_set('session.use_strict_mode', 1);
if (isset($_COOKIE['idSesion'])) {
    session_id($_COOKIE['idSesion']);
    if (!initSesion()) {
        $_SESSION['deleted_time'] = time();
    }
}

/** @var Usuarios $tUsuario */
$tUsuario = Usuarios::singletonUsuarios();
$usuario = null;
$legitima = false;

if (isset($_SESSION['idAdmin'])) {
    $usuario = $tUsuario->buscaUsuario($_SESSION['idAdmin']);
    if (is_null($usuario)) {
        $_SESSION['idAdmin'] = "";
    } else {
        $_SESSION['time'] = time();
        $nombre = $usuario->getLogin();
        $legitima = true;
    }
}
?>
<!doctype html>
<html lang="es">
<head> <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Cosme José Nieto Pérez">


    <!-- Bootstrap CSS en la web-->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


    <link rel="stylesheet" href="estilos.css">

    <!--  <link href="css/bootstrap.min.css" rel="stylesheet">
  -->
    <title> ERP IES Castelar</title>
</head>
<body>

<?php
if ($legitima) {
    echo '
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href=' . LOGO . ' > <img src=../../' . LOGO . ' width="30" height="30"
                                                                                                 alt="">
    </a>

    <a class="navbar-brand pb-2" href="IndexAdmin.php">IES Castlelar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarClientes" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false"> Clientes </a>
                <ul class="dropdown-menu" aria-labelledby="navbarClientes">
                    <li><a class="dropdown-item" href="IndexAdmin.php?principal=formAltaCliente.php">Añadir</a></li>
                    <li><a class="dropdown-item" href="IndexAdmin.php?principal=editarClientes.php">Editar/Modificar
                            datos y direcciones</a></li>
                    <li><a class="dropdown-item" href="IndexAdmin.php?principal=filtroCliente.php">Filtrar</a></li>
                    <li><a class="dropdown-item" href="IndexAdmin.php?principal=listadoPedidosClientes.php">Listado
                            Pedidos/Facturas de un cliente</a></li>

                    <li><a class="dropdown-item"
                           href="#">Gráficos
                            de compras entre fechas</a></li>


                </ul>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarEmpleados" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false"> Empleados </a>
                <ul class="dropdown-menu" aria-labelledby="navbarEmpleados">
                    <li><a class="dropdown-item"
                           href="IndexAdmin.php?principal=formAltaEmpleado.php">Añadir</a></li>
                    <li><a class="dropdown-item"
                           href="IndexAdmin.php?principal=editarEmpleados.php">Editar/Modificar
                            datos y direcciones</a></li>
                    <li><a class="dropdown-item" href="IndexAdmin.php?principal=filtroEmpleados.php">Filtrar</a></li>
                    <li><a class="dropdown-item" href="IndexAdmin.php?principal=listadoPedidosEmpleados.php">Listado
                            Pedidos/Facturas empaquetadas por un empleado</a></li>

                    <li><a class="dropdown-item"
                           href="#">Gráficos
                            de actividad entre fechas</a>

                    </li>
                </ul>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarFamilias" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false"> Familias </a>
                <ul class="dropdown-menu" aria-labelledby="navbarFamilias">
                    <li><a class="dropdown-item"
                           href="IndexAdmin.php?principal=formAltaFamilia.php">Añadir</a></li>
                    <li><a class="dropdown-item"
                           href="IndexAdmin.php?principal=editarFamilias.php">Editar/Modificar</a>
                    </li>
                    <li><a class="dropdown-item" href="IndexAdmin.php?principal=filtroFamilias.php">Filtros
                            de Familias</a></li>
                    <li><a class="dropdown-item" href="IndexAdmin.php?principal=listadoPedidosFamilias.php">Listado
                            Pedidos/Facturas de una familia</a></li>

                    <li><a class="dropdown-item"
                           href="#">Gráficos
                            de compras por familias entre fechas</a></li>


                </ul>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarProductos" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false"> Productos </a>
                <ul class="dropdown-menu" aria-labelledby="navbarProductos">
                    <li><a class="dropdown-item"
                           href="IndexAdmin.php?principal=formAltaProducto.php">Añadir</a></li>
                    <li><a class="dropdown-item"
                           href="IndexAdmin.php?principal=editarProductos.php">Editar/Modificar
                            datos</a></li>
                    <li><a class="dropdown-item" href="IndexAdmin.php?principal=filtroProductos.php">Filtros
                            de Productos</a></li>
                    <li><a class="dropdown-item" href="../../'.PDF.'imprimirProductos.php">Catálogo
                            de productos por familia</a></li>
                    <li><a class="dropdown-item" href="IndexAdmin.php?principal=listadoPedidosProductos.php">Listado
                            Pedidos donde aparece un producto</a></li>
                    <li><a class="dropdown-item" href="IndexAdmin.php?principal=listadoClientesProductos.php">Listado
                            Clientes que han comprado un determinado producto</a></li>

                    <li><a class="dropdown-item"
                           href="#">Gráficos
                            de compras (top ten)</a></li>


                </ul>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarPedidos" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false"> Pedidos </a>
                <ul class="dropdown-menu" aria-labelledby="navbarPedidos">
                    <li><a class="dropdown-item"
                           href="IndexAdmin.php?principal=listadoFactura.php">Listado entre
                            fechas</a></li>
                    <li><a class="dropdown-item"
                           href="IndexAdmin.php?principal=listadoPedidosClientes.php">Listado por
                            cliente</a></li>
                    <li><a class="dropdown-item" href="IndexAdmin.php?principal=facturarPedido.php">Facturar
                            Pedido</a></li>
                    <li><a class="dropdown-item" href="IndexAdmin.php?principal=listadoPedidosEmpleados.php">Listado
                            por empleado que empaqueta</a></li>
                    <li><a class="dropdown-item" href="IndexAdmin.php?principal=listadoPedidosTransporte.php">Listado
                            por empresa de transporte</a></li>

                </ul>
            </li>


            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarUsuarios" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false"> Usuarios/Roles </a>
                <ul class="dropdown-menu" aria-labelledby="navbarUsuarios">
                    <li><a class="dropdown-item" href="IndexAdmin.php?principal=altaUsuarios.php">Alta
                            Usuario</a></li>
                    <li><a class="dropdown-item" href="IndexAdmin.php?principal=editarUsuarios.php">Modificación/Baja</a>
                    </li>
                    <li><a class="dropdown-item" href="IndexAdmin.php?principal=listadoUsuariosRoles.php">Filtros Usuarios/Roles</a></li>
                    <li><a class="dropdown-item" href="IndexAdmin.php?principal=altaRol.php">Añadir roles</a></li>


                    <li><a class="dropdown-item" href="IndexAdmin.php?principal=bajaRol.php">Suprimir un rol</a></li>


                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Ofertas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../../' . INFORMATIVAS . 'cerrarSesion.php">Cerrar
                    Sesión</a>
            </li>
        </ul>
    </div>
</nav>
<div class="container p-3">
    <div class="p-2">';
    if (isset($_GET['principal'])) {
        require_once $_GET['principal'];
    } else {
        echo "<h1 class=\"display-4\">Bienvenido a la vista de administrador</h1>";
    }

    echo '</div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
</div> <!--contenedor principal-->
<footer class="py-3 bg-info">
    <div class="container">
        <p class="m-0 text-center text-white">Cosme José Nieto Pérez</p>
        <p class="m-0 text-right text-dark">IES Castelar(Badajoz)</p>
    </div>

</footer>

</body>
</html> ';
} else {
    echo "<h1 class=\"display-4\">Error 505: Acceso Prohibido</h1></body></html>";
    echo "<p>Haga click <a href=\"../../IndexInicial.php\">
        aquí</a> para volver a la pagina de inicio.";
}
?>
