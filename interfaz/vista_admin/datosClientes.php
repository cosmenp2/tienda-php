<?php

/** @var Clientes $tCliente */
$tCliente = Clientes::singletonClientes();
/** @var DireccionesCliente $tDireccionesCliente */
$tDireccionesCliente = DireccionesCliente::singletonDireccionesCliente();

if (isset($_POST['cliente'])) {
    $_SESSION['tmpClient'] = $_POST['cliente'];
}

if (isset($_SESSION['tmpClient'])) {
    $usuario = $tCliente->getUnCliente($_SESSION['tmpClient']);
} else {
    $usuario = new Cliente(0, "", "", "", "", "", "", 0, "", "", 1);
}
$_SESSION['urlAnteriorAdmin'] = 'IndexAdmin.php?principal=datosClientes.php';
$errorDir = "";
$successDir = "";
$error = "";
$success = "";


if (isset($_POST['idEliminar'])) {
    if ($tDireccionesCliente->desactivarDireccion($_POST['idEliminar'])) {
        $successDir = "La direccion se ha eliminado correctamente";
    } else {
        $errorDir = "No se ha podido eliminar la direccion";
    }
}

if (isset($_GET['success'])) {
    $success = $_GET['success'];
}

if (isset($_GET['error'])) {
    $error = $_GET['error'];
}

if (isset($_GET['successDir'])) {
    $successDir = $_GET['successDir'];
}

if (isset($_GET['errorDir'])) {
    $errorDir = $_GET['errorDir'];
}
$direcciones = $tDireccionesCliente->getDireccionesCliente($usuario->getIdCliente());
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Datos del cliente</title>
</head>
<body>

<div class="card">
    <div class="card-header">
        Datos del cliente
    </div>
    <div class="card-body">
        <?php
        if ($error != "") {
            echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> " . $error . "</div>";
        } elseif ($success != "") {
            echo "<div class=\"alert alert-success\"><strong>Información</strong> " . $success . "</div>";
        }
        ?>
        <blockquote class="blockquote mb-0">
            <table class="table">
                <tr>
                    <td class="col-lg-6">Nombre:</td>
                    <td class="col-lg-6"><?php echo $usuario->getNombre() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">1º Apellido:</td>
                    <td class="col-lg-6"><?php echo $usuario->getApellido1() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">2º Apellido:</td>
                    <td class="col-lg-6"><?php echo $usuario->getApellido2() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">NIF:</td>
                    <td class="col-lg-6"><?php echo $usuario->getNif() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">Sexo:</td>
                    <td class="col-lg-6"><?php
                        if ($usuario->getVaron() == 1) {
                            echo "Hombre";
                        } else {
                            echo "Mujer";
                        } ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">Numero de cuenta:</td>
                    <td class="col-lg-6"><?php
                        if (empty($usuario->getNumCta())) {
                            echo "Sin rellenar";
                        } else {
                            echo $usuario->getNumCta();
                        }
                        ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">Como nos conoció:</td>
                    <td class="col-lg-6"><?php
                        if (empty($usuario->getComoNosConocio())) {
                            echo "Sin rellenar";
                        } else {
                            echo $usuario->getComoNosConocio();
                        }
                        ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6 col-md-6 col-sm-12"></td>
                    <td class="col-lg-6 col-md-6 col-sm-12">
                        <a class="btn btn-primary" href="IndexAdmin.php?principal=formEditCliente.php">Editar</a>
                    </td>
                </tr>

            </table>
        </blockquote>
    </div>
</div>
<div class="card">
    <div class="card-header">
        Direcciones
    </div>
    <div class="card-body">
        <?php
        if ($errorDir != "") {
            echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> " . $errorDir . "</div>";
        } elseif ($successDir != "") {
            echo "<div class=\"alert alert-success\"><strong>Información</strong> " . $successDir . "</div>";
        }
        ?>
        <blockquote class="blockquote mb-0">
            <table class="table">
                <?php
                if (sizeof($direcciones) > 0) {
                    /** @var DireccionCliente $d */
                    foreach ($direcciones as $d) {
                        echo '<tr>
                    <td class="col-lg-6">' . $d->getDireccion() . '. ' . $d->getCodPostal() . ' ' . $d->getLocalidad() . ' (' .
                            $d->getProvincia() . ') ,' . $d->getPais() . '.</td>
                    <td class="col-lg-6">
                    <form name="formularioModDirection" method="POST"
                        action="IndexAdmin.php?principal=formDireccionCliente.php" >
                        <input type="hidden" name="idDir" value="' . $d->getId() . '">
                        <input type="hidden" name="direccion" value="' . $d->getDireccion() . '">
                        <input type="hidden" name="codPostal" value="' . $d->getCodPostal() . '">
                        <input type="hidden" name="localidad" value="' . $d->getLocalidad() . '">
                        <input type="hidden" name="provincia" value="' . $d->getProvincia() . '">
                        <input type="hidden" name="pais" value="' . $d->getPais() . '">
                        <input class="btn btn-info" type="submit" name="editDirection" value="Editar">
                     </form>
                     <form name="formularioModDirection" method="POST"
                        action="IndexAdmin.php?principal=datosClientes.php" >
                        <input type="hidden" name="idEliminar" value="' . $d->getId() . '">
                        <input class="btn btn-danger" type="submit" name="removeDirection" value="Eliminar">
                      </form>
                      </td>
                    </tr>';
                    }
                }
                ?>
                <tr>
                    <td class="col-lg-6"></td>
                    <td class="col-lg-6">
                        <a class="btn btn-primary" href="IndexAdmin.php?principal=formDireccionCliente.php">Añadir
                            direccion</a>
                    </td>
                </tr>
            </table>
        </blockquote>
    </div>
</div>

</body>
</html>