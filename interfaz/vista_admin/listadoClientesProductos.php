<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Listado de clientes que han comprado un producto</title>
</head>
<body>
<h1>Listado de clientes que han comprado un producto</h1>
<form name="formulario1" method="POST" action="IndexAdmin.php?principal=listadoClientesProductos.php">
    <table class="table">
        <tr>
            <th>Propiedad</th>
            <th>Valor</th>
            <th></th>
        </tr>

        <tr>
            <td>Producto:</td>
            <td><select name="idProducto" required>
                    <option value=""></option>
                <?php
                /** @var Productos $tProducto */
                $tProducto = Productos::singletonProductos();
                /** @var Clientes $tClientes */
                $tClientes = Clientes::singletonClientes();

                $tablaClientes = null;
                $tablaProductos = null;
                $productos = null;
                $productos = $tProducto->getProductosTodos();
                /** @var Producto $p */
                foreach ($productos as $p) {
                    echo "<option value='" . $p->getIdProducto() . "'>" . $p->getIdProducto() . "</option>";
                }
                ?>
                </select>
            </td>
            <td><input class="btn btn-primary" type="submit" name="filtrar" value="Ver pedidos con este producto"></td>
        </tr>
    </table>
</form>

<?php

if (isset($_POST['idProducto'])) {
    $tablaClientes = $tClientes->getClientesByProductos($_POST['idProducto']);
    if (sizeof($tablaClientes) == 0) {
        echo "<div class=\"alert alert-warning\">No hay ningún cliente que haya comprado este producto</div>";
    } else {
        echo "<table class='table'>
			<tr>
				<td>IdCliente</td>
				<td>IdUsusario</td>
				<td>Nombre</td>
				<td>Primer apellido</td>
				<td>Segundo apellido</td>
				<td>Nif</td>
				<td>Sexo</td>
				<td>Numcta</td>
				<td>Como nos conoció</td>
				<td>Activo</td>
			</tr>";
        foreach ($tablaClientes as $c) {
            echo "<tr>";
            echo "<td>" . $c->getIdCliente() . "</td>";
            echo "<td>" . $c->getIdUsuario() . "</td>";
            echo "<td>" . $c->getNombre() . "</td>";
            echo "<td>" . $c->getApellido1() . "</td>";
            echo "<td>" . $c->getApellido2() . "</td>";
            echo "<td>" . $c->getNif() . "</td>";
            $valor = ($c->getVaron()) ? "Hombre" : "Mujer";
            echo "<td>" . "$valor" . "</td>";
            echo "<td>" . $c->getNumcta() . "</td>";
            echo "<td>" . $c->getComoNosConocio() . "</td>";
            echo "<td>" . $c->getActivo() . "</td>";
            echo " </tr>";
        }
        echo "</table>";
        echo "<h1 class='display-4'>Total: ".sizeof($tablaClientes)."</h1>";
    }
}
?>

</body>
</html>
