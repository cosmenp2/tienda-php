<?php
/** @var Usuarios $tUsuario */
$tUsuario = Usuarios::singletonUsuarios();
$isFirstTime = false;
if (!$tUsuario->existAdmin()) {
    $isFirstTime = true;
}

if ($isFirstTime){
    echo "
    <!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>Primer Inicio</title>
</head>
<h1 class=\"display-4\">Bienvenido</h1>
<p>Para poder administrar el sitio web debe de crear al menos un usuario administrador. Este proceso solo se puede realizar
si no existe ningún usuario administrador.</p>
<body>";
    if (isset($_GET['error'])) {
        echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> " . $_GET['error'] . "</div>";
    }
    echo "<div class=\"form-group table-responsive\">
    <form name=\"formularioPrimerRegistro\" method=\"POST\"
          action=\"" . VISTA_ADMIN . "primerRegistro.php\" >
        <table class=\"table\">
            <tr>
                <td class=\"col-lg-6 col-md-6 col-sm-12\">Nombre de usuario:</td>
                <td class=\"col-lg-6 col-md-6 col-sm-12\"><input type=\"text\" name=\"username\"
                                                               required ";
    if (isset($_SESSION['form'])){
        echo "value=\"".$_SESSION['form']."\" ";
    }
    echo "></td>
            </tr>
            <tr>
                <td class=\"col-lg-6 col-md-6 col-sm-12\">Contraseña:</td>
                <td class=\"col-lg-6 col-md-6 col-sm-12\"><input type=\"password\" name=\"password\"  min='3' max='64' required></td>
            </tr>
            <tr>
                <td class=\"col-lg-6 col-md-6 col-sm-12\">Repite la contraseña:</td>
                <td class=\"col-lg-6 col-md-6 col-sm-12\"><input type=\"password\" name=\"passwordRepite\" min=\"3\" max=\"64\" required></td>
            </tr>
            <tr>
                <td class=\"col-lg-6 col-md-6 col-sm-12\"><input class=\"btn btn-primary\" type=\"submit\" name=\"alta\" value=\"Registrar administrador\"></td>
            </tr>
        </table>

    </form>
</div>
</body>
</html>";
} else {
    echo "<h1 class=\"display-4\">505: Acceso prohibido</h1>";
}
