<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Datos de los productos</title>
</head>
<body>
<?php
/** @var Productos $tProductos */
$tProductos = Productos::singletonProductos();
$_SESSION['urlAnteriorAdmin'] = 'IndexAdmin.php?principal=editarProductos.php';
if (isset($_SESSION['tmpProducto'])){
    unset($_SESSION['tmpProducto']);
}

$productos = $tProductos->getProductosTodos();
if (sizeof($productos) > 0) {
    echo '
<table class="table">
    <tr>
        <th class="col-lg-3 col-md-3 col-sm-12">Id producto</th>            
        <th class="col-lg-7 col-md-7 col-sm-9">Descripcion</th>            
        <th class="col-lg-2 col-md-2 col-sm-3">Ver</th>        
    </tr>';
    /** @var Producto $p */
    foreach ($productos as $p) {
        echo '
    <tr>';
        if ($p->getStockActual()< $p->getStockMinimo()){
            echo "<div class=\"alert alert-warning\"><strong>Atención</strong> Hay pocas unidades del producto ".$p->getIdProducto()
                ." disponibles</div>";
        }
        echo '
        <td class="col-lg-3 col-md-3 col-sm-12">' . $p->getIdProducto() . '</td>            
        <td class="col-lg-7 col-md-7 col-sm-9">' . $p->getDescripcion() . '</td>            
        <td class="col-lg-2 col-md-2 col-sm-3">
            <form method="post" action="IndexAdmin.php?principal=datosProductos.php">
            <input type="hidden" name="producto" value="' . $p->getIdProducto() . '">
            <input class="btn btn-primary" type="submit" name="verProducto" value="Ver">
</form>
        </td>
     </tr>';
    }

    echo '</table>';
} else {
    echo "<h1 class=\"display-4\">No hay ningún producto</h1>";
}
?>
</body>
</html>