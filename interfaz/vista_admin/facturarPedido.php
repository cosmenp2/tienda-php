<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Pedidos</title>
</head>
<body>
<?php
/** @var Pedidos $tPedidos */
$tPedidos = Pedidos::singletonPedidos();

if (isset($_POST['idPedido'])) {
    if ($tPedidos->facturarPedido($_POST['idPedido'])) {
        $successFactura = "La factura se ha procesado correctamente";
    } else {
        $errorFactura = "No se ha podido procesar la factura";
    }
}

if (isset($errorFactura)) {
    echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> ".$errorFactura."</div>";
}
if (isset($successFactura)) {
    echo "<div class=\"alert alert-success\"><strong>Información</strong> " . $successFactura . "</div>";
}

$pedidos = $tPedidos->getPedidosTodos();
if (sizeof($pedidos) > 0) {
    echo "<table class='table'>
			<tr>
				<td>Empresa de transporte</td>
				<td>Fecha del pedido</td>
				<td>Fecha de envio</td>
				<td>Fecha de entrega</td>
				<td>Factura</td>
				<td>Fecha factura</td>
				<td>Pagado</td>
				<td>Fecha pago</td>
				<td>Metodo de pago</td>
				<td>Cliente</td>
				<td>Facturar</td>
			</tr>";

    /** @var Pedido $p */
    foreach ($pedidos as $p) {
        $mostrarBoton = false;
        $empresaTrans = "";
        switch ($p->getIdEmpresaTransporte()){
            case 0:
                $empresaTrans = "No asignada";
                break;
            case 1:
                $empresaTrans = "Seur";
                break;
            case 2:
                $empresaTrans = "DHL";
                break;
            case 3:
                $empresaTrans = "Correos";
                break;
            case 4:
                $empresaTrans = "MRV";
                break;
        }
        if ($p->getFacturado()==0){
            $mostrarBoton = true;
        }
        echo "<tr>";
        echo "<td>" . $empresaTrans . "</td>";
        echo "<td>" . $p->getFechaPedido() . "</td>";
        echo "<td>" . $p->getFechaEnvio() . "</td>";
        echo "<td>" . $p->getFechaEntrega() . "</td>";
        echo "<td>" . $p->getIdFactura() . "</td>";
        echo "<td>" . $p->getFechaFactura() . "</td>";
        echo "<td>" . (($p->getPagado() == 1) ? "Si" : "No") . "</td>";
        echo "<td>" . $p->getFechaPago() . "</td>";
        echo "<td>" . $p->getMetodoPago() . "</td>";
        echo "<td>" . $p->getIdCliente() . "</td>";
        if ($mostrarBoton){
            echo "<td><form name=\"formularioModPedido\" method=\"POST\"
                        action=\"IndexAdmin.php?principal=facturarPedido.php\" >
                        <input type=\"hidden\" name=\"idPedido\" value=\"" . $p->getIdPedido() . "\">
                        <input class=\"btn btn-primary\" type=\"submit\" name=\"addFactura\" value=\"Facturar\">
                        </form></td>";
        } else {
            echo "<td>Facturado</td>";
        }
        echo " </tr>";
    }
    echo "</table>";
} else {
    echo "<h1 class=\"display-4\">No hay ningún pedido</h1>";
}
?>
</body>
</html>