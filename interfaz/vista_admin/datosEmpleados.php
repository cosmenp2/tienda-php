<?php

/** @var Empleados $tEmpleado */
$tEmpleado = Empleados::singletonEmpleados();
/** @var DireccionesEmpleado $tDireccionesEmpleado */
$tDireccionesEmpleado = DireccionesEmpleado::singletonDireccionesEmpleado();

if (isset($_POST['empleado'])) {
    $_SESSION['tmpEmpleado'] = $_POST['empleado'];
}

if (isset($_SESSION['tmpEmpleado'])) {
    $usuario = $tEmpleado->getEmpleado($_SESSION['tmpEmpleado']);
} else {
    $usuario = new Empleado(0, "", "", "", "", "", "", "", "", "", 1);
}
$_SESSION['urlAnteriorAdmin'] = 'IndexAdmin.php?principal=datosEmpleados.php';
$errorDir = "";
$successDir = "";
$error = "";
$success = "";


if (isset($_POST['idEliminar'])) {
    if ($tDireccionesEmpleado->desactivarDireccion($_POST['idEliminar'])) {
        $successDir = "La direccion se ha eliminado correctamente";
    } else {
        $errorDir = "No se ha podido eliminar la direccion";
    }
}

if (isset($_GET['success'])) {
    $success = $_GET['success'];
}

if (isset($_GET['error'])) {
    $error = $_GET['error'];
}

if (isset($_GET['successDir'])) {
    $successDir = $_GET['successDir'];
}

if (isset($_GET['errorDir'])) {
    $errorDir = $_GET['errorDir'];
}
$direcciones = $tDireccionesEmpleado->getDireccionesByEmpleado($usuario->getIdEmpleado());
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Datos del empleado</title>
</head>
<body>

<div class="card">
    <div class="card-header">
        Datos del empleado
    </div>
    <div class="card-body">
        <?php
        if ($error != "") {
            echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> " . $error . "</div>";
        } elseif ($success != "") {
            echo "<div class=\"alert alert-success\"><strong>Información</strong> " . $success . "</div>";
        }
        ?>
        <img id="myImg" class="card-img-top"
             src="<?php echo "../../".FOTOS . $usuario->getRutaFoto(); ?>" style="width:50%;" alt="Foto del empleado">
        <blockquote class="blockquote mb-0">
            <table class="table">
                <tr>
                    <td class="col-lg-6">Nombre:</td>
                    <td class="col-lg-6"><?php echo $usuario->getNombre() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">1º Apellido:</td>
                    <td class="col-lg-6"><?php echo $usuario->getApellido1() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">2º Apellido:</td>
                    <td class="col-lg-6"><?php echo $usuario->getApellido2() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">NIF:</td>
                    <td class="col-lg-6"><?php echo $usuario->getNif() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">Numero de cuenta:</td>
                    <td class="col-lg-6"><?php
                        if (empty($usuario->getNumCta())) {
                            echo "Sin rellenar";
                        } else {
                            echo $usuario->getNumCta();
                        }
                        ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">Movil:</td>
                    <td class="col-lg-6"><?php echo $usuario->getMovil() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6 col-md-6 col-sm-12"></td>
                    <td class="col-lg-6 col-md-6 col-sm-12">
                        <a class="btn btn-primary" href="IndexAdmin.php?principal=formEditEmpleado.php">Editar</a>
                    </td>
                </tr>

            </table>
        </blockquote>
    </div>
</div>
<div class="card">
    <div class="card-header">
        Direcciones
    </div>
    <div class="card-body">
        <?php
        if ($errorDir != "") {
            echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> " . $errorDir . "</div>";
        } elseif ($successDir != "") {
            echo "<div class=\"alert alert-success\"><strong>Información</strong> " . $successDir . "</div>";
        }
        ?>
        <blockquote class="blockquote mb-0">
            <table class="table">
                <?php
                if (sizeof($direcciones) > 0) {
                    /** @var DireccionEmpleado $d */
                    foreach ($direcciones as $d) {
                        echo '<tr>
                    <td class="col-lg-6">' . $d->getDireccion() . '. ' . $d->getCodPostal() . ' ' . $d->getLocalidad() . ' (' .
                            $d->getProvincia() . ') ,' . $d->getPais() . '.</td>
                    <td class="col-lg-6">
                    <form name="formularioModDirection" method="POST"
                        action="IndexAdmin.php?principal=formDireccionEmpleado.php" >
                        <input type="hidden" name="idDir" value="' . $d->getId() . '">
                        <input type="hidden" name="direccion" value="' . $d->getDireccion() . '">
                        <input type="hidden" name="codPostal" value="' . $d->getCodPostal() . '">
                        <input type="hidden" name="localidad" value="' . $d->getLocalidad() . '">
                        <input type="hidden" name="provincia" value="' . $d->getProvincia() . '">
                        <input type="hidden" name="pais" value="' . $d->getPais() . '">
                        <input class="btn btn-info" type="submit" name="editDirection" value="Editar">
                     </form>
                     <form name="formularioModDirection" method="POST"
                        action="IndexAdmin.php?principal=datosEmpleados.php" >
                        <input type="hidden" name="idEliminar" value="' . $d->getId() . '">
                        <input class="btn btn-danger" type="submit" name="removeDirection" value="Eliminar">
                      </form>
                      </td>
                    </tr>';
                    }
                }
                ?>
                <tr>
                    <td class="col-lg-6"></td>
                    <td class="col-lg-6">
                        <a class="btn btn-primary" href="IndexAdmin.php?principal=formDireccionEmpleado.php">Añadir
                            direccion</a>
                    </td>
                </tr>
            </table>
        </blockquote>
    </div>
</div>

</body>
</html>