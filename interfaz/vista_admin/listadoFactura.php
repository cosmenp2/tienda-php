<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Pedidos</title>
</head>
<body>
<h1>Listado de pedidos entre fechas</h1>
<table class="table">
    <tr>
        <td>
            <form action='../pdf/imprimirPedidosAEmpaquetar.php' name='formulario' method='post'>
                <input class="btn btn-primary" type="submit" name="imprimir" value="Imprimir pedidos pendientes de empaquetar">
            </form>
        </td>
        <td>
            <form action='../pdf/imprimirPedidosAEnviar.php' name='formulario' method='post'>
                <input class="btn btn-primary" type="submit" name="imprimir" value="Imprimir pedidos pendientes de envio">
            </form>
        </td>
    </tr>
</table>



<form action="IndexAdmin.php?principal=listadoFactura.php" method="post">
    <table class="table">
        <tr>
            <td>Fecha inicial</td>
            <td>Fecha tope</td>
        </tr>
        <tr>
            <td>
                <input type="date" name="fechaInicio">
            </td>
            <td>
                <input type="date" name="fechaFinal">
            </td>
        </tr>
    </table>
    <input class="btn btn-primary" type="submit" name="Listar" value="Listar pedidos">
</form>
<?php

if (isset($_POST['fechaInicio'])) {
    $fechaInicio = $_POST['fechaInicio'];
    $fechaFinal = $_POST['fechaFinal'];
    /** @var Pedidos $tP */
    $tP = Pedidos::singletonPedidos();
    /** @var LineasPedidos $tLinea */
    $tLinea = LineasPedidos::singletonLineasPedidos();

    if (empty($fechaInicio) && empty($fechaFinal)) {
        $tablaPedidos = $tP->getPedidosTodos();
    } else {
        $tablaPedidos = $tP->getPedidosByDate($fechaInicio, $fechaFinal);
    }


    if (sizeof($tablaPedidos) != 0) {
        echo "<table class='table'>
			<tr>
				<td>Número de factura</td>
				<td>Número de pedido</td>
				<td>Fecha de pedido</td>
				<td>Cliente</td>
				<td>Base imponible</td>
			</tr>";
        /** @var Pedido $p */
        foreach ($tablaPedidos as $p) {
            $baseImponible = $tLinea->getTotalUnPedido($p->getIdPedido());
            echo "<tr>";
            echo "<td>" . $p->getIdFactura() . "</td>";
            echo "<td>" . $p->getIdPedido() . "</td>";
            echo "<td>" . $p->getFechaPedido() . "</td>";
            echo "<td>" . $p->getIdCliente() . "</td>";
            echo "<td>" . $baseImponible . "</td>";
            echo "</tr>";
        }
        echo "</table>";
        echo "<form action='../../".PDF."imprimirFacturas.php' name='formulario' method='post'>";
        echo "<input class=\"btn btn-primary\" type=\"submit\" name=\"imprimir\" value=\"Imprimir estos pedidos\">";
        echo "<input type='HIDDEN' name='fechaI' value='" . $fechaInicio . "'>";
        echo "<input type='HIDDEN' name='fechaF' value='" . $fechaFinal . "'>";
        echo "</form>";
    } else {
        echo "<div class=\"alert alert-warning\">No hay ningún pedido con estos patrones</div>";
    }

}
?>
</body>
</html>
