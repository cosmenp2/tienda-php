<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Suprimir roles</title>
</head>
<body>
<h1>Suprimir roles</h1>
<?php
/** @var Roles $tRoles */
$tRoles = Roles::singletonRoles();


$error = "";
$success = "";

if (isset($_POST['idEliminar'])) {
    if ($_POST['idEliminar'] > 3 ) {
        $eliminado = $tRoles->deleteRol($_POST['idEliminar']);
        if ($eliminado) {
            $success = "Se ha eliminado el rol correctamente";
        } else {
            $error = "Ha habido un error al eliminar el rol";
        }
    } else {
        $error = "No puedes borrar un rol del sistema";
    }
}

if (!empty($error)) {
    echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> " . $error . "</div>";
} elseif (!empty($success)) {
    echo "<div class=\"alert alert-success\">" . $success . "</div>";
}

$roles = $tRoles->getTodos();
if (sizeof($roles) > 0) {
    echo '
<table class="table">
    <tr>
        <th class="col-lg-2 col-md-2 col-sm-6">Rol</th>       
        <th class="col-lg-2 col-md-2 col-sm-6">Acción</th>    
    </tr>';
    /** @var Rol $r */
    foreach ($roles as $r) {
        echo '
    <tr>
        <td class="col-lg-2 col-md-2 col-sm-6">' . $r->getNombre() . '</td>';

        if ($r->getId() > 3)
            echo '<td class="col-lg-2 col-md-2 col-sm-6">
            <form method="post" action="IndexAdmin.php?principal=bajaRol.php">
            <input type="hidden" name="idEliminar" value="' . $r->getId() . '">
            <input class="btn btn-danger" type="submit" name="eliminarRol" value="Eliminar">
</form>
        </td>           
    </tr>';
        else
            echo '<td class="col-lg-2 col-md-2 col-sm-6">
            <input class="btn btn-info" value="Rol del sistema" disabled>
        </td>           
    </tr>';
    }
    echo '</table>';
} else {
    echo "<h1 class=\"display-4\">No hay ningún rol</h1>";
}
?>
</body>
</html>
