<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Editar empleado</title>
</head>
<h1 class="display-4">Editar empleado</h1>
<body>
<?php
/** @var Empleados $tEmpleado */
$tEmpleado = Empleados::singletonEmpleados();

if (isset($_GET['error'])) {
    echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> ".$_GET['error']."</div>";
}
if (isset($_GET['success'])) {
    echo "<div class=\"alert alert-success\"><strong>Información</strong> " . $_GET['success'] . "</div>";
}
if (isset($_SESSION['form'])) {
    $nombre = $_SESSION['form']['nombre'];
    $apellido1 = $_SESSION['form']['apellido1'];
    $apellido2 = (isset($_SESSION['form']['apellido2'])) ? $_SESSION['form']['apellido2'] : "";
    $nif = $_SESSION['form']['nif'];
    $movil = $_SESSION['form']['movil'];
    $numCta = (isset($_SESSION['form']['numCta'])) ? $_SESSION['form']['numCta'] : "";
} else {
    $empleado = $tEmpleado->getEmpleado($_SESSION['tmpEmpleado']);
    $nombre = $empleado->getNombre();
    $apellido1 = $empleado->getApellido1();
    $apellido2 = $empleado->getApellido2();
    $nif = $empleado->getNif();
    $movil = $empleado->getMovil();
    $numCta = $empleado->getNumCta();
}
?>
<div class="form-group table-responsive">

    <form name="formularioAltaEmpleadoAdmin" method="POST" enctype="multipart/form-data"
          action="editEmpleado.php">
        <table class="table">
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Nombre*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="nombre" max="20" required <?php echo 'value="'.$nombre.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Primer apellido*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="apellido1" max="20" required <?php echo 'value="'.$apellido1.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Segundo apellido:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="apellido2" max="20" <?php echo 'value="'.$apellido2.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">NIF*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="nif" max="9" required <?php echo 'value="'.$nif.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Movil*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="movil" max="9" required <?php echo 'value="'.$movil.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Numero de cuenta*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="numCta" max="24" <?php echo 'value="'.$numCta.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12" >Modificar foto: </td>
                <td class="col-lg-6 col-md-6 col-sm-12" ><input type="file" name="foto"/></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12"><input class="btn btn-danger" type="reset" name="reset" value="Reiniciar todo"></td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input class="btn btn-primary" type="submit" name="alta" value="Modificar"></td>
            </tr>
        </table>
    </form>
    <p class="p">Los campos con * se deben rellenar obligatoriamente</p>
</div>

</body>
</html>