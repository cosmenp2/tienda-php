<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Editar Familia</title>
</head>
<h1 class="display-4">Editar familia</h1>
<body>
<?php
/** @var FamiliasProductos $tFamilia */
$tFamilia = FamiliasProductos::singletonFamiliasProductos();

if (isset($_GET['error'])) {
    echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> ".$_GET['error']."</div>";
}
if (isset($_GET['success'])) {
    echo "<div class=\"alert alert-success\"><strong>Información</strong> " . $_GET['success'] . "</div>";
}

if (isset($_POST['familia'])){
    $_SESSION['tmpFamilia'] = $_POST['familia'];
}
if (isset($_SESSION['form'])) {
    $nombre = $_SESSION['form']['nombre'];
    $descripcion = $_SESSION['form']['descripcion'];
} else {
    $familia = $tFamilia->getFamiliaById($_SESSION['tmpFamilia']);
    $nombre = $familia->getNombre();
    $descripcion = $familia->getDescripcion();
}
?>
<div class="form-group table-responsive">

    <form name="formularioAltaClienteAdmin" method="POST"
          action="editFamilia.php">
        <table class="table">
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Nombre*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="nombre" max="20" required <?php echo 'value="'.$nombre.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Descripcion:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="descripcion" <?php echo 'value="'.$descripcion.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12"><input class="btn btn-danger" type="reset" name="reset" value="Reiniciar todo"></td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input class="btn btn-primary" type="submit" name="alta" value="Modificar"></td>
            </tr>
        </table>
    </form>
    <p class="p">Los campos con * se deben rellenar obligatoriamente</p>
</div>

</body>
</html>