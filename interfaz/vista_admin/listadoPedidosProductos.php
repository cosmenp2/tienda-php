<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Listado de pedidos de un producto</title>
</head>
<body>
<h1>Listado de pedidos de un producto</h1>
<form name="formulario1" method="POST" action="IndexAdmin.php?principal=listadoPedidosProductos.php">
    <table class="table">
        <tr>
            <th>Propiedad</th>
            <th>Valor</th>
            <th></th>
        </tr>

        <tr>
            <td>Producto:</td>
            <td><select name="idProducto" required>
                    <option value=""></option>
                <?php
                /** @var Productos $tProducto */
                $tProducto = Productos::singletonProductos();
                /** @var Pedidos $tPedidos */
                $tPedidos = Pedidos::singletonPedidos();

                $tablaPedidos = null;
                $tablaProductos = null;
                $productos = null;
                $productos = $tProducto->getProductosTodos();
                /** @var Producto $p */
                foreach ($productos as $p) {
                    echo "<option value='" . $p->getIdProducto() . "'>" . $p->getIdProducto() . "</option>";
                }
                ?>
                </select>
            </td>
            <td><input class="btn btn-primary" type="submit" name="filtrar" value="Ver pedidos con este producto"></td>
        </tr>
    </table>
</form>

<?php

if (isset($_POST['idProducto'])) {
    $tablaPedidos = $tPedidos->getPedidosByProductos($_POST['idProducto']);
    if (sizeof($tablaPedidos) == 0) {
        echo "<div class=\"alert alert-warning\">No hay ningún pedido con este producto</div>";
    } else {
        echo "<table class='table'>
			<tr>
				<td>Empresa de transporte</td>
				<td>Fecha del pedido</td>
				<td>Fecha de envio</td>
				<td>Fecha de entrega</td>
				<td>Facturado</td>
				<td>Factura</td>
				<td>Fecha factura</td>
				<td>Pagado</td>
				<td>Fecha pago</td>
				<td>Metodo de pago</td>
				<td>Cliente</td>
			</tr>";

        foreach ($tablaPedidos as $p) {
            $empresaTrans = "";
            switch ($p->getIdEmpresaTransporte()){
                case 0:
                    $empresaTrans = "No asignada";
                    break;
                case 1:
                    $empresaTrans = "Seur";
                    break;
                case 2:
                    $empresaTrans = "DHL";
                    break;
                case 3:
                    $empresaTrans = "Correos";
                    break;
                case 4:
                    $empresaTrans = "MRV";
                    break;
            }
            echo "<tr>";
            echo "<td>" . $empresaTrans . "</td>";
            echo "<td>" . $p->getFechaPedido() . "</td>";
            echo "<td>" . $p->getFechaEnvio() . "</td>";
            echo "<td>" . $p->getFechaEntrega() . "</td>";
            echo "<td>" . (($p->getFacturado() == 1) ? "Si" : "No") . "</td>";
            echo "<td>" . $p->getIdFactura() . "</td>";
            echo "<td>" . $p->getFechaFactura() . "</td>";
            echo "<td>" . (($p->getPagado() == 1) ? "Si" : "No") . "</td>";
            echo "<td>" . $p->getFechaPago() . "</td>";
            echo "<td>" . $p->getMetodoPago() . "</td>";
            echo "<td>" . $p->getIdCliente() . "</td>";
            echo " </tr>";
        }
        echo "</table>";
        echo "<h1 class='display-4'>Total: ".sizeof($tablaPedidos)."</h1>";
    }
}
?>

</body>
</html>
