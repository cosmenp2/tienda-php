<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Listado de Familias</title>
</head>
<body>
<h1>Listado de Familias</h1>
<form name="formulario1" method="POST" action="IndexAdmin.php?principal=filtroFamilias.php">
    <table class="table">
        <tr>
            <th>Propiedad</th>
            <th>Valor</th>
            <th></th>
        </tr>

        <tr>
            <td>Nombre:</td>
            <td><input type="text" name="nombre" required></td>
            <td><input class="btn btn-primary" type="submit" name="filtrar" value="Filtrar"></td>
        </tr>
    </table>
</form>

<?php

/** @var FamiliasProductos $tFamilia */
$tFamilia = FamiliasProductos::singletonFamiliasProductos();
$tablaFamilia = null;

if (isset($_POST['nombre'])) {
    $tablaFamilia = $tFamilia->getByName($_POST['nombre']);

    if (sizeof($tablaFamilia) == 0) {
        echo "<div class=\"alert alert-warning\">No se ha encontrado ninguna familia con este patrón</div>";
    } else {
        echo "<table class='table'>
			<tr>
				<td>IdFamilia</td>
				<td>Nombre</td>
				<td>Descripcion</td>
				<td>Activo</td>
			</tr>";
        /** @var FamiliaProducto $f */
        foreach ($tablaFamilia as $f) {
            echo "<tr>";
            echo "<td>" . $f->getIdFamilia() . "</td>";
            echo "<td>" . $f->getNombre() . "</td>";
            echo "<td>" . $f->getDescripcion() . "</td>";
            echo "<td>" . $f->getActivo() . "</td>";
            echo " </tr>";
        }
        echo "</table>";
    }
}
?>

</body>
</html>
