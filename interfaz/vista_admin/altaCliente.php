<?php
require_once('../../rutas.php');
require_once "../../".PERSISTENCIA . 'Clientes.php';
require_once "../../".PERSISTENCIA . "Usuarios.php";
require_once "../../".POJOS . "Cliente.php";
require_once "../../".POJOS . "Usuario.php";
ini_set('session.use_strict_mode', 1);
session_id($_COOKIE['idSesion']);
session_start();
/** @var Usuarios $tUsuario */
$tUsuario = Usuarios::singletonUsuarios();
$usuario = null;
$legitima = false;

if (isset($_SESSION['idAdmin'])) {
    $usuario = $tUsuario->buscaUsuario($_SESSION['idAdmin']);
    if (is_null($usuario)) {
        $_SESSION['idAdmin'] = "";
    } else {
        $_SESSION['time'] = time();
        $nombre = $usuario->getLogin();
        $legitima = true;
    }
}

$direccion = "Location:IndexAdmin.php?principal=formAltaCliente.php";
$error = "";
$success = "";

if (isset($_POST['nombre']) && $legitima) {
    $nombre = $_POST['nombre'];
    $apellido1 = $_POST['apellido1'];
    $apellido2 = (isset($_POST['apellido2'])) ? $_POST['apellido2'] : "";
    $nif = $_POST['nif'];
    $sexo = ($_POST['sexo'] == "H") ? 1 : 0;
    $numCta = (isset($_POST['numCta'])) ? $_POST['numCta'] : "";
    $cnc = (isset($_POST['cnc'])) ? $_POST['cnc'] : "";
    $_SESSION['form'] = array("nombre"=>$nombre,"apellido1"=>$apellido1, "apellido2"=>$apellido2, "nif"=>$nif,
        "sexo"=>$sexo,"numCta"=>$numCta,"cnc"=>$cnc);
    /** @var Clientes $tCliente */
    $tCliente = Clientes::singletonClientes();

    if (!is_null($tCliente->getUnClienteByNif($nif))){
        $error .= "&error=Ya%20existe%20un%20cliente%20con%20el%20mismo%20DNI.%20";
    }

    if (empty($error)){
        $newID = $tCliente -> generarID();
        $c = new Cliente(0, $newID, 0, $nombre, $apellido1, $apellido2, $nif, $sexo,
            $numCta, $cnc, 1);


        $insertado = $tCliente->adduncliente($c);
        if ($insertado) {
            unset($_SESSION['form']);
            $success .= "&success=Se%20ha%20realizado%20el%20alta%20correctamente";
        } else {
            $error .= "&error=Ha%20habido%20un%20problema%20al%20guardar%20los%20datos%20del%20cliente.";
        }

    }
}
header($direccion.$error.$success);