<?php
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Alta Empleados</title>
</head>
<body>
<h1 class="display-4">Alta de empleados</h1>
<?php
if (isset($_GET['error'])) {
    echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> ".$_GET['error']."</div>";
}
if (isset($_GET['success'])) {
    echo "<div class=\"alert alert-success\"><strong>Información</strong> " . $_GET['success'] . "</div>";
}
if (isset($_SESSION['form'])) {
    $nombre = $_SESSION['form']['nombre'];
    $apellido1 = $_SESSION['form']['apellido1'];
    $apellido2 = (isset($_SESSION['form']['apellido2'])) ? $_SESSION['form']['apellido2'] : "";
    $nif = $_SESSION['form']['nif'];
    $numCta = $_SESSION['form']['numCta'];
    $movil = $_SESSION['form']['movil'];
    $pais = $_SESSION['form']['pais'];
    $provincia = $_SESSION['form']['provincia'];
    $localidad = $_SESSION['form']['localidad'];
    $codPostal = $_SESSION['form']['codPostal'];
    $direccion = $_SESSION['form']['direccion'];
} else {
    $nombre = "";
    $apellido1 = "";
    $apellido2 = "";
    $nif = "";
    $numCta = "";
    $movil = "";
    $pais = "";
    $provincia = "";
    $localidad = "";
    $codPostal = "";
    $direccion = "";
}
?>
<div class="form-group table-responsive">

    <form name="formularioAltaEmpleado" method="POST" enctype="multipart/form-data"
          action="altaEmpleado.php" >
        <table class="table">
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Nombre*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="nombre" max="20" required <?php echo 'value="'.$nombre.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Primer apellido*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="apellido1" max="20" required <?php echo 'value="'.$apellido1.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Segundo apellido:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="apellido2" max="20" <?php echo 'value="'.$apellido2.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">NIF*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="nif" max="9" required <?php echo 'value="'.$nif.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Numero de cuenta*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="numCta" max="24" required <?php echo 'value="'.$numCta.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Movil*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="movil" max="24" required <?php echo 'value="'.$movil.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Pais*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="pais"
                                                               required <?php echo 'value="' . $pais . '"' ?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Provincia*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="provincia" required
                        <?php echo 'value="' . $provincia . '"' ?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Localidad*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="localidad"
                                                               required <?php echo 'value="' . $localidad . '"' ?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Codigo Postal*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="codPostal" minlength="5" maxlength="5"
                                                               required <?php echo 'value="' . $codPostal . '"' ?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Direccion*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="direccion"
                                                               required <?php echo 'value="' . $direccion . '"' ?>
                    ></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12" >Foto*: </td>
                <td class="col-lg-6 col-md-6 col-sm-12" ><input type="file" name="foto" required/></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12"><input class="btn btn-danger" type="reset" name="reset" value="Borrar todo"></td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input class="btn btn-primary" type="submit" name="alta" value="Realizar Alta"></td>
            </tr>
        </table>
    </form>
    <p class="p">Los campos con * se deben rellenar obligatoriamente</p>
</div>

</body>
</html>