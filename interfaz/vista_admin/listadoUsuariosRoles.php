<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Listado de pedidos de un producto</title>
</head>
<body>
<h1>Listado de pedidos de un producto</h1>
<form name="formulario1" method="POST" action="IndexAdmin.php?principal=listadoUsuariosRoles.php">
    <table class="table">
        <tr>
            <th>Propiedad</th>
            <th>Valor</th>
            <th></th>
        </tr>

        <tr>
            <td>Rol:</td>
            <td><select name="idRol" required>
                    <option value=""></option>
                <?php
                /** @var Roles $tRoles */
                $tRoles = Roles::singletonRoles();
                /** @var Usuarios $tUsuarios */
                $tUsuarios = Usuarios::singletonUsuarios();

                $tablaUsuarios = null;
                $tablaRoles = null;
                $roles = null;
                $roles = $tRoles->getTodos();
                /** @var Rol $u */
                foreach ($roles as $u) {
                    echo "<option value='" . $u->getId() . "'>" . $u->getNombre() . "</option>";
                }
                ?>
                </select>
            </td>
            <td><input class="btn btn-primary" type="submit" name="filtrar" value="Ver usuarios con este rol"></td>
        </tr>
    </table>
</form>

<?php
if (isset($_POST['idRol'])) {
    $tablaUsuarios = $tUsuarios->getUsuariosByRol($_POST['idRol']);
    if (sizeof($tablaUsuarios) == 0) {
        echo "<div class=\"alert alert-warning\">No hay ningún usuario con este rol</div>";
    } else {
        echo "<table class='table'>
			<tr>
				<td>Nombre</td>
				<td>Activo</td>
			</tr>";

        /** @var Usuario $u */
        foreach ($tablaUsuarios as $u) {
            echo "<tr>";
            echo "<td>" . $u->getLogin() . "</td>";
            if ($u->getActivo() == 1){
                echo "<td> Si </td>";
            } else {
                echo "<td> No </td>";
            }
            echo " </tr>";
        }
        echo "</table>";
        echo "<h1 class='display-4'>Total: ".sizeof($tablaUsuarios)."</h1>";
    }
}
?>

</body>
</html>
