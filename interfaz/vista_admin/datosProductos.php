<?php

/** @var Productos $tProducto */
$tProducto = Productos::singletonProductos();
/** @var FamiliasProductos $tFamilia */
$tFamilia = FamiliasProductos::singletonFamiliasProductos();

if (isset($_POST['producto'])) {
    $_SESSION['tmpProducto'] = $_POST['producto'];
}

if (isset($_SESSION['tmpProducto'])) {
    $producto = $tProducto->getProductoById($_SESSION['tmpProducto']);
    $familia = $tFamilia->getFamiliaById($producto->getIdFamilia());
} else {
    $producto = new Producto(0, "", "", "", "", "", "",
        "", "", "", "","","",1);
    $familia = new FamiliaProducto(0,"","","",1);
}
$_SESSION['urlAnteriorAdmin'] = 'IndexAdmin.php?principal=datosProductos.php';
$error = "";
$success = "";
$errorStock = "";
$successStock = "";

if (isset($_POST['stockActual'])) {
    if ($tProducto->actualizarStock($producto->getIdProducto(),$_POST['stockActual'])) {
        $successStock = "El stock se ha actualizado correctamente";
        $producto ->setStockActual($_POST['stockActual']);
    } else {
        $errorStock = "No se ha podido actualizar el stock";
    }
}

if (isset($_GET['success'])) {
    $success = $_GET['success'];
}

if (isset($_GET['error'])) {
    $error = $_GET['error'];
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Datos del producto</title>
</head>
<body>

<div class="card">
    <div class="card-header">
        Datos del producto
    </div>
    <div class="card-body">
        <?php
        if ($error != "") {
            echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> " . $error . "</div>";
        } elseif ($success != "") {
            echo "<div class=\"alert alert-success\"><strong>Información</strong> " . $success . "</div>";
        }
        if ($producto->getStockActual()< $producto->getStockMinimo()){
            echo "<div class=\"alert alert-warning\"><strong>Atención</strong> Hay pocas unidades del producto disponibles</div>";
        }
        ?>
        <img id="myImg" class="card-img-top"
             src="<?php echo "../../".FOTOS . $producto->getRutaFoto(); ?>" style="width:50%;" alt="Foto del producto">
        <blockquote class="blockquote mb-0">
            <table class="table">
                <tr>
                    <td class="col-lg-6">Id producto:</td>
                    <td class="col-lg-6"><?php echo $producto->getIdProducto() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">Familia:</td>
                    <td class="col-lg-6"><?php echo $familia->getNombre() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">Iva:</td>
                    <td class="col-lg-6"><?php echo $producto->getTipoIva() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">Precio coste:</td>
                    <td class="col-lg-6"><?php echo $producto->getPrecioCoste() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">Precio venta al público:</td>
                    <td class="col-lg-6"><?php echo $producto->getPvp() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">Descripción:</td>
                    <td class="col-lg-6"><?php echo $producto->getDescripcion() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">Código de barras:</td>
                    <td class="col-lg-6"><?php echo $producto->getCodigoBarras() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">Proveedor:</td>
                    <td class="col-lg-6"><?php
                        if ($producto->getIdProveedor() == 0) {
                            echo "Sin rellenar";
                        } else {
                            echo $producto->getIdProveedor();
                        }
                        ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">Stock Actual:</td>
                    <td class="col-lg-6"><?php echo $producto->getStockActual() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">Stock Mínimo:</td>
                    <td class="col-lg-6"><?php echo $producto->getStockMinimo() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">Stock Máximo:</td>
                    <td class="col-lg-6"><?php echo $producto->getStockMaximo() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6 col-md-6 col-sm-12"></td>
                    <td class="col-lg-6 col-md-6 col-sm-12">
                        <a class="btn btn-primary" href="IndexAdmin.php?principal=formEditProducto.php">Editar</a>
                    </td>
                </tr>

            </table>
        </blockquote>
    </div>
</div>
<div class="card">
    <div class="card-header">
        Actualizar stock
    </div>
    <div class="card-body">
        <?php
        if ($errorStock != "") {
            echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> " . $errorStock . "</div>";
        } elseif ($successStock != "") {
            echo "<div class=\"alert alert-success\"><strong>Información</strong> " . $successStock . "</div>";
        }
        ?>
        <blockquote class="blockquote mb-0">
            <table class="table">
                <tr>
                    <td class="col-lg-6">Stock Actual</td>
                    <td class="col-lg-6">
                    <form name="formularioModStock" method="POST"
                        action="IndexAdmin.php?principal=datosProductos.php" >
                        <input type="number" step="1" name="stockActual" required min="0"
                            <?php echo 'value="' . $producto->getStockMaximo() . '"' ?>>
                        <input class="btn btn-info" type="submit" name="editDirection" value="Actualizar">
                     </form>
                      </td>
                    </tr>
            </table>
        </blockquote>
    </div>
</div>

</body>
</html>