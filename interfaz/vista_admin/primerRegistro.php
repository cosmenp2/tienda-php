<?php
require_once('../../rutas.php');
require_once "../../" . PERSISTENCIA . "Usuarios.php";
require_once "../../" . POJOS . "Usuario.php";
ini_set('session.use_strict_mode', 1);
session_id($_COOKIE['idSesion']);
session_start();

$direccion = "Location:../../IndexInicial.php?error=";
$error = "";
/** @var Usuarios $tUsuario */
$tUsuario = Usuarios::singletonUsuarios();
$isFirstTime = false;
if (!$tUsuario->existAdmin()) {
    $isFirstTime = true;
}

if (isset($_POST['username']) && $isFirstTime) {
    $direccion = "Location:../../IndexInicial.php?error=";
    $login = $_POST['username'];
    $password = $_POST['password'];
    $passwordRep = $_POST['passwordRepite'];
    $_SESSION['form'] = $login;

    /** @var Usuarios $tUsuario */
    $tUsuario = Usuarios::singletonUsuarios();

    if ($password != $passwordRep) {
        $error .= "Las%20contraseñas%20no%20coinciden.";
    }

    if (empty($error)) {
        $securePass = hash("sha512", $password);
        $u = new Usuario(0, $login, 1, $login, $securePass, 1);


        $insertado = $tUsuario->addUnUsuario($u);
        if ($insertado) {
            $_SESSION['idAdmin'] = $login;
            $_SESSION['time'] = time();
            $direccion = "Location:IndexAdmin.php";
            unset($_SESSION['form']);
        } else {
            $error .= "Ha%20habido%20un%20problema%20al%20guardar%20sus%20datos.%20Vuelva%20a%20intentarlo%20mas%20tarde%20o%20compruebe%20que%20tenga%20acceso%20a%20la%20base%20de%datos.";
        }

    }
}
header($direccion . $error);