<?php
require_once('../../rutas.php');
require_once "../../".PERSISTENCIA . 'Empleados.php';
require_once "../../".PERSISTENCIA . "Usuarios.php";
require_once "../../".PERSISTENCIA . "DireccionesEmpleado.php";
require_once "../../".POJOS . "Empleado.php";
require_once "../../".POJOS . "DireccionEmpleado.php";
require_once "../../".POJOS . "Usuario.php";
ini_set('session.use_strict_mode', 1);
session_id($_COOKIE['idSesion']);
session_start();
/** @var Usuarios $tUsuario */
$tUsuario = Usuarios::singletonUsuarios();
$usuario = null;
$legitima = false;

if (isset($_SESSION['idAdmin'])) {
    $usuario = $tUsuario->buscaUsuario($_SESSION['idAdmin']);
    if (is_null($usuario)) {
        $_SESSION['idAdmin'] = "";
    } else {
        $_SESSION['time'] = time();
        $nombre = $usuario->getLogin();
        $legitima = true;
    }
}

$direccion = "Location:IndexAdmin.php?principal=formAltaEmpleado.php";
$error = "";
$success = "";

if (isset($_POST['nombre']) && $legitima) {
    $nombre = $_POST['nombre'];
    $apellido1 = $_POST['apellido1'];
    $apellido2 = (isset($_POST['apellido2'])) ? $_POST['apellido2'] : "";
    $nif = $_POST['nif'];
    $numCta = $_POST['numCta'];
    $movil = $_POST['movil'];
    $pais = $_POST['pais'];
    $provincia = $_POST['provincia'];
    $localidad = $_POST['localidad'];
    $codPostal = $_POST['codPostal'];
    $direccionEmpleado = $_POST['direccion'];
    $_SESSION['form'] = array("nombre"=>$nombre,"apellido1"=>$apellido1, "apellido2"=>$apellido2, "nif"=>$nif,
        "numCta"=>$numCta,"movil"=>$movil,"pais"=>$pais,"provincia"=>$provincia,"localidad"=>$localidad,"codPostal"=>$codPostal,
        "direccion"=>$direccionEmpleado);

    /** @var Empleados $tEmpleado */
    $tEmpleado = Empleados::singletonEmpleados();
    /** @var DireccionesEmpleado $tDE */
    $tDE = DireccionesEmpleado::singletonDireccionesEmpleado();

    if (!is_null($tEmpleado->getEmpleadoByNif($nif))){
        $error .= "&error=Ya%20existe%20un%20empleado%20con%20el%20mismo%20DNI.%20";
    }

    if (empty($error)){
        $nombreOriginal = $_FILES['foto']['name'];
        $posPunto = strpos($nombreOriginal, ".") + 1;
        $extensionOriginal = substr($nombreOriginal, $posPunto, 3);

        $tipo = $_FILES['foto']['type'];
        $tamanio = $_FILES['foto']['size'];

        $id = $tEmpleado->getUltimoCodigo($codPostal) + 1; //Averiguamos cuál es el id del último producto
        $codigoString = (string) $id;

        $numCaracteres = strlen($codigoString);
        $resta = 3 - $numCaracteres;
        for ($i = 1; $i <= $resta; $i++) {
            $codigoString = '0' . $codigoString;
        }
        $codigoFormateado = $codPostal . $codigoString;

        $nombreNuevo = "empleados/emp_" . $codigoFormateado . "." . $extensionOriginal;

        $fallo = move_uploaded_file($_FILES['foto']['tmp_name'], "../../".FOTOS.$nombreNuevo);

        if ($fallo != 0) {
            $e = new Empleado(0, $codigoFormateado, 0, $nif, $nombre, $apellido1, $apellido2, $numCta, $movil,
                $nombreNuevo, 1);
            $dE = new DireccionEmpleado(0,$codigoFormateado,$direccionEmpleado,$localidad,$codPostal,$provincia,1,$pais);
            $insertado = $tEmpleado->addUnEmpleado($e) && $tDE->addUnaDireccion($dE);
            if ($insertado) {
                unset($_SESSION['form']);
                $success .= "&success=Se%20ha%20realizado%20el%20alta%20correctamente";
            } else {
                $error .= "&error=Ha%20habido%20un%20problema%20al%20guardar%20los%20datos%20del%20empleado.";
            }

        } else {
            $error .= "&error=Ha%20habido%20un%20problema%20al%20guardar%20la%20foto%20de%20perfil%20del%20empleado";
        }
    }

}
header($direccion.$error.$success);