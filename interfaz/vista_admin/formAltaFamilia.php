<?php
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Alta Familias (Categorias)</title>
</head>
<h1 class="display-4">Alta de familias (Categorias)</h1>
<body>
<?php
if (isset($_GET['error'])) {
    echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> ".$_GET['error']."</div>";
}
if (isset($_GET['success'])) {
    echo "<div class=\"alert alert-success\"><strong>Información</strong> " . $_GET['success'] . "</div>";
}
if (isset($_SESSION['form'])) {
    $nombre = $_SESSION['form']['nombre'];
    $descripcion = (isset($_SESSION['form']['descripcion'])) ? $_SESSION['form']['descripcion'] : "";
} else {
    $nombre = "";
    $descripcion = "";
}
?>
<div class="form-group table-responsive">

    <form name="formularioAltaFamiliaAdmin" method="POST"
          action="altaFamilia.php">
        <table class="table">
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Nombre*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="nombre" max="20" required <?php echo 'value="'.$nombre.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Descripcion:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="descripcion" <?php echo 'value="'.$descripcion.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12"><input class="btn btn-danger" type="reset" name="reset" value="Borrar todo"></td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input class="btn btn-primary" type="submit" name="alta" value="Realizar Alta"></td>
            </tr>
        </table>
    </form>
    <p class="p">Los campos con * se deben rellenar obligatoriamente</p>
</div>

</body>
</html>