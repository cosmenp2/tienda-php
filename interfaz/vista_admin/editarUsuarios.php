<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Datos de los usuarios</title>
</head>
<body>
<?php
/** @var Usuarios $tUsuarios */
$tUsuarios = Usuarios::singletonUsuarios();
/** @var Roles $tRoles */
$tRoles = Roles::singletonRoles();


$error = "";
$success = "";

if (isset($_POST['usuario'])){
    $u = $tUsuarios->buscaUsuarioWithoutFilter($_POST['usuario']);
    $securePassword = hash("sha512",$_POST['password']);
    if ($tUsuarios->changePassword($u->getLogin(),$securePassword,$u->getPassword())){
        $success = "Se ha cambiado la contraseña correctamente";
    } else {
        $error = "Ha habido un error al cambiar la contraseña";
    }
} elseif (isset($_POST['idDesactivar'])){
    $u = $tUsuarios->buscaUsuarioWithoutFilter($_POST['idDesactivar']);
    if ($usuario->getIdUsuario() != $u->getIdUsuario()){
        if ($tUsuarios->desactivarUsuario($u->getIdUsuario())){
            $success = "Se ha descativado el usuario correctamente";
        } else {
            $error = "Ha habido un error al descativar el usuario";
        }
    } else {
        $error = "No puede desactivarse a si mismo";
    }
} elseif (isset($_POST['idActivar'])){
    $u = $tUsuarios->buscaUsuarioWithoutFilter($_POST['idActivar']);
    if ($tUsuarios->activarUsuario($u->getIdUsuario())){
        $success = "Se ha ativado el usuario correctamente";
    } else {
        $error = "Ha habido un error al ativar el usuario";
    }
}

if (!empty($error)) {
    echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> ".$error."</div>";
} elseif (!empty($success)){
    echo "<div class=\"alert alert-success\">".$success."</div>";
}

$usuarios = $tUsuarios->getUsuariosTodos();
if (sizeof($usuarios) > 0) {
    echo '
<table class="table">
    <tr>
        <th class="col-lg-2 col-md-2 col-sm-6">Rol</th>            
        <th class="col-lg-3 col-md-3 col-sm-6">Nombre</th>            
        <th class="col-lg-5 col-md-5 col-sm-6">Cambiar contraseña</th>            
        <th class="col-lg-2 col-md-2 col-sm-6">Acción</th>    
    </tr>';
    /** @var Usuario $u */
    foreach ($usuarios as $u) {
        $rol = $tRoles->buscaRolById($u->getIdRol());
        echo '
    <tr>
        <td class="col-lg-2 col-md-2 col-sm-6">' . $rol->getNombre() . '</td>            
        <td class="col-lg-3 col-md-3 col-sm-6">' . $u->getLogin() . '</td>
        <td class="col-lg-5 col-md-5 col-sm-6">
        <form method="post" action="IndexAdmin.php?principal=editarUsuarios.php">
            <input type="password" name="password" min="3" max="64" required> 
            <input type="hidden" name="usuario" value="' . $u->getLogin() . '">
            <input class="btn btn-info" type="submit" name="editarUsuario" value="Cambiar">
</form>
        </td>';
        if ($u->getActivo() == 1) {
            if ($usuario->getIdUsuario() != $u->getIdUsuario())
                echo '<td class="col-lg-2 col-md-2 col-sm-6">
            <form method="post" action="IndexAdmin.php?principal=editarUsuarios.php">
            <input type="hidden" name="idDesactivar" value="' . $u->getLogin() . '">
            <input class="btn btn-warning" type="submit" name="descativarUsuario" value="Desactivar">
</form>
        </td>           
    </tr>';
            else
                echo '<td class="col-lg-2 col-md-2 col-sm-6">
            <input class="btn btn-warning" value="Usuario Actual" disabled>
        </td>           
    </tr>';
        } else {
            echo '<td class="col-lg-2 col-md-2 col-sm-6">
            <form method="post" action="IndexAdmin.php?principal=editarUsuarios.php">
            <input type="hidden" name="idActivar" value="' . $u->getLogin() . '">
            <input class="btn btn-warning" type="submit" name="activarUsuario" value="Activar">
</form>
        </td>           
    </tr>
        ';
        }
    }
    echo '</table>';
} else {
    echo "<h1 class=\"display-4\">No hay ningún usuario</h1>";
}
?>
</body>
</html>