<?php
require_once('../../rutas.php');
require_once "../../" . PERSISTENCIA . 'Productos.php';
require_once "../../" . PERSISTENCIA . "Usuarios.php";
require_once "../../" . POJOS . "Producto.php";
require_once "../../" . POJOS . "Usuario.php";
ini_set('session.use_strict_mode', 1);
session_id($_COOKIE['idSesion']);
session_start();
/** @var Usuarios $tUsuario */
$tUsuario = Usuarios::singletonUsuarios();
$usuario = null;
$legitima = false;

if (isset($_SESSION['idAdmin'])) {
    $usuario = $tUsuario->buscaUsuario($_SESSION['idAdmin']);
    if (is_null($usuario)) {
        $_SESSION['idAdmin'] = "";
    } else {
        $_SESSION['time'] = time();
        $nombre = $usuario->getLogin();
        $legitima = true;
    }
}

$direccion = "Location:IndexAdmin.php?principal=formEditProducto.php";
$error = "";
$success = "";

if (isset($_POST['idFamilia']) && $legitima) {
    $idFamilia = $_POST['idFamilia'];
    $tipoIva = $_POST['tipoIva'];
    $precioCoste = $_POST['precioCoste'];
    $pvp = $_POST['pvp'];
    $descripcion = $_POST['descripcion'];
    $codigoBarras = $_POST['codigoBarras'];
    $stockActual = $_POST['stockActual'];
    $stockMinimo = $_POST['stockMinimo'];
    $stockMaximo = $_POST['stockMaximo'];
    $_SESSION['form'] = array("idFamilia" => $idFamilia, "tipoIva" => $tipoIva, "precioCoste" => $precioCoste, "pvp" => $pvp,
        "descripcion" => $descripcion, "codigoBarras" => $codigoBarras, "stockActual" => $stockActual,
        "stockMinimo" => $stockMinimo, "stockMaximo" => $stockMaximo);

    /** @var Productos $tProductos */
    $tProductos = Productos::singletonProductos();
    $producto = $tProductos->getProductoById($_SESSION['tmpProducto']);
    $idProducto = $producto->getIdProducto();


    if (rtrim($_FILES['foto']['name']) != "") {
        $nombreOriginal = $_FILES['foto']['name'];
        $posPunto = strpos($nombreOriginal, ".") + 1;
        $extensionOriginal = substr($nombreOriginal, $posPunto, 3);
        $nombreNuevo = "productos/pro_" . $idProducto . "." . $extensionOriginal;

        $errUpload = move_uploaded_file($_FILES['foto']['tmp_name'], "../../" . FOTOS . $nombreNuevo);

        if ($errUpload == 0) {
            $error .= "&error=Ha%20habido%20un%20error%20al%20subir%20la%20foto.%20";
        } else {
            $rutaFoto = $nombreNuevo;
        }
    } else {
        $rutaFoto = $producto->getRutaFoto();
    }


    if (empty($error)) {
        $p = new Producto(0, $idProducto, $idFamilia,$tipoIva,$precioCoste, $pvp,$descripcion, $codigoBarras,
            0,$stockActual, $stockMinimo, $stockMaximo,$nombreNuevo, 1);
        $insertado = $tProductos->actualizarProducto($p);
        if ($insertado) {
            unset($_SESSION['form']);
            $direccion = "Location:IndexAdmin.php?principal=datosProductos.php";
            $success .= "&success=Se%20ha%20modificado%20el%20producto%20correctamente";
        } else {
            $error .= "&error=Ha%20habido%20un%20problema%20al%20guardar%20los%20datos%20del%20producto.";
        }
    }


}
header($direccion . $error . $success);