<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Listado de Empleado</title>
</head>
<body>
<h1>Listado de Empleados</h1>
<form name="formulario1" method="POST" action="IndexAdmin.php?principal=filtroEmpleados.php">
    <table class="table">
        <tr>
            <th>Propiedad</th>
            <th>Valor</th>
            <th></th>
        </tr>

        <tr>
            <td>Codigo Postal:</td>
            <td><input type="text" name="codPostal" required></td>
            <td><input class="btn btn-primary" type="submit" name="filtrar" value="Filtrar"></td>
        </tr>
    </table>
</form>

<?php

/** @var Empleados $tEmpleado */
$tEmpleado = Empleados::singletonEmpleados();
/** @var Pedidos $tPedidos */
$tPedidos = Pedidos::singletonPedidos();

$tablaPedidos = null;
$tablaEmpleado = null;
$empleados = null;
$empleados = $tEmpleado->getEmpleadosTodos();

echo "<form name=\"allEmpleados\" method=\"post\" action=\"../../" . PDF . "imprimirEmpleados.php\">
                <input class=\"btn btn-primary\" type=\"submit\" name=\"pdf\" value=\"Imprimir todos los empleados\">
            </form>";

if (isset($_POST['codPostal'])) {
    $tablaEmpleado = $tEmpleado->getByCodPostal($_POST['codPostal']);

    if (sizeof($tablaEmpleado) == 0) {
        echo "<div class=\"alert alert-warning\">No se ha encontrado ningún empleado con esta dirección postal</div>";
    } else {
        echo "<table class='table'>
			<tr>
				<td>IdEmpleado</td>
				<td>IdUsusario</td>
				<td>Nif</td>
				<td>Nombre</td>
				<td>Primer apellido</td>
				<td>Segundo apellido</td>
				<td>Numcta</td>
				<td>movil</td>
				<td>foto</td>
				<td>Activo</td>
			</tr>";
        /** @var Empleado $e */
        foreach ($tablaEmpleado as $e) {
            echo "<tr>";
            echo "<td>" . $e->getIdEmpleado() . "</td>";
            echo "<td>" . $e->getIdUsuario() . "</td>";
            echo "<td>" . $e->getNif() . "</td>";
            echo "<td>" . $e->getNombre() . "</td>";
            echo "<td>" . $e->getApellido1() . "</td>";
            echo "<td>" . $e->getApellido2() . "</td>";
            echo "<td>" . $e->getNumcta() . "</td>";
            echo "<td>" . $e->getMovil() . "</td>";
            echo "<td><img src=\"../../" . FOTOS . $e->getRutaFoto() . "\" border='0' width='150' height='100' /></td>";
            echo "<td>" . $e->getActivo() . "</td>";
            echo " </tr>";
        }
        echo "</table> <form name=\"formulario\" method=\"post\" action=\"../../" . PDF . "imprimirInfoPDF.php\">
                <input type='HIDDEN' name='codPostal' value='" . $_POST['codPostal'] . "'>
                <input class=\"btn btn-primary\" type=\"submit\" name=\"pdf\" value=\"Imprimir información\">
            </form>";
    }
}
?>

</body>
</html>
