<?php
require_once('../../rutas.php');
require_once "../../" . PERSISTENCIA . 'Productos.php';
require_once "../../" . PERSISTENCIA . "Usuarios.php";
require_once "../../" . POJOS . "Producto.php";
require_once "../../" . POJOS . "Usuario.php";
ini_set('session.use_strict_mode', 1);
session_id($_COOKIE['idSesion']);
session_start();
/** @var Usuarios $tUsuario */
$tUsuario = Usuarios::singletonUsuarios();
$usuario = null;
$legitima = false;

if (isset($_SESSION['idAdmin'])) {
    $usuario = $tUsuario->buscaUsuario($_SESSION['idAdmin']);
    if (is_null($usuario)) {
        $_SESSION['idAdmin'] = "";
    } else {
        $_SESSION['time'] = time();
        $idFamilia = $usuario->getLogin();
        $legitima = true;
    }
}

$direccion = "Location:IndexAdmin.php?principal=formAltaProducto.php";
$error = "";
$success = "";

if (isset($_POST['idFamilia']) && $legitima) {
    $idFamilia = $_POST['idFamilia'];
    $tipoIva = $_POST['tipoIva'];
    $precioCoste = $_POST['precioCoste'];
    $pvp = $_POST['pvp'];
    $descripcion = $_POST['descripcion'];
    $codigoBarras = $_POST['codigoBarras'];
    $stockActual = $_POST['stockActual'];
    $stockMinimo = $_POST['stockMinimo'];
    $stockMaximo = $_POST['stockMaximo'];
    $_SESSION['form'] = array("idFamilia" => $idFamilia, "tipoIva" => $tipoIva, "precioCoste" => $precioCoste, "pvp" => $pvp,
        "descripcion" => $descripcion, "codigoBarras" => $codigoBarras, "stockActual" => $stockActual,
        "stockMinimo" => $stockMinimo, "stockMaximo" => $stockMaximo);

    /** @var Productos $tProductos */
    $tProductos = Productos::singletonProductos();


    $nombreOriginal = $_FILES['foto']['name'];
    $posPunto = strpos($nombreOriginal, ".") + 1;
    $extensionOriginal = substr($nombreOriginal, $posPunto, 3);

    $tipo = $_FILES['foto']['type'];
    $tamanio = $_FILES['foto']['size'];
    $id = $tProductos->getUltimoProducto($idFamilia) + 1; //Averiguamos cuál es el id del último producto
    $codigoString = (string)$id;

    $numCaracteres = strlen($codigoString);
    $resta = 5 - $numCaracteres;
    for ($i = 1; $i <= $resta; $i++) {
        $codigoString = '0' . $codigoString;
    }

    $codigoFormateado = $idFamilia . $codigoString;

    $nombreNuevo = "productos/pro_" . $codigoFormateado . "." . $extensionOriginal;

    $fallo = move_uploaded_file($_FILES['foto']['tmp_name'], "../../" . FOTOS . $nombreNuevo);

    if ($fallo != 0) {
        $p = new Producto(0, $codigoFormateado, $idFamilia,$tipoIva,$precioCoste, $pvp,$descripcion, $codigoBarras,
            0,$stockActual, $stockMinimo, $stockMaximo,$nombreNuevo, 1);
        $insertado = $tProductos->altaProducto($p);
        if ($insertado) {
            unset($_SESSION['form']);
            $success .= "&success=Se%20ha%20añadido%20el%20producto%20correctamente";
        } else {
            $error .= "&error=Ha%20habido%20un%20problema%20al%20guardar%20los%20datos%20del%20producto.";
        }

    } else {
        $error .= "&error=Ha%20habido%20un%20problema%20al%20guardar%20la%20foto%20del%20producto.";
    }


}
header($direccion . $error . $success);