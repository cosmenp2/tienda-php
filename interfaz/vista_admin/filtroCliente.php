<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Buscar cliente</title>
</head>
<body>
<h1>Buscar clientes</h1>
<form name="formulario1" method="POST" action="IndexAdmin.php?principal=filtroCliente.php">
    <table class="table">
        <tr>
            <th>Propiedad</th>
            <th>Valor</th>
            <th>Ordenar</th>
        </tr>
        <tr>
            <td>Nombre:</td>
            <td><input type="text" name="nombre"></td>
            <td><select name="order[]">
                    <option value="">No ordenar</option>
                    <option value="1">1º</option>
                    <option value="2">2º</option>
                    <option value="3">3º</option>
                    <option value="4">4º</option>
                    <option value="5">5º</option>
                    <option value="6">6º</option>
                    <option value="7">7º</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Primer apellido:</td>
            <td><input type="text" name="apellido1"></td>
            <td>
                <select name="order[]">
                    <option value="">No ordenar</option>
                    <option value="1">1º</option>
                    <option value="2">2º</option>
                    <option value="3">3º</option>
                    <option value="4">4º</option>
                    <option value="5">5º</option>
                    <option value="6">6º</option>
                    <option value="7">7º</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Segundo apellido:</td>
            <td><input type="text" name="apellido2"></td>
            <td>
                <select name="order[]">
                    <option value="">No ordenar</option>
                    <option value="1">1º</option>
                    <option value="2">2º</option>
                    <option value="3">3º</option>
                    <option value="4">4º</option>
                    <option value="5">5º</option>
                    <option value="6">6º</option>
                    <option value="7">7º</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>NIF:</td>
            <td><input type="text" name="nif"></td>
            <td>
                <select name="order[]">
                    <option value="">No ordenar</option>
                    <option value="1">1º</option>
                    <option value="2">2º</option>
                    <option value="3">3º</option>
                    <option value="4">4º</option>
                    <option value="5">5º</option>
                    <option value="6">6º</option>
                    <option value="7">7º</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Sexo:</td>
            <td>H<input type="radio" name="sexo" value="hombre"> M<input type="radio" name="sexo" value="mujer">
                Ambos<input type="radio" name="sexo" value="ambos" checked="checked"></td>
            <td>
                <select name="order[]">
                    <option value="">No ordenar</option>
                    <option value="1">1º</option>
                    <option value="2">2º</option>
                    <option value="3">3º</option>
                    <option value="4">4º</option>
                    <option value="5">5º</option>
                    <option value="6">6º</option>
                    <option value="7">7º</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>NumCta:</td>
            <td><input type="text" name="numCta"></td>
            <td>
                <select name="order[]">
                    <option value="">No ordenar</option>
                    <option value="1">1º</option>
                    <option value="2">2º</option>
                    <option value="3">3º</option>
                    <option value="4">4º</option>
                    <option value="5">5º</option>
                    <option value="6">6º</option>
                    <option value="7">7º</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Como nos conocio:</td>
            <td><input type="text" name="cnc"></td>
            <td>
                <select name="order[]">
                    <option value="">No ordenar</option>
                    <option value="1">1º</option>
                    <option value="2">2º</option>
                    <option value="3">3º</option>
                    <option value="4">4º</option>
                    <option value="5">5º</option>
                    <option value="6">6º</option>
                    <option value="7">7º</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><input class="btn btn-danger" type="reset" name="reset" value="Borrar todo"></td>
            <td><input class="btn btn-primary" type="submit" name="filtrar" value="Filtrar"></td>
            <td>Historico<input type="checkbox" name="historico" value="hitorico"></td>
        </tr>
    </table>
</form>

<?php

if (isset($_POST['filtrar'])) {
    $nombres = array('nombre', 'apellido1', 'apellido2', 'nif', 'varon', 'numcta', 'como_nos_conocio');
    /** @var Clientes $tC */
    $tC = Clientes::singletonClientes();
    $tablaCliente = null;
    $nombre = (rtrim($_POST['nombre']) == "") ? null : $_POST['nombre'];
    $apellido1 = (rtrim($_POST['apellido1']) == "") ? null : $_POST['apellido1'];
    $apellido2 = (rtrim($_POST['apellido2']) == "") ? null : $_POST['apellido2'];
    $nif = (rtrim($_POST['nif']) == "") ? null : $_POST['nif'];
    $numCta = (rtrim($_POST['numCta']) == "") ? null : $_POST['numCta'];
    $cnc = (rtrim($_POST['cnc']) == "") ? null : $_POST['cnc'];
    $ordenacion = array();
    $valores = $_POST['order'];
    $valido = true;
    $i = 0;
    $impClientes = array();

    while ($valido && sizeof($valores) > $i) {
        $value = $valores[$i];
        if ($value != "") {
            if (!isset($ordenacion[$value])) {
                $ordenacion[$value] = $nombres[$i];
            } else {
                $valido = false;
                echo "<div class=\"alert alert-danger\">No puede asignar dos valores iguales en ordenar</div>";
            }
        }
        $i = $i + 1;
    }

    if ($_POST['sexo'] == "hombre") {
        $sexo = 1;
    } elseif ($_POST['sexo'] == "mujer") {
        $sexo = 0;
    } else {
        $sexo = null;
    }
    if (isset($_POST['historico'])) {
        $activo = null;
    } else {
        $activo = 1;
    }

    $relleno = (!is_null($nombre) || !is_null($apellido1) || !is_null($apellido2) || !is_null($nif) || !is_null($numCta) || !is_null($cnc) || !is_null($sexo) || !is_null($activo));

    if (!sizeof($ordenacion) > 0) {
        $valido = false;
    }

    if ($relleno) {
        $c = new Cliente(null, null, null, $nombre, $apellido1, $apellido2, $nif, $sexo,
            $numCta, $cnc, $activo);
        if ($valido) {
            $tablaCliente = $tC->buscarCliente($c, $ordenacion);
        } else {
            $tablaCliente = $tC->buscarCliente($c);
        }
    } else {
        if ($valido) {
            $tablaCliente = $tC->getClientesTodos($ordenacion);
        } else {
            $tablaCliente = $tC->getClientesTodos();
        }
    }

    if (!is_null($tablaCliente)) {
        echo "<table class='table'>
			<tr>
				<td>IdCliente</td>
				<td>IdUsusario</td>
				<td>Nombre</td>
				<td>Primer apellido</td>
				<td>Segundo apellido</td>
				<td>Nif</td>
				<td>Sexo</td>
				<td>Numcta</td>
				<td>Como nos conoció</td>
				<td>Activo</td>
			</tr>";
        foreach ($tablaCliente as $c) {
            array_push($impClientes, $c->getIdCliente());
            echo "<tr>";
            echo "<td>" . $c->getIdCliente() . "</td>";
            echo "<td>" . $c->getIdUsuario() . "</td>";
            echo "<td>" . $c->getNombre() . "</td>";
            echo "<td>" . $c->getApellido1() . "</td>";
            echo "<td>" . $c->getApellido2() . "</td>";
            echo "<td>" . $c->getNif() . "</td>";
            $valor = ($c->getVaron()) ? "Hombre" : "Mujer";
            echo "<td>" . "$valor" . "</td>";
            echo "<td>" . $c->getNumcta() . "</td>";
            echo "<td>" . $c->getComoNosConocio() . "</td>";
            echo "<td>" . $c->getActivo() . "</td>";
            echo " </tr>";
        }
        echo "</table>";
        echo "<form action='../../" . PDF . "imprimirClientes.php' name='formulario' method='post'>";
        foreach ($impClientes as $i) {
            echo "<input type='HIDDEN' name='clientes[]' value='" . $i . "'/>";
        }
        echo "<input class=\"btn btn-primary\" type=\"submit\" name=\"imprimir\" value=\"Imprimir clientes\">";
        echo "</form>";
    }
}
?>

</body>
</html>