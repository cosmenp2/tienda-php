<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Listado de pedidos empaquetados por un empleado</title>
</head>
<body>
<h1>Listado de pedidos empaquetados por un empleado</h1>
<form name="formulario1" method="POST" action="IndexAdmin.php?principal=listadoPedidosEmpleados.php">
    <table class="table">
        <tr>
            <th>Propiedad</th>
            <th>Valor</th>
            <th></th>
        </tr>

        <tr>
            <td>Empleado:</td>
            <td><select name="idEmpleado" required>
                    <option value=""></option>
                <?php
                /** @var Empleados $tEmpleado */
                $tEmpleado = Empleados::singletonEmpleados();
                /** @var Pedidos $tPedidos */
                $tPedidos = Pedidos::singletonPedidos();

                $tablaPedidos = null;
                $tablaEmpleado = null;
                $empleados = null;
                $empleados = $tEmpleado->getEmpleadosTodos();
                /** @var Empleado $c */
                foreach ($empleados as $c) {
                    echo "<option value='" . $c->getIdEmpleado() . "'>" . $c->getNombre() . " " . $c->getApellido1() . " " . $c->getApellido2() .
                        "</option>";
                }
                ?>
                </select>
            </td>
            <td><input class="btn btn-primary" type="submit" name="filtrar" value="Ver pedidos empaquetados"></td>
        </tr>
    </table>
</form>

<?php

if (isset($_POST['idEmpleado'])) {
    $tablaPedidos = $tPedidos->getPedidosEmpaquetados($_POST['idEmpleado']);
    if (sizeof($tablaPedidos) == 0) {
        echo "<div class=\"alert alert-warning\">Este empleado no ha empaquetado ningún pedido</div>";
    } else {
        echo "<table class='table'>
			<tr>
				<td>Empresa de transporte</td>
				<td>Fecha del pedido</td>
				<td>Fecha de envio</td>
				<td>Fecha de entrega</td>
				<td>Facturado</td>
				<td>Factura</td>
				<td>Fecha factura</td>
				<td>Pagado</td>
				<td>Fecha pago</td>
				<td>Metodo de pago</td>
				<td>Cliente</td>
			</tr>";

        foreach ($tablaPedidos as $p) {
            $empresaTrans = "";
            switch ($p->getIdEmpresaTransporte()){
                case 0:
                    $empresaTrans = "No asignada";
                    break;
                case 1:
                    $empresaTrans = "Seur";
                    break;
                case 2:
                    $empresaTrans = "DHL";
                    break;
                case 3:
                    $empresaTrans = "Correos";
                    break;
                case 4:
                    $empresaTrans = "MRV";
                    break;
            }
            echo "<tr>";
            echo "<td>" . $empresaTrans . "</td>";
            echo "<td>" . $p->getFechaPedido() . "</td>";
            echo "<td>" . $p->getFechaEnvio() . "</td>";
            echo "<td>" . $p->getFechaEntrega() . "</td>";
            echo "<td>" . (($p->getFacturado() == 1) ? "Si" : "No") . "</td>";
            echo "<td>" . $p->getIdFactura() . "</td>";
            echo "<td>" . $p->getFechaFactura() . "</td>";
            echo "<td>" . (($p->getPagado() == 1) ? "Si" : "No") . "</td>";
            echo "<td>" . $p->getFechaPago() . "</td>";
            echo "<td>" . $p->getMetodoPago() . "</td>";
            echo "<td>" . $p->getIdCliente() . "</td>";
            echo " </tr>";
        }
        echo "</table>";
    }
}
?>

</body>
</html>
