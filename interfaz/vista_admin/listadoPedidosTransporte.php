<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Listado de pedidos por empresa de transporte</title>
</head>
<body>
<h1>Listado de pedidos por empresa de transporte</h1>
<form name="formulario1" method="POST" action="IndexAdmin.php?principal=listadoPedidosTransporte.php">
    <table class="table">
        <tr>
            <th>Propiedad</th>
            <th>Valor</th>
            <th></th>
        </tr>

        <tr>
            <td>Empresa:</td>
            <td><select name="idTransport" required>
                    <option value=""></option>
                    <option value="0">No asignada</option>
                    <option value="1">Seur</option>
                    <option value="2">DHL</option>
                    <option value="3">Correos</option>
                    <option value="4">MRV</option>
                </select>
            <td><input class="btn btn-primary" type="submit" name="filtrar" value="Filtrar"></td>
        </tr>
    </table>
</form>

<?php
/** @var Pedidos $tPedidos */
$tPedidos = Pedidos::singletonPedidos();
$tablaPedidos = null;
if (isset($_POST['idTransport'])) {
    $tablaPedidos = $tPedidos->getPedidosByTransport($_POST['idTransport']);
    if (sizeof($tablaPedidos) == 0) {
        echo "<div class=\"alert alert-warning\">Esta empresa no ha enviado ningún pedido</div>";
    } else {
        echo "<table class='table'>
			<tr>
				<td>Fecha del pedido</td>
				<td>Fecha de envio</td>
				<td>Fecha de entrega</td>
				<td>Facturado</td>
				<td>Factura</td>
				<td>Fecha factura</td>
				<td>Pagado</td>
				<td>Fecha pago</td>
				<td>Metodo de pago</td>
				<td>Cliente</td>
			</tr>";

        foreach ($tablaPedidos as $p) {
            echo "<tr>";
            echo "<td>" . $p->getFechaPedido() . "</td>";
            echo "<td>" . $p->getFechaEnvio() . "</td>";
            echo "<td>" . $p->getFechaEntrega() . "</td>";
            echo "<td>" . (($p->getFacturado() == 1) ? "Si" : "No") . "</td>";
            echo "<td>" . $p->getIdFactura() . "</td>";
            echo "<td>" . $p->getFechaFactura() . "</td>";
            echo "<td>" . (($p->getPagado() == 1) ? "Si" : "No") . "</td>";
            echo "<td>" . $p->getFechaPago() . "</td>";
            echo "<td>" . $p->getMetodoPago() . "</td>";
            echo "<td>" . $p->getIdCliente() . "</td>";
            echo " </tr>";
        }
        echo "</table>";
    }
}
?>

</body>
</html>
