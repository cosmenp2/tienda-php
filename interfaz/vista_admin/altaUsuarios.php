<?php
/** @var Clientes $tClientes */
$tClientes = Clientes::singletonClientes();
/** @var Empleados $tEmpleados */
$tEmpleados = Empleados::singletonEmpleados();
/** @var Usuarios $tUsuarios */
$tUsuarios = Usuarios::singletonUsuarios();

$errorClient = "";
$errorEmployer = "";
$errorUser = "";
$successClient = "";
$successEmployer = "";
$successUser = "";

if (isset($_POST['cliente'])){
    $cliente = $tClientes->getUnCliente($_POST['cliente']);
    $login = $_POST['login'];
    $password = $_POST['password'];
    if (is_null($tUsuarios->buscaUsuario($login))){
        $securePass = hash("sha512",$password);
        $cliente ->setIdUsuario($login);
        $u = new Usuario(0,$login,3,$login,$securePass,1);
        $insertado = $tClientes->modificarCliente($cliente) && $tUsuarios->addUnUsuario($u);
        if ($insertado) {
            $successClient = "Se ha añadido el usuario correctamente";
        } else {
            $errorClient = "Ha habido un error al añadir el usuario";
        }
    } else {
        $errorClient = "Ya existe un usuario con ese nombre";
    }
} elseif (isset($_POST['empleado'])){
    $empleado = $tEmpleados->getEmpleado($_POST['empleado']);
    $login = $_POST['login'];
    $password = $_POST['password'];
    if (is_null($tUsuarios->buscaUsuario($login))){
        $securePass = hash("sha512",$password);
        $empleado ->setIdUsuario($login);
        $u = new Usuario(0,$login,2,$login,$securePass,1);
        $insertado = $tEmpleados->actualizarEmpleado($empleado) && $tUsuarios->addUnUsuario($u);
        if ($insertado) {
            $successEmployer = "Se ha añadido el usuario correctamente";
        } else {
            $errorEmployer = "Ha habido un error al añadir el usuario";
        }
    } else {
        $errorEmployer = "Ya existe un usuario con ese nombre";
    }

} elseif (isset($_POST['login'])){
    $login = $_POST['login'];
    $password = $_POST['password'];
    if (is_null($tUsuarios->buscaUsuario($login))){
        $securePass = hash("sha512",$password);
        $u = new Usuario(0,$login,1,$login,$securePass,1);
        $insertado = $tUsuarios->addUnUsuario($u);
        if ($insertado) {
            $successUser = "Se ha añadido el usuario correctamente";
        } else {
            $errorUser = "Ha habido un error al añadir el usuario";
        }
    } else {
        $errorUser = "Ya existe un usuario con ese nombre";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Alta de usuarios</title>
</head>
<body>
<h1>Alta de usuarios</h1>
<form name="formulario1" method="POST" action="IndexAdmin.php?principal=altaUsuarios.php">
    <?php
    if (!empty($errorClient)) {
        echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> ".$errorClient."</div>";
    } elseif (!empty($successClient)){
        echo "<div class=\"alert alert-success\">".$successClient."</div>";
    }
    ?>
    <table class="table">
        <tr>
            <th class="col-lg-6 col-md-6 col-sm-6">Usuarios con perfil de clientes</th>
            <th class="col-lg-6 col-md-6 col-sm-6"></th>
        </tr>

        <tr>
            <td class="col-lg-6 col-md-6 col-sm-6">Cliente:</td>
            <td class="col-lg-6 col-md-6 col-sm-6">
                <select name="cliente" required>
                    <option value=""></option>
                    <?php
                    $clientes = $tClientes->getClientesWithoutUser();
                    /** @var Cliente $c */
                    foreach ($clientes as $c) {
                        echo "<option value='" . $c->getIdCliente() . "'>" . $c->getNif() . " " . $c->getNombre() . "</option>";
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="col-lg-6 col-md-6 col-sm-6">Nombre de usuario:</td>
            <td class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="login" max="20" min="3" required</td>
        </tr>
        <tr>
            <td class="col-lg-6 col-md-6 col-sm-6">Contraseña:</td>
            <td class="col-lg-6 col-md-6 col-sm-6"><input type="password" name="password" min="3" max="64" required>
            </td>
        </tr>
        <tr>
            <td class="col-lg-6 col-md-6 col-sm-6"></td>
            <td class="col-lg-6 col-md-6 col-sm-6"><input class="btn btn-primary" type="submit" name="registrar"
                                                          value="Registrar"></td>
        </tr>
    </table>
</form>

<form name="formulario2" method="POST" action="IndexAdmin.php?principal=altaUsuarios.php">
    <?php
    if (!empty($errorEmployer)) {
        echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> ".$errorEmployer."</div>";
    } elseif (!empty($successEmployer)){
        echo "<div class=\"alert alert-success\">".$successEmployer."</div>";
    }
    ?>
    <div class="alert alert-warning">El usuario no podrá iniciar sesión ya que la web no tiene un espacio para los empleados</div>
    <table class="table">
        <tr>
            <th class="col-lg-6 col-md-6 col-sm-6">Usuarios con perfil de empleado</th>
            <th class="col-lg-6 col-md-6 col-sm-6"></th>
        </tr>

        <tr>
            <td class="col-lg-6 col-md-6 col-sm-6">Empleado:</td>
            <td class="col-lg-6 col-md-6 col-sm-6">
                <select name="empleado" required>
                    <option value=""></option>
                    <?php
                    $empleados = $tEmpleados->getEmpleadosWithoutUser();
                    /** @var Empleado $e */
                    foreach ($empleados as $e) {
                        echo "<option value='" . $e->getIdEmpleado() . "'>" . $e->getNif() . " " . $e->getNombre() . "</option>";
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="col-lg-6 col-md-6 col-sm-6">Nombre de usuario:</td>
            <td class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="login" max="20" min="3" required</td>
        </tr>
        <tr>
            <td class="col-lg-6 col-md-6 col-sm-6">Contraseña:</td>
            <td class="col-lg-6 col-md-6 col-sm-6"><input type="password" name="password" min="3" max="64" required>
            </td>
        </tr>
        <tr>
            <td class="col-lg-6 col-md-6 col-sm-6"></td>
            <td class="col-lg-6 col-md-6 col-sm-6"><input class="btn btn-primary" type="submit" name="registrar"
                                                          value="Registrar"></td>
        </tr>
    </table>
</form>
<form name="formulario1" method="POST" action="IndexAdmin.php?principal=altaUsuarios.php">
    <?php
    if (!empty($errorUser)) {
        echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> ".$errorUser."</div>";
    } elseif (!empty($successUser)){
        echo "<div class=\"alert alert-success\">".$successUser."</div>";
    }
    ?>
    <table class="table">
        <tr>
            <th class="col-lg-6 col-md-6 col-sm-6">Usuarios con perfil de administradores</th>
            <th class="col-lg-6 col-md-6 col-sm-6"></th>
        </tr>
        <tr>
            <td class="col-lg-6 col-md-6 col-sm-6">Nombre de usuario:</td>
            <td class="col-lg-6 col-md-6 col-sm-6"><input type="text" name="login" max="20" min="3" required</td>
        </tr>
        <tr>
            <td class="col-lg-6 col-md-6 col-sm-6">Contraseña:</td>
            <td class="col-lg-6 col-md-6 col-sm-6"><input type="password" name="password" min="3" max="64" required>
            </td>
        </tr>
        <tr>
            <td class="col-lg-6 col-md-6 col-sm-6"></td>
            <td class="col-lg-6 col-md-6 col-sm-6"><input class="btn btn-primary" type="submit" name="registrar"
                                                          value="Registrar"></td>
        </tr>
    </table>
</form>
</body>
</html>
