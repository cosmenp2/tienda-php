<?php
require_once('../../rutas.php');
require_once "../../" . PERSISTENCIA . 'FamiliasProductos.php';
require_once "../../" . PERSISTENCIA . "Usuarios.php";
require_once "../../" . POJOS . "FamiliaProducto.php";
require_once "../../" . POJOS . "Usuario.php";
ini_set('session.use_strict_mode', 1);
session_id($_COOKIE['idSesion']);
session_start();
/** @var Usuarios $tUsuario */
$tUsuario = Usuarios::singletonUsuarios();
$usuario = null;
$legitima = false;

if (isset($_SESSION['idAdmin'])) {
    $usuario = $tUsuario->buscaUsuario($_SESSION['idAdmin']);
    if (is_null($usuario)) {
        $_SESSION['idAdmin'] = "";
    } else {
        $_SESSION['time'] = time();
        $nombre = $usuario->getLogin();
        $legitima = true;
    }
}

$direccion = "Location:IndexAdmin.php?principal=formAltaFamilia.php";
$error = "";
$success = "";

if (isset($_POST['nombre']) && $legitima) {
    $nombre = $_POST['nombre'];
    $descripcion = (isset($_POST['descripcion'])) ? $_POST['descripcion'] : "";
    $_SESSION['form'] = array("nombre" => $nombre, "descripcion" => $descripcion);
    /** @var FamiliasProductos $tFamilia */
    $tFamilia = FamiliasProductos::singletonFamiliasProductos();

    $newID = $tFamilia->generarID();
    $f = new FamiliaProducto(0, $newID, $nombre, $descripcion, 1);

    $insertado = $tFamilia->altaFamiliaProducto($f);
    if ($insertado) {
        unset($_SESSION['form']);
        $success .= "&success=Se%20ha%20realizado%20el%20alta%20correctamente";
    } else {
        $error .= "&error=Ha%20habido%20un%20problema%20al%20guardar%20los%20datos%20de%20la%20familia.";
    }
}
header($direccion . $error . $success);