<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Datos de las familias</title>
</head>
<body>
<?php
if (isset($_GET['error'])) {
    echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> ".$_GET['error']."</div>";
}
if (isset($_GET['success'])) {
    echo "<div class=\"alert alert-success\"><strong>Información</strong> " . $_GET['success'] . "</div>";
}
/** @var FamiliasProductos $tFamilias */
$tFamilias = FamiliasProductos::singletonFamiliasProductos();

$_SESSION['urlAnteriorAdmin'] = 'IndexAdmin.php?principal=editarFamilias.php';
if (isset($_SESSION['tmpFamilia'])){
    unset($_SESSION['tmpFamilia']);
}

$familias = $tFamilias->getFamiliaProductoTodos();
if (sizeof($familias) > 0) {
    echo '
<table class="table">
    <tr>
        <th class="col-lg-2 col-md-2 col-sm-6">Id Familia</th>            
        <th class="col-lg-3 col-md-3 col-sm-6">Nombre</th>            
        <th class="col-lg-5 col-md-5 col-sm-6">Descripcion</th>            
        <th class="col-lg-2 col-md-2 col-sm-6">Editar</th>    
    </tr>';
    /** @var FamiliaProducto $f */
    foreach ($familias as $f) {
        echo '
    <tr>
        <td class="col-lg-2 col-md-2 col-sm-6">' . $f->getIdFamilia() . '</td>            
        <td class="col-lg-3 col-md-3 col-sm-6">' . $f->getNombre() . '</td>
        <td class="col-lg-5 col-md-5 col-sm-6">' . $f->getDescripcion() . '</td>
        <td class="col-lg-2 col-md-2 col-sm-6">
            <form method="post" action="IndexAdmin.php?principal=formEditFamilia.php">
            <input type="hidden" name="familia" value="' . $f->getIdFamilia() . '">
            <input class="btn btn-info" type="submit" name="editarFamilia" value="Editar">
</form>
        </td>           
    </tr>
        ';
    }

    echo '</table>';
} else {
    echo "<h1 class=\"display-4\">No hay ninguna familia</h1>";
}
?>
</body>
</html>