<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Listado de pedidos de un cliente</title>
</head>
<body>
<h1>Listado de pedidos de un cliente</h1>
<form name="formulario1" method="POST" action="IndexAdmin.php?principal=listadoPedidosClientes.php">
    <table class="table">
        <tr>
            <th>Propiedad</th>
            <th>Valor</th>
            <th></th>
        </tr>

        <tr>
            <td>Cliente:</td>
            <td><select name="idCliente" required>
                    <option value=""></option>
                <?php
                /** @var Clientes $tCliente */
                $tCliente = Clientes::singletonClientes();
                /** @var Pedidos $tPedidos */
                $tPedidos = Pedidos::singletonPedidos();

                $tablaPedidos = null;
                $tablaCliente = null;
                $clientes = null;
                $clientes = $tCliente->getClientesTodos();

                /** @var Cliente $c */
                foreach ($clientes as $c) {
                    echo "<option value='" . $c->getIdCliente() . "'>" . $c->getNombre() . " " . $c->getApellido1() . " " . $c->getApellido2() .
                        "</option>";
                }
                ?>
                </select>
            <td><input class="btn btn-primary" type="submit" name="filtrar" value="Filtrar"></td>
        </tr>
    </table>
</form>

<?php
if (isset($_POST['idCliente'])) {
    $tablaPedidos = $tPedidos->getPedidosByCliente($_POST['idCliente']);
    if (sizeof($tablaPedidos) == 0) {
        echo "<div class=\"alert alert-warning\">Este cliente no ha realizado ningún pedido</div>";
    } else {
        echo "<table class='table'>
			<tr>
				<td>Empresa de transporte</td>
				<td>Fecha del pedido</td>
				<td>Fecha de envio</td>
				<td>Fecha de entrega</td>
				<td>Facturado</td>
				<td>Factura</td>
				<td>Fecha factura</td>
				<td>Pagado</td>
				<td>Fecha pago</td>
				<td>Metodo de pago</td>
				<td>Cliente</td>
			</tr>";

        foreach ($tablaPedidos as $p) {
            $empresaTrans = "";
            switch ($p->getIdEmpresaTransporte()){
                case 0:
                    $empresaTrans = "No asignada";
                    break;
                case 1:
                    $empresaTrans = "Seur";
                    break;
                case 2:
                    $empresaTrans = "DHL";
                    break;
                case 3:
                    $empresaTrans = "Correos";
                    break;
                case 4:
                    $empresaTrans = "MRV";
                    break;
            }
            echo "<tr>";
            echo "<td>" . $empresaTrans . "</td>";
            echo "<td>" . $p->getFechaPedido() . "</td>";
            echo "<td>" . $p->getFechaEnvio() . "</td>";
            echo "<td>" . $p->getFechaEntrega() . "</td>";
            echo "<td>" . (($p->getFacturado() == 1) ? "Si" : "No") . "</td>";
            echo "<td>" . $p->getIdFactura() . "</td>";
            echo "<td>" . $p->getFechaFactura() . "</td>";
            echo "<td>" . (($p->getPagado() == 1) ? "Si" : "No") . "</td>";
            echo "<td>" . $p->getFechaPago() . "</td>";
            echo "<td>" . $p->getMetodoPago() . "</td>";
            echo "<td>" . $p->getIdCliente() . "</td>";
            echo " </tr>";
        }
        echo "</table>";
    }
}
?>

</body>
</html>
