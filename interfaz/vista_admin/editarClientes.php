<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Datos del usuario</title>
</head>
<body>
<?php
if (isset($_GET['errorDir'])) {
    echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> ".$_GET['errorDir']."</div>";
}
if (isset($_GET['successDir'])) {
    echo "<div class=\"alert alert-success\"><strong>Información</strong> " . $_GET['successDir'] . "</div>";
}
/** @var Clientes $tClientes */
$tClientes = Clientes::singletonClientes();
/** @var DireccionesCliente $tDC */
$tDC = DireccionesCliente::singletonDireccionesCliente();
$_SESSION['urlAnteriorAdmin'] = 'IndexAdmin.php?principal=editarClientes.php';
if (isset($_SESSION['tmpClient'])){
    unset($_SESSION['tmpClient']);
}

if (isset($_POST['idEliminar'])) {
    if ($tDC->desactivarDireccion($_POST['idEliminar'])) {
        $successDir = "La direccion se ha eliminado correctamente";
    } else {
        $errorDir = "No se ha podido eliminar la direccion";
    }
}

if (isset($errorDir)) {
    echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> ".$errorDir."</div>";
}
if (isset($successDir)) {
    echo "<div class=\"alert alert-success\"><strong>Información</strong> " . $successDir . "</div>";
}

$clientes = $tClientes->getClientesTodos();
if (sizeof($clientes) > 0) {
    echo '
<table class="table">
    <tr>
        <th class="col-lg-2 col-md-3 col-sm-12">DNI</th>            
        <th class="col-lg-3 col-md-7 col-sm-9">Nombre</th>            
        <th class="col-lg-1 col-md-2 col-sm-3">Ver</th>            
        <th class="col-lg-4 col-md-9 col-sm-9">Direccion - Acciones</th>            
        <th class="col-lg-2 col-md-4 col-sm-3">Añadir direccion</th>            
    </tr>';
    foreach ($clientes as $c) {
        $nombre = (empty($c->getApellido2())) ? $c->getApellido1() . ' ' . $c->getNombre() :
            $c->getApellido1() . ' ' . $c->getApellido2() . ' ' . $c->getNombre();
        echo '
    <tr>
        <td class="col-lg-2 col-md-3 col-sm-12">' . $c->getNif() . '</td>            
        <td class="col-lg-3 col-md-7 col-sm-9">' . $nombre . '</td>            
        <td class="col-lg-1 col-md-2 col-sm-3">
            <form method="post" action="IndexAdmin.php?principal=datosClientes.php">
            <input type="hidden" name="cliente" value="' . $c->getIdCliente() . '">
            <input class="btn btn-primary" type="submit" name="verCliente" value="Ver">
</form>
        </td>';
        $direcciones = $tDC->getDireccionesCliente($c->getIdCliente());
        if (sizeof($direcciones) == 0) {
            echo '<td class="col-lg-4 col-md-9 col-sm-9">No tiene ninguna direccion</td>';
        } elseif (sizeof($direcciones) == 1) {
            /** @var DireccionCliente $d */
            $d = $direcciones[0];
            $nombreDir = $d->getDireccion() . '. ' . $d->getCodPostal() . ' ' . $d->getLocalidad() . ' (' .
                $d->getProvincia() . ') ,' . $d->getPais();
            echo '<td> ' . $nombreDir . ' 
                <form name="formularioModDirectionCliAdmin" method="POST"
                        action="IndexAdmin.php?principal=formDireccionCliente.php" >
                        <input type="hidden" name="idCliente" value="' . $c->getIdCliente() . '">
                        <input type="hidden" name="idDir" value="' . $d->getId() . '">
                        <input class="btn btn-info" type="submit" name="editDirection" value="Editar">
                     </form>
                     <form name="formularioModDirection" method="POST"
                        action="IndexAdmin.php?principal=editarClientes.php" >
                        <input type="hidden" name="idCliente" value="' . $c->getIdCliente() . '">
                        <input type="hidden" name="idEliminar" value="' . $d->getId() . '">
                        <input class="btn btn-danger" type="submit" name="removeDirection" value="Eliminar">
                      </form>
                      </td>
';
        } else {
            echo '<td>
                <form name="formularioModDirectionCliAdmin" method="POST"
                        action="IndexAdmin.php?principal=formDireccionCliente.php" >
                        <input type="hidden" name="idCliente" value="' . $c->getIdCliente() . '">
                        <select name="direccion" required>';
            /** @var DireccionCliente $d */
            foreach ($direcciones as $d) {
                echo "<option value=\"" . $d->getId() . "\">" . $d->getDireccion() . ". " . $d->getCodPostal() . "</option>";
            }

            echo '</select>       
                        <input class="btn btn-info" type="submit" name="editDirection" value="Editar">
                     </form>
                     <form name="formularioModDirection" method="POST"
                        action="IndexAdmin.php?principal=editarClientes.php" >
                        <input type="hidden" name="idCliente" value="' . $c->getIdCliente() . '">
                        <select name="idEliminar" required>';
            /** @var DireccionCliente $d */
            foreach ($direcciones as $d) {
                echo "<option value=\"" . $d->getId() . "\">" . $d->getDireccion() . ". " . $d->getCodPostal() . "</option>";
            }
            echo '</select>
                        <input class="btn btn-danger" type="submit" name="removeDirection" value="Eliminar">
                      </form>
                      </td>
';
        }
        echo '
        <td class="col-lg-2 col-md-4 col-sm-3">
        <form name="formularioModDirection" method="POST"
                        action="IndexAdmin.php?principal=formDireccionCliente.php" >
                        <input type="hidden" name="idCliente" value="' . $c->getIdCliente() . '">
                        <input class="btn btn-primary" type="submit" name="addDireccion" value="Añadir">
                        </form>
</td>            
    </tr>
        ';
    }

    echo '</table>';
} else {
    echo "<h1 class=\"display-4\">No hay ningún cliente</h1>";
}
?>
</body>
</html>