<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Formulario dirección</title>
</head>
<body>
<?php
/** @var DireccionesCliente $tDC */
$tDC = DireccionesCliente::singletonDireccionesCliente();
$id = "";
$idCliente = "";
if (isset($_POST['idDir'])){
    $id = $_POST['idDir'];
} elseif(isset($_POST['direccion'])) {
    $id = $_POST['direccion'];
}
if (isset($_POST['idCliente'])){
    $idCliente = $_POST['idCliente'];
} else {
    $idCliente = $_SESSION['tmpClient'];
}


if (!empty($id)) {
    echo "<h1 class=\"display-4\">Editar dirección</h1>";
    $d = $tDC->getDireccionByID($id);
    $direccion = $d->getDireccion();
    $codPostal = $d->getCodPostal();
    $localidad = $d->getLocalidad();
    $provincia = $d->getProvincia();
    $pais = $d->getPais();
} else {
    echo "<h1 class=\"display-4\">Añadir dirección</h1>";
    $id = "";
    $direccion = "";
    $codPostal = "";
    $localidad = "";
    $provincia = "";
    $pais = "";
}
?>
<div class="form-group table-responsive">

    <form name="formulario1" method="POST"
          action="altaDirCliente.php">
        <table class="table">
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Pais*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="pais"
                                                               required <?php echo 'value="' . $pais . '"' ?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Provincia:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="provincia" required
                        <?php echo 'value="' . $provincia . '"' ?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Localidad*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="localidad"
                                                               required <?php echo 'value="' . $localidad . '"' ?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Codigo Postal*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="codPostal"
                                                               required <?php echo 'value="' . $codPostal . '"' ?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Direccion*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="direccion"
                                                               required <?php echo 'value="' . $direccion . '"' ?>>
                    <input type="hidden" name="idDir" <?php echo 'value="' . $id . '"' ?>>
                    <input type="hidden" name="idCliente" <?php echo 'value="' . $idCliente . '"' ?>>
                </td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12"><input class="btn btn-danger" type="reset" name="reset" value="Borrar todo"></td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input class="btn btn-primary" type="submit" name="alta" <?php if (empty($id)){echo 'value="Añadir dirección"';}
                else {echo 'value="Modificar dirección"'; }?>></td>
            </tr>
        </table>

    </form>
    <p class="p">Los campos con * se deben rellenar obligatoriamente</p>
</div>

</body>
</html>