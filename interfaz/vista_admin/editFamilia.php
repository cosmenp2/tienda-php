<?php
require_once('../../rutas.php');
require_once "../../" . PERSISTENCIA . 'FamiliasProductos.php';
require_once "../../" . PERSISTENCIA . "Usuarios.php";
require_once "../../" . POJOS . "FamiliaProducto.php";
require_once "../../" . POJOS . "Usuario.php";
ini_set('session.use_strict_mode', 1);
session_id($_COOKIE['idSesion']);
session_start();
/** @var Usuarios $tUsuario */
$tUsuario = Usuarios::singletonUsuarios();
$usuario = null;
$legitima = false;

if (isset($_SESSION['idAdmin'])) {
    $usuario = $tUsuario->buscaUsuario($_SESSION['idAdmin']);
    if (is_null($usuario)) {
        $_SESSION['idAdmin'] = "";
    } else {
        $_SESSION['time'] = time();
        $nombre = $usuario->getLogin();
        $legitima = true;
    }
}

$direccion = "Location:IndexAdmin.php?principal=formEditFamilia.php";
$error = "";
$success = "";

if (isset($_POST['nombre']) && $legitima) {
    $nombre = $_POST['nombre'];
    $descripcion = (isset($_POST['descripcion'])) ? $_POST['descripcion'] : "";
    $_SESSION['form'] = array("nombre" => $nombre,"descripcion" => $descripcion);
    /** @var FamiliasProductos $tFamilia */
    $tFamilia = FamiliasProductos::singletonFamiliasProductos();
    $familia = $tFamilia->getFamiliaById($_SESSION['tmpFamilia']);
    $ID = $_SESSION['tmpFamilia'];

    if (empty($error)) {
        $f = new FamiliaProducto(0, $ID, $nombre,$descripcion, 1);
        $insertado = $tFamilia->modificarFamilia($f);
        if ($insertado) {
            unset($_SESSION['form']);
            $direccion = "Location:IndexAdmin.php?principal=editarFamilias.php";
            $success .= "&success=Se%20ha%20modificado%20la%20familia%20correctamente";
        } else {
            $error .= "&error=Ha%20habido%20un%20problema%20al%20guardar%20los%20datos%20de%20la%20familia.";
        }
    }


}
header($direccion . $error . $success);