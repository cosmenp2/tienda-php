<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Añadir roles</title>
</head>
<body>
<h1>Añadir roles</h1>
<?php
/** @var Roles $tRoles */
$tRoles = Roles::singletonRoles();


$error = "";
$success = "";

if (isset($_POST['name'])){
    if (is_null($tRoles->buscaRol($_POST['name']))){
        $r = new Rol(0,$_POST['name']);
        $insertado = $tRoles->addUnRol($r);
        if ($insertado) {
            $success = "Se ha añadido el rol correctamente";
        } else {
            $error = "Ha habido un error al añadir el rol";
        }
    } else {
        $error = "Ya existe un rol con ese nombre";
    }
}

if (!empty($error)) {
    echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> ".$error."</div>";
} elseif (!empty($success)){
    echo "<div class=\"alert alert-success\">".$success."</div>";
}
?>
<form name="formulario1" method="POST" action="IndexAdmin.php?principal=altaRol.php">
    <table class="table">
        <tr>
            <th class="col-lg-5 col-md-5 col-sm-5">Añadir rol</th>
            <th class="col-lg-5 col-md-5 col-sm-5"></th>
            <th class="col-lg-2 col-md-2 col-sm-2"></th>
        </tr>

        <tr>
            <td class="col-lg-5 col-md-5 col-sm-5" >Nombre:</td>
            <td class="col-lg-5 col-md-5 col-sm-5" ><input type="text" name="name" required></td>
            <td class="col-lg-2 col-md-2 col-sm-2" ><input class="btn btn-primary" type="submit" name="add" value="Añadir"></td>
        </tr>
    </table>
</form>
</body>
</html>
