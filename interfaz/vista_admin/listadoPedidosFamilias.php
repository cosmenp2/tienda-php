<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Listado de familia</title>
</head>
<body>
<h1>Listado de familia</h1>
<form name="formulario1" method="POST" action="IndexAdmin.php?principal=listadoPedidosFamilias.php">
    <table class="table">
        <tr>
            <th>Propiedad</th>
            <th>Valor</th>
            <th></th>
        </tr>

        <tr>
            <td>Familia:</td>
            <td><select name="idFamilia" required>
                    <option value=""></option>
                <?php
                /** @var FamiliasProductos $tFamilia */
                $tFamilia = FamiliasProductos::singletonFamiliasProductos();
                /** @var Pedidos $tPedidos */
                $tPedidos = Pedidos::singletonPedidos();

                $tablaPedidos = null;
                $tablaFamilia = null;
                $familias = null;
                $familias = $tFamilia->getFamiliaProductoTodos();
                /** @var FamiliaProducto $f */
                foreach ($familias as $f) {
                    echo "<option value='" . $f->getIdFamilia() . "'>" . $f->getNombre() . "</option>";
                }
                ?>
                </select>
            </td>
            <td><input class="btn btn-primary" type="submit" name="filtrar" value="Ver pedidos de esta familia"></td>
        </tr>
    </table>
</form>

<?php

if (isset($_POST['idFamilia'])) {
    $tablaPedidos = $tPedidos->getPedidosByFamilia($_POST['idFamilia']);
    if (sizeof($tablaPedidos) == 0) {
        echo "<div class=\"alert alert-warning\">Este empleado no ha empaquetado ningún pedido</div>";
    } else {
        echo "<table class='table'>
			<tr>
				<td>Empresa de transporte</td>
				<td>Fecha del pedido</td>
				<td>Fecha de envio</td>
				<td>Fecha de entrega</td>
				<td>Facturado</td>
				<td>Factura</td>
				<td>Fecha factura</td>
				<td>Pagado</td>
				<td>Fecha pago</td>
				<td>Metodo de pago</td>
				<td>Cliente</td>
			</tr>";

        foreach ($tablaPedidos as $p) {
            $empresaTrans = "";
            switch ($p->getIdEmpresaTransporte()){
                case 0:
                    $empresaTrans = "No asignada";
                    break;
                case 1:
                    $empresaTrans = "Seur";
                    break;
                case 2:
                    $empresaTrans = "DHL";
                    break;
                case 3:
                    $empresaTrans = "Correos";
                    break;
                case 4:
                    $empresaTrans = "MRV";
                    break;
            }
            echo "<tr>";
            echo "<td>" . $empresaTrans . "</td>";
            echo "<td>" . $p->getFechaPedido() . "</td>";
            echo "<td>" . $p->getFechaEnvio() . "</td>";
            echo "<td>" . $p->getFechaEntrega() . "</td>";
            echo "<td>" . (($p->getFacturado() == 1) ? "Si" : "No") . "</td>";
            echo "<td>" . $p->getIdFactura() . "</td>";
            echo "<td>" . $p->getFechaFactura() . "</td>";
            echo "<td>" . (($p->getPagado() == 1) ? "Si" : "No") . "</td>";
            echo "<td>" . $p->getFechaPago() . "</td>";
            echo "<td>" . $p->getMetodoPago() . "</td>";
            echo "<td>" . $p->getIdCliente() . "</td>";
            echo " </tr>";
        }
        echo "</table>";
        echo "<h1 class='display-4'>Total: ".sizeof($tablaPedidos)."</h1>";
    }
}
?>

</body>
</html>
