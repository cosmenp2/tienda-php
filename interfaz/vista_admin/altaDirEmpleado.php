<?php
require_once('../../rutas.php');
require_once "../../".POJOS . "DireccionEmpleado.php";
require_once "../../".POJOS . "Empleado.php";
require_once "../../".POJOS . "Usuario.php";
require_once "../../".PERSISTENCIA . "DireccionesEmpleado.php";
require_once "../../".PERSISTENCIA . "Empleados.php";
require_once "../../".PERSISTENCIA . "Usuarios.php";
ini_set('session.use_strict_mode', 1);
session_id($_COOKIE['idSesion']);
session_start();
/** @var Usuarios $tUsuario */
$tUsuario = Usuarios::singletonUsuarios();
$usuario = null;
$legitima = false;

if (isset($_SESSION['idAdmin'])) {
    $usuario = $tUsuario->buscaUsuario($_SESSION['idAdmin']);
    if (is_null($usuario)) {
        $_SESSION['idAdmin'] = "";
    } else {
        $_SESSION['time'] = time();
        $nombre = $usuario->getLogin();
        $legitima = true;
    }
}

if (isset($_SESSION['urlAnteriorAdmin'])){
    $url = "Location:". $_SESSION['urlAnteriorAdmin'];
} else {
    $url = "Location:IndexAdmin.php?principal=editarEmpleados.php";
}
$error = "";
$success = "";

if (isset($_POST['idEmpleado']) && $legitima) {
    $idEmpleado = $_POST['idEmpleado'];
    $id = (empty($_POST['idDir']))?"":$_POST['idDir'];
    $direccion = $_POST['direccion'];
    $codPostal = $_POST['codPostal'];
    $localidad = $_POST['localidad'];
    $provincia = $_POST['provincia'];
    $pais = $_POST['pais'];

    /** @var DireccionesEmpleado $tDC */
    $tDC = DireccionesEmpleado::singletonDireccionesEmpleado();


    if (empty($error)){
        $dc = new DireccionEmpleado($id, $idEmpleado,$direccion,$localidad,$codPostal,$provincia,1,$pais);
        if (empty($id)){
            $insertado = $tDC->addUnaDireccion($dc);
            if ($insertado){
                $success = "&successDir=Se%20ha%20añadido%20la%20direccion%20correctamente";
            } else {
                $error = "&errorDir=Ha%20habido%20un%20error%20al%20guardar%20la%20direccion";
            }
        }else {
            $modificado = $tDC->editUnaDireccion($dc);
            if ($modificado){
                $success = "&successDir=Se%20ha%20modificado%20la%20direccion%20correctamente";
            } else {
                $error = "&errorDir=Ha%20habido%20un%20error%20al%20modificar%20la%20direccion";
            }
        }

    }
}
header($url.$error.$success);