<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Añadir Productos</title>
</head>
<body>
<h1 class="display-4">Añadir un producto</h1>
<?php
/** @var FamiliasProductos $tFamilias */
$tFamilias = FamiliasProductos::singletonFamiliasProductos();
$familias = $tFamilias->getFamiliaProductoTodos();

if (isset($_GET['error'])) {
    echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> " . $_GET['error'] . "</div>";
}
if (isset($_GET['success'])) {
    echo "<div class=\"alert alert-success\"><strong>Información</strong> " . $_GET['success'] . "</div>";
}
if (isset($_SESSION['form'])) {
    $idFamilia = $_SESSION['form']['idFamilia'];
    $tipoIva = $_SESSION['form']['tipoIva'];
    $precioCoste = $_SESSION['form']['precioCoste'];
    $pvp = $_SESSION['form']['pvp'];
    $descripcion = $_SESSION['form']['descripcion'];
    $codigoBarras = $_SESSION['form']['codigoBarras'];
    $StockActual = $_SESSION['form']['stockActual'];
    $stockMinimo = $_SESSION['form']['stockMinimo'];
    $stockMaximo = $_SESSION['form']['stockMaximo'];
} else {
    $idFamilia = "";
    $tipoIva = "";
    $precioCoste = "";
    $pvp = "";
    $descripcion = "";
    $codigoBarras = "";
    $StockActual = "";
    $stockMinimo = "";
    $stockMaximo = "";
}
?>
<div class="form-group table-responsive">

    <form name="formularioAltaEmpleado" method="POST" enctype="multipart/form-data"
          action="altaProducto.php">
        <table class="table">
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Familia:</td>
                <td class="col-lg-6 col-md-6 col-sm-12">
                    <select name="idFamilia" required>
                        <option value=""></option>
                        <?php
                        /** @var FamiliaProducto $f */
                        foreach ($familias as $f) {
                            if ($idFamilia == $f->getIdFamilia())
                                echo "<option selected='selected' value='" . $f->getIdFamilia() . "'>" . $f->getNombre() . "</option>";
                            else
                                echo "<option value='" . $f->getIdFamilia() . "'>" . $f->getNombre() . "</option>";
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Iva del elemento:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="number" name="tipoIva" max="100" min="0" step="0.01"
                                                               required <?php echo 'value="' . $tipoIva . '"' ?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Precio coste:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="number" name="precioCoste" step="0.01"
                                                               required min="0" <?php echo 'value="' . $precioCoste . '"' ?>>
                </td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Precio de venta al público:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="number" name="pvp" step="0.01"
                                                               required min="0" <?php echo 'value="' . $pvp . '"' ?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Descripción:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="descripcion"
                                                               required <?php echo 'value="' . $descripcion . '"' ?>>
                </td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Código de barras:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="codigoBarras" max="24"
                                                               required <?php echo 'value="' . $codigoBarras . '"' ?>>
                </td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Stock Actual:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="number" step="1" name="stockActual" required min="0"
                        <?php echo 'value="' . $StockActual . '"' ?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Stock Mínimo:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="number" name="stockMinimo" min="0" step="1"
                                                               required <?php echo 'value="' . $stockMinimo . '"' ?>>
                </td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Stock Máximo:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="number" name="stockMaximo" min="0" step="1"
                                                               required <?php echo 'value="' . $stockMaximo . '"' ?>>
                </td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Foto:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="file" name="foto" required/></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12"><input class="btn btn-danger" type="reset" name="reset"
                                                               value="Borrar todo"></td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input class="btn btn-primary" type="submit" name="alta"
                                                               value="Añadir producto"></td>
            </tr>
        </table>
    </form>
    <p class="p">Todos los campos se deben de rellenar</p>
</div>

</body>
</html>