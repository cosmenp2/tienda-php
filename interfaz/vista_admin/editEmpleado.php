<?php
require_once('../../rutas.php');
require_once "../../" . PERSISTENCIA . 'Empleados.php';
require_once "../../" . PERSISTENCIA . "Usuarios.php";
require_once "../../" . POJOS . "Empleado.php";
require_once "../../" . POJOS . "Usuario.php";
ini_set('session.use_strict_mode', 1);
session_id($_COOKIE['idSesion']);
session_start();
/** @var Usuarios $tUsuario */
$tUsuario = Usuarios::singletonUsuarios();
$usuario = null;
$legitima = false;

if (isset($_SESSION['idAdmin'])) {
    $usuario = $tUsuario->buscaUsuario($_SESSION['idAdmin']);
    if (is_null($usuario)) {
        $_SESSION['idAdmin'] = "";
    } else {
        $_SESSION['time'] = time();
        $nombre = $usuario->getLogin();
        $legitima = true;
    }
}

$direccion = "Location:IndexAdmin.php?principal=formEditEmpleado.php";
$error = "";
$success = "";

if (isset($_POST['nombre']) && $legitima) {
    $nombre = $_POST['nombre'];
    $apellido1 = $_POST['apellido1'];
    $apellido2 = (isset($_POST['apellido2'])) ? $_POST['apellido2'] : "";
    $nif = $_POST['nif'];
    $movil = $_POST['movil'];
    $rutaFoto = "";
    $numCta = (isset($_POST['numCta'])) ? $_POST['numCta'] : "";
    $_SESSION['form'] = array("nombre" => $nombre, "apellido1" => $apellido1, "apellido2" => $apellido2, "nif" => $nif,
        "movil" => $movil, "numCta" => $numCta);
    /** @var Empleados $tEmpleado */
    $tEmpleado = Empleados::singletonEmpleados();
    $empleado = $tEmpleado->getEmpleado($_SESSION['tmpEmpleado']);
    $ID = $_SESSION['tmpEmpleado'];
    $empleadoComprobacion = $tEmpleado->getEmpleadoByNif($nif);

    if (!is_null($empleadoComprobacion)) {
        if ($empleadoComprobacion->getIdEmpleado() != $ID){
            $error .= "&error=Ya%20existe%20un%20empleado%20con%20el%20mismo%20DNI.%20";
        }
    }

    if (empty($error)){
        if (rtrim($_FILES['foto']['name']) != "") {
            $nombreOriginal = $_FILES['foto']['name'];
            $posPunto = strpos($nombreOriginal, ".") + 1;
            $extensionOriginal = substr($nombreOriginal, $posPunto, 3);
            $nombreNuevo = "empleados/emp_" . $ID . "." . $extensionOriginal;

            $errUpload = move_uploaded_file($_FILES['foto']['tmp_name'], "../../".FOTOS.$nombreNuevo);

            if ($errUpload == 0) {
                $error .= "&error=Ha%20habido%20un%20error%20al%20subir%20la%20foto.%20";
            } else {
                $rutaFoto=$nombreNuevo;
            }
        } else {
            $rutaFoto = $empleado->getRutaFoto();
        }
    }

    if (empty($error)) {
        $e = new Empleado(0, $ID, $empleado->getIdUsuario(),$nif, $nombre, $apellido1, $apellido2,
            $numCta, $movil,$rutaFoto, 1);
        $insertado = $tEmpleado->actualizarEmpleado($e);
        if ($insertado) {
            unset($_SESSION['form']);
            $direccion = "Location:IndexAdmin.php?principal=datosEmpleados.php";
            $success .= "&success=Se%20ha%20modificado%20el%20cliente%20correctamente";
        } else {
            $error .= "&error=Ha%20habido%20un%20problema%20al%20guardar%20los%20datos%20del%20cliente.";
        }
    }


}
header($direccion . $error . $success);