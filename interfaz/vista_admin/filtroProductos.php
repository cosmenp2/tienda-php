<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Busqueda de productos</title>
</head>
<body>
<h1>Busqueda de productos</h1>
<form name="formulario1" method="POST" action="IndexAdmin.php?principal=filtroProductos.php">
    <table class="table">
        <tr>
            <th class="col-lg-5 col-md-5 col-sm-5">Busqueda por id</th>
            <th class="col-lg-5 col-md-5 col-sm-5"></th>
            <th class="col-lg-2 col-md-2 col-sm-2"></th>
        </tr>

        <tr>
            <td class="col-lg-5 col-md-5 col-sm-5" >Id Producto:</td>
            <td class="col-lg-5 col-md-5 col-sm-5" ><input type="text" name="idProducto" required></td>
            <td class="col-lg-2 col-md-2 col-sm-2" ><input class="btn btn-primary" type="submit" name="filtrar" value="Filtrar"></td>
        </tr>
    </table>
</form>

<form name="formulario2" method="POST" action="IndexAdmin.php?principal=filtroProductos.php">
    <table class="table">
        <tr>
            <th class="col-lg-5 col-md-5 col-sm-5" >Busqueda por familia</th>
            <th class="col-lg-5 col-md-5 col-sm-5" ></th>
            <th class="col-lg-2 col-md-2 col-sm-2" ></th>
        </tr>

        <tr>
            <td class="col-lg-5 col-md-5 col-sm-5" >Familia:</td>
            <td class="col-lg-5 col-md-5 col-sm-5" >
                <select name="familia" required>
                    <option value=""></option>
                    <?php
                    /** @var FamiliasProductos $tFamilia */
                    $tFamilia = FamiliasProductos::singletonFamiliasProductos();

                    $familias = $tFamilia->getFamiliaProductoTodos();
                    /** @var FamiliaProducto $p */
                    foreach ($familias as $p) {
                        echo "<option value='" . $p->getIdFamilia() . "'>" . $p->getNombre() . "</option>";
                    }
                    ?>
                </select>
            </td>
            <td class="col-lg-2 col-md-2 col-sm-2" ><input class="btn btn-primary" type="submit" name="filtrar" value="Filtrar"></td>
        </tr>
    </table>
</form>
<form name="formulario3" method="POST" action="IndexAdmin.php?principal=filtroProductos.php">
    <table class="table">
        <tr>
            <th class="col-lg-5 col-md-5 col-sm-5" >Busqueda por código de barras</th>
            <th class="col-lg-5 col-md-5 col-sm-5" ></th>
            <th class="col-lg-2 col-md-2 col-sm-2" ></th>
        </tr>

        <tr>
            <td class="col-lg-5 col-md-5 col-sm-5" >Código de barras:</td>
            <td class="col-lg-5 col-md-5 col-sm-5" ><input type="text" name="codBarras" required></td>
            <td class="col-lg-2 col-md-2 col-sm-2" ><input class="btn btn-primary" type="submit" name="filtrar" value="Filtrar"></td>
        </tr>
    </table>
</form>

<?php

/** @var Productos $tProductos */
$tProductos = Productos::singletonProductos();

$tablaProductos = null;

if (isset($_POST['idProducto'])) {
    $tablaProductos = $tProductos->searchProductosById($_POST['idProducto']);

    if (sizeof($tablaProductos) == 0) {
        echo "<div class=\"alert alert-warning\">No se ha encontrado ningún producto con este patrón</div>";
    } else {
        echo "<table class='table'>
			<tr>
				<td>IdProducto</td>
				<td>Familia</td>
				<td>Descripcion</td>
				<td>Precio de venta</td>
				<td>Precio coste</td>
				<td>IVA</td>
				<td>Código de barras</td>
				<td>Stock actual</td>
			</tr>";
        /** @var Producto $p */
        foreach ($tablaProductos as $p) {
            echo "<tr>";
            echo "<td>" . $p->getIdProducto() . "</td>";
            echo "<td>" . $p->getIdFamilia() . "</td>";
            echo "<td>" . $p->getDescripcion() . "</td>";
            echo "<td>" . $p->getPvp() . "</td>";
            echo "<td>" . $p->getPrecioCoste() . "</td>";
            echo "<td>" . $p->getTipoIva() . "</td>";
            echo "<td>" . $p->getCodigoBarras() . "</td>";
            echo "<td>" . $p->getStockActual() . "</td>";
            echo " </tr>";
        }
        echo "</table>";
    }
} else if (isset($_POST['familia'])) {
    $tablaProductos = $tProductos->getProductosByFamilia($_POST['familia']);

    if (sizeof($tablaProductos) == 0) {
        echo "<div class=\"alert alert-warning\">No se ha encontrado ningún producto de esta familia</div>";
    } else {
        echo "<table class='table'>
			<tr>
				<td>IdProducto</td>
				<td>Familia</td>
				<td>Descripcion</td>
				<td>Precio de venta</td>
				<td>Precio coste</td>
				<td>IVA</td>
				<td>Código de barras</td>
				<td>Stock actual</td>
			</tr>";
        /** @var Producto $p */
        foreach ($tablaProductos as $p) {
            echo "<tr>";
            echo "<td>" . $p->getIdProducto() . "</td>";
            echo "<td>" . $p->getIdFamilia() . "</td>";
            echo "<td>" . $p->getDescripcion() . "</td>";
            echo "<td>" . $p->getPvp() . "</td>";
            echo "<td>" . $p->getPrecioCoste() . "</td>";
            echo "<td>" . $p->getTipoIva() . "</td>";
            echo "<td>" . $p->getCodigoBarras() . "</td>";
            echo "<td>" . $p->getStockActual() . "</td>";
            echo " </tr>";
        }
        echo "</table>";
    }
} else if (isset($_POST['codBarras'])) {
    $tablaProductos = $tProductos->searchProductosByCodBarras($_POST['codBarras']);

    if (sizeof($tablaProductos) == 0) {
        echo "<div class=\"alert alert-warning\">No se ha encontrado ningún producto con este patrón</div>";
    } else {
        echo "<table class='table'>
			<tr>
				<td>IdProducto</td>
				<td>Familia</td>
				<td>Descripcion</td>
				<td>Precio de venta</td>
				<td>Precio coste</td>
				<td>IVA</td>
				<td>Código de barras</td>
				<td>Stock actual</td>
			</tr>";
        /** @var Producto $p */
        foreach ($tablaProductos as $p) {
            echo "<tr>";
            echo "<td>" . $p->getIdProducto() . "</td>";
            echo "<td>" . $p->getIdFamilia() . "</td>";
            echo "<td>" . $p->getDescripcion() . "</td>";
            echo "<td>" . $p->getPvp() . "</td>";
            echo "<td>" . $p->getPrecioCoste() . "</td>";
            echo "<td>" . $p->getTipoIva() . "</td>";
            echo "<td>" . $p->getCodigoBarras() . "</td>";
            echo "<td>" . $p->getStockActual() . "</td>";
            echo " </tr>";
        }
        echo "</table>";
    }
}
?>

</body>
</html>
