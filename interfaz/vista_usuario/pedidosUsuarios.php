<?php

/** @var Pedidos $tPedidos */
$tPedidos = Pedidos::singletonPedidos();
/** @var Clientes $tCliente */
$tCliente = Clientes::singletonClientes();
$c = $tCliente ->getUnClienteByIDUsuario($_SESSION['idCliente']);
$idCliente = $c ->getIdCliente();

$pedidos = $tPedidos->getPedidosByCliente($idCliente);


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Pedidos del usuario</title>
</head>
<body>

<div class="card">
    <div class="card-header">
        Pedidos
    </div>
    <div class="card-body">
        <blockquote class="blockquote mb-0">
            <table class="table">
                <?php
                if (sizeof($pedidos) > 0) {
                    echo '<tr>
                            <th>Referencia del Pedido</th>
                            <th>Fecha Pedido</th>
                            <th>Metodo de Pago</th>
                            <th>Pagado</th>
                            <th>Estado</th>
                            <th>Fecha de Entrega</th>
                            <th>PDF</th>
                        </tr>';
                    /** @var Pedido $p */
                    foreach ($pedidos as $p) {
                        $pagado = ($p->getPagado() == 1)?"Si":"No";
                        $estado = "";
                        $fechaEntrega = "Desconocida";
                        if (empty($p->getIdEmpleadoQueEmpaqueta())){
                            $estado = "Procesando Pedido";
                        } else {
                            $estado = "En almacenes";
                        }

                        if ($p->getFechaEnvio()=="0000-00-00"){
                            $fechaEntrega = "Desconocido";
                        } else {
                            $estado = "Enviado";
                        }

                        if ($p->getFechaEntrega() != "0000-00-00"){
                            $estado = "Entregado";
                            $fechaEntrega = $p->getFechaEntrega();
                        }
                        echo '<tr>
                    <td class="col-lg-6">' . $p->getIdPedido() .'</td>
                    <td class="col-lg-6">' . $p->getFechaPedido() .'</td>
                    <td class="col-lg-6">' . $p->getMetodoPago() .'</td>
                    <td class="col-lg-6">' . $pagado .'</td>
                    <td class="col-lg-6">' . $estado .'</td>
                    <td class="col-lg-6">' . $fechaEntrega .'</td>
                    <td class="col-lg-6">
                        <form name="formularioDownloadPDFPedido" method="POST"
                                action="' . PDF . 'imprimirPedidoPDF.php" >
                            <input type="hidden" name="idPedido" value="' . $p->getIdPedido() . '">
                            <input class="btn btn-info" type="submit" name="editDirection" value="Imprimir">
                        </form>
                    </td>
                    </tr>';
                    }
                }
                ?>
            </table>
        </blockquote>
    </div>
</div>

</body>
</html>