<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Iniciar Sesión</title>
</head>
<h1 class="display-4">Iniciar Sesión</h1>
<body>
<div class="form-group table-responsive">
    <?php
    if (isset($_GET['error'])) {
        echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> ".$_GET['error']."</div>";
    }
    ?>

    <form name="formularioInicioSesion" method="POST"
          action=<?php echo '"' . VISTA_USUARIO . 'iniciarSesion.php"' ?>>
        <table class="table">
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Nombre de usuario:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="username"
                                                               required></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Contraseña:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="password" name="password" required></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">
                    <a class="nav-link" <?php echo 'href="IndexInicial.php?principal=' . VISTA_USUARIO . 'formularioRegistro.php"' ?>>
                        Registrarse</a></td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input class="btn btn-primary" type="submit" name="alta" value="Iniciar Sesion"></td>
            </tr>
        </table>

    </form>
</div>

</body>
</html>