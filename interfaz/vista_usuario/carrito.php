<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cesta de compra</title>
</head>
<body>

<?php
/** @var Productos $tProducto */
$tProducto = Productos::singletonProductos();
/** @var ArrayObject $productos */
$productos = $tProducto->getProductosTodos();

if (isset($_GET['error'])) {
    echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> ".$_GET['error']."</div>";
}

if (isset($_POST['idProducto'])) {
    $id = 0;
    $idPedido = '001'; //hay que elaborarlo
    $idProducto = $_POST['idProducto'];
    $unidades = $_POST['unidades'];
    if (!is_numeric($unidades)) {
        $unidades = 1;
    }
    $descripcion = $_POST['descripcion'];
    $pvp = $_POST['pvp'];
    $tipoIva = $_POST['tipoIva'];
    $activo = 1; //en las inserciones siempre a 1

    $pr = new LineaPedido($id, $idPedido, $idProducto, $unidades, $descripcion, $pvp, $tipoIva, $activo);


    $t = $_SESSION['cesta'];

    $ids = array();
    foreach ($t as $id) {
        $id = unserialize($id);
        $id2 = $id->getIdProducto();
        array_push($ids, $id2);
    }

    if ($_SESSION['urlAnterior'] != 'IndexInicial.php?principal=' . VISTA_USUARIO . 'carrito.php') {
        $_SESSION['urlAnterior'] = 'IndexInicial.php?principal=' . VISTA_USUARIO . 'carrito.php';
        if (in_array($idProducto, $ids)) {
            $i = 0;
            $encontrado = false;
            while (!$encontrado && $i < sizeof($t)) {
                $p = unserialize($t[$i]);
                if ($p->getIdProducto() == $idProducto) {
                    $x = "" . ($p->getUnidades() + $unidades);
                    $p->setUnidades($x);
                    $pr = serialize($p);
                    $_SESSION['cesta'][$i] = $pr;
                    $encontrado = true;
                }
                $i = $i + 1;
            }
        } else {
            $pr = serialize($pr);
            array_push($_SESSION['cesta'], $pr);
        }
    }
}

if (isset($_POST['idMinus'])){
    $i = 0;
    $encontrado = false;
    while (!$encontrado && $i < sizeof($_SESSION['cesta'])) {
        $p = unserialize($_SESSION['cesta'][$i]);
        if ($p->getIdProducto() == $_POST['idMinus']) {
            $x = "" . ($p->getUnidades() - 1);
            if ($x <= 0){
                unset($_SESSION['cesta'][$i]);
                $_SESSION['cesta'] = array_values($_SESSION['cesta']);
            }else {
                $p->setUnidades($x);
                $pr = serialize($p);
                $_SESSION['cesta'][$i] = $pr;
            }
            $encontrado = true;
        }
        $i = $i + 1;
    }
}

if (isset($_POST['idMore'])){
    $i = 0;
    $encontrado = false;
    while (!$encontrado && $i < sizeof($_SESSION['cesta'])) {
        $p = unserialize($_SESSION['cesta'][$i]);
        if ($p->getIdProducto() == $_POST['idMore']) {
            $x = "" . ($p->getUnidades() + 1);
            $p->setUnidades($x);
            $pr = serialize($p);
            $_SESSION['cesta'][$i] = $pr;
            $encontrado = true;
        }
        $i = $i + 1;
    }
}
if (isset($_SESSION['cesta'])) {
    if (sizeof($_SESSION['cesta']) > 0) {
        echo "<h1 class='display-6'>Actualmente llevas en tu carrito:</h1>";
        echo "<table class='table table-striped'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th scope='col'>Id Producto</th>";
        echo "<th scope='col'>descripcion</th>";
        echo "<th scope='col'>Unidades</th>";
        echo "<th scope='col'>PVP</th>";
        echo "<th scope='col'>IVA</th>";
        echo "<th scope='col'>Foto</th>";
        echo "<th scope='col'>Subtotal</th>";
        echo "<th scope='col'>Acciones</th>";
        echo "</tr>";
        echo "</thead>";
        $total = 0;
        foreach ($_SESSION['cesta'] as $pr) {
            $pr = unserialize($pr);
            $encontrado = false;
            $contador = 0;
            $producto = null;
            while ($contador < sizeof($productos) && !$encontrado) {
                if ($productos[$contador]->getIdProducto() == $pr->getIdProducto()) {
                    $encontrado = true;
                    $producto = $productos[$contador];
                }
                $contador++;
            }
            $subtotal = ($pr->getUnidades() * $pr->getPvp()) * (1 + ($pr->getPvp() / 100));
            $total += $subtotal;
            echo "<tr>";
            echo "<td>" . $pr->getIdProducto() . "</td>";
            echo "<td>" . $pr->getDescripcion() . "</td>";
            echo "<td>" . $pr->getUnidades() . "</td>";
            echo "<td>" . $pr->getPvp() . "</td>";
            echo "<td>" . $pr->getTipoIva() . "</td>";
            if (is_null($producto)) {
                echo "<td></td>";
            } else {
                echo "<td><img  class=\"card-img-top\" src=\"" . FOTOS . $producto->getRutaFoto() . "\"
              alt=\"" . $pr->getDescripcion() . "\" style=\"width:80px;height: 80px\"></td>";
            }
            echo "<td>" . $subtotal . "</td>";
            echo "<td><table><tr style='background-color: rgba(0,0,0,0)'><td style='border-top-color: rgba(0,0,0,0)'>
            <form name=\"formularioMinus\" method=\"POST\" action=\"indexInicial.php?principal=" . VISTA_USUARIO . "carrito.php\">
                <input type=\"hidden\" name=\"idMinus\" value=\"" . $pr->getIdProducto() . "\">
                <input class=\"btn btn-danger\" type=\"submit\" name=\"menos\" value='-'>
            </form>
            </td>
            <td style='border-top-color: rgba(0,0,0,0)'>
            <form name=\"formularioAdd\" method=\"POST\" action=\"indexInicial.php?principal=" . VISTA_USUARIO . "carrito.php\">
                <input type=\"hidden\" name=\"idMore\" value=\"" . $pr->getIdProducto() . "\">
                <input class=\"btn btn-success\" type=\"submit\" name=\"mas\" value='+'>
            </form></td></tr></table></td>";
            echo "</tr>";
        }
        echo "</table>";
        echo '<h1 class="display-6">Total: ' . $total . '€</h1>';
        $_SESSION['totalPayment'] = $total;
        echo "<br><br>";
        echo '<a class="btn btn-primary" href="indexInicial.php?principal=' . VISTA_USUARIO . 'catalogo_productos_carrito.php">Ver más productos</a> ';
        if (isset($_SESSION['idCliente']) && !empty($_SESSION['idCliente'])) {
            echo '<a class="btn btn-primary" href="indexInicial.php?principal=' . VISTA_USUARIO . 'formularioPago.php">Comprar </a>';
        } else {
            echo '<a class="btn btn-primary" href="indexInicial.php?principal=' . VISTA_USUARIO . 'formInicioSesion.php">Comprar </a>';
        }
    } else {
        echo '<h1 class="display-4">El carro está vacio</h1>';
        echo '<h1 class="display-6"><a class="btn btn-primary" href="indexInicial.php?principal=' . VISTA_USUARIO . 'catalogo_productos_carrito.php">Ver productos</a></h1>';
    }
} else {
    echo '<h1 class="display-4">El carro está vacio</h1>';
    echo '<h1 class="display-6"><a class="btn btn-primary" href="indexInicial.php?principal=' . VISTA_USUARIO . 'catalogo_productos_carrito.php">Ver productos</a></h1>';
}
?>
</body>
</html>