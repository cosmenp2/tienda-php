<?php
require_once('../../rutas.php');
require_once "../../" . PERSISTENCIA . "Usuarios.php";
require_once "../../" . POJOS . "Usuario.php";
ini_set('session.use_strict_mode', 1);
session_id($_COOKIE['idSesion']);
session_start();

if (isset($_SESSION['urlAnterior'])) {
    $url = "Location:../../" . $_SESSION['urlAnterior'];
} else {
    $url = "Location:../../IndexInicial.php";
}
$error = "";

if (isset($_POST['username'])) {
    $username = $_POST['username'];
    $password = hash("sha512", $_POST['password']);;

    /** @var Usuarios $tUsuario */
    $tUsuario = Usuarios::singletonUsuarios();

    $u = $tUsuario->login($username, $password);
    if (is_null($u)) {
        $url = 'Location:../../IndexInicial.php?principal=' . VISTA_USUARIO . 'formInicioSesion.php';
        $error = "&error=Usuario%20o%20contraseña%20incorrectos";
    } else {
        if ($u->getIdRol() == 1) {
            $_SESSION['idAdmin'] = $username;
            $url = 'Location:../../'.VISTA_ADMIN.'IndexAdmin.php';
        } elseif ($u->getIdRol() == 3) {
            $_SESSION['idCliente'] = $username;
        }
    }

}
header($url . $error);