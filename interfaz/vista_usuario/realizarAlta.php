<?php
require_once('../../rutas.php');
require_once "../../".PERSISTENCIA . 'Clientes.php';
require_once "../../".PERSISTENCIA . "Usuarios.php";
require_once "../../".POJOS . "Cliente.php";
require_once "../../".POJOS . "Usuario.php";
ini_set('session.use_strict_mode', 1);
session_id($_COOKIE['idSesion']);
session_start();

$direccion = "Location:../../IndexInicial.php?principal=" . VISTA_USUARIO . "formularioRegistro.php&error=";
$error = "";

if (isset($_POST['nombre'])) {
    $nombre = $_POST['nombre'];
    $apellido1 = $_POST['apellido1'];
    $apellido2 = (isset($_POST['apellido2'])) ? $_POST['apellido2'] : "";
    $nif = $_POST['nif'];
    $sexo = ($_POST['sexo'] == "H") ? 1 : 0;
    $numCta = (isset($_POST['numCta'])) ? $_POST['numCta'] : "";
    $cnc = (isset($_POST['cnc'])) ? $_POST['cnc'] : "";
    $login = $_POST['login'];
    $password = $_POST['password'];
    $passwordRep = $_POST['passwordRepite'];
    $_SESSION['form'] = array("nombre"=>$nombre,"apellido1"=>$apellido1, "apellido2"=>$apellido2, "nif"=>$nif,
        "sexo"=>$sexo,"numCta"=>$numCta,"cnc"=>$cnc,"login"=>$login);
    /** @var Clientes $tCliente */
    $tCliente = Clientes::singletonClientes();

    /** @var Usuarios $tUsuario */
    $tUsuario = Usuarios::singletonUsuarios();

    if (!is_null($tCliente->getUnClienteByNif($nif))){
        $error .= "Ya%20existe%20un%20cliente%20con%20el%20mismo%20DNI.%20";
    }
    if (!is_null($tUsuario->buscaUsuario($login))){
        $error .= "Ya%20existe%20un%20usuario%20con%20el%20mismo%20nombre.%20";
    }
    if ($password != $passwordRep){
        $error .= "Las%20contraseñas%20no%20coinciden.";
    }

    if (empty($error)){
        $newID = $tCliente -> generarID();
        $c = new Cliente(0, $newID, $login, $nombre, $apellido1, $apellido2, $nif, $sexo,
            $numCta, $cnc, 1);
        $securePass = hash("sha512",$password);
        $u = new Usuario(0,$login,3,$login,$securePass,1);


        $insertado = $tCliente->adduncliente($c) && $tUsuario->addUnUsuario($u);
        if ($insertado) {
            $_SESSION['idCliente'] = $login;
            $_SESSION['pass'] = $securePass;
            $_SESSION['time'] = time();
            if (isset($_SESSION['urlAnterior'])){
                $direccion = "Location:../../".$_SESSION['urlAnterior'];
            }else {
                $direccion = "Location:../../IndexInicial.php?principal=" . INFORMATIVAS . "graciasRegistro.php";
            }
            unset($_SESSION['form']);
        } else {
            $error .= "Ha%20habido%20un%20problema%20al%20guardar%20sus%20datos.%20Vuelva%20a%20intentarlo%20mas%20tarde%20o%20contacte%20con%20el%20administrador.";
        }

    }
}
header($direccion.$error);