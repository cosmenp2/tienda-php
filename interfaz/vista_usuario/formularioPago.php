<?php
$_SESSION['urlAnterior'] = 'IndexInicial.php?principal=' . VISTA_USUARIO . 'formularioPago.php';
/** @var DireccionesCliente $tDireccionesCliente */
$tDireccionesCliente = DireccionesCliente::singletonDireccionesCliente();
/** @var Clientes $tCliente */
$tCliente = Clientes::singletonClientes();
$c = $tCliente ->getUnClienteByIDUsuario($_SESSION['idCliente']);
$idCliente = $c ->getIdCliente();
$direcciones = $tDireccionesCliente->getDireccionesCliente($idCliente);
if (isset($_SESSION['cesta']) && sizeof($_SESSION['cesta'])>0) {
    $_SESSION['valido'] = "yes";
    if (isset($_SESSION['idPedido'])){
        unset($_SESSION['idPedido']);
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Metodo de compra</title>
</head>
<h1 class="display-4">Metodo de compra y direccion</h1>
<body>
<div class="form-group table-responsive">

    <form name="formulario1" method="POST"
          action=<?php echo '"' . VISTA_USUARIO . 'generarPedido.php"' ?>>
        <table class="table">
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Metodo de pago:</td>
                <td class="col-lg-6 col-md-6 col-sm-12">
                    <input type="radio" name="method" value="paypal" checked = checked><img src="<?php echo ICONOS. "paypal.png" ?>">
                    <input type="radio" name="method" value="numCta"> Transferencia Bancaria
                    <input type="radio" name="method" value="contrareembolso"> Contrareembolso
                </td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Direccion:</td>
                <td class="col-lg-6 col-md-6 col-sm-12">
                    <select name="direccion" required>
                        <?php
                        /** @var DireccionCliente $d */
                        foreach ($direcciones as $d) {
                            echo "<option value=\"".$d->getId()."\">".$d->getDireccion() . ". " . $d->getCodPostal()."</option>";
                        }
                        ?>
                    </select>
                </td>
            </tr>

            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12"><a class="btn btn-warning" <?php echo 'href="IndexInicial.php?principal=' . VISTA_USUARIO . 'formularioDireccion.php"' ?>>
                        Añadir direccion</a></td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input class="btn btn-primary" type="submit" name="comprar" value="Pagar"></td>
            </tr>
        </table>
    </form>
    <p class="p">Los campos con * se deben rellenar obligatoriamente</p>
</div>

</body>
</html>