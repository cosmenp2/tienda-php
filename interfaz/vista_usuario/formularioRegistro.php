<?php
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Formulario de registro</title>
</head>
<h1 class="display-4">Formulario de registro</h1>
<body>
<?php
if (isset($_GET['error'])) {
    echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> ".$_GET['error']."</div>";
}
if (isset($_SESSION['form'])) {
    $nombre = $_SESSION['form']['nombre'];
    $apellido1 = $_SESSION['form']['apellido1'];
    $apellido2 = (isset($_SESSION['form']['apellido2'])) ? $_SESSION['form']['apellido2'] : "";
    $nif = $_SESSION['form']['nif'];
    $sexo = ($_SESSION['form']['sexo'] == "H") ? 1 : 0;
    $numCta = (isset($_SESSION['form']['numCta'])) ? $_SESSION['form']['numCta'] : "";
    $cnc = (isset($_SESSION['form']['cnc'])) ? $_SESSION['form']['cnc'] : "";
    $login = $_SESSION['form']['login'];
} else {
    $nombre = "";
    $apellido1 = "";
    $apellido2 = "";
    $nif = "";
    $sexo = "";
    $numCta = "";
    $cnc = "";
    $login = "";
}
?>
<div class="form-group table-responsive">

    <form name="formulario1" method="POST"
          action=<?php echo '"'.VISTA_USUARIO . 'realizarAlta.php"' ?>>
        <table class="table">
            <tr>
                <td class="col-lg-6">Nombre de usuario*:</td>
                <td class="col-lg-6"><input type="text" name="login" max="20" min="3" required <?php echo 'value="'.$login.'"'?></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Contraseña*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="password" name="password" min="3" max="64" required></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Repetir contraseña*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="password" name="passwordRepite" min="3" max="64" required></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Nombre*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="nombre" max="20" required <?php echo 'value="'.$nombre.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Primer apellido*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="apellido1" max="20" required <?php echo 'value="'.$apellido1.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Segundo apellido:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="apellido2" max="20" <?php echo 'value="'.$apellido2.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">NIF*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="nif" max="9" required <?php echo 'value="'.$nif.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Sexo*:</td>
                <td class="col-lg-6 col-md-6 col-sm-12">
                    <div class="form-check">
                        <label>
                            <input type="radio" name="sexo" value="H" <?php if($sexo!=0){echo 'checked="checked"';}?>> Hombre
                        </label>
                        <label>
                            <input type="radio" name="sexo" value="M" <?php if($sexo==0){echo 'checked="checked"';}?>> Mujer
                        </label>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Numero de cuenta bancaria:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="numCta" max="24" <?php echo 'value="'.$numCta.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12">Como nos conocio:</td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input type="text" name="cnc" max="50" <?php echo 'value="'.$cnc.'"'?>></td>
            </tr>
            <tr>
                <td class="col-lg-6 col-md-6 col-sm-12"><input class="btn btn-danger" type="reset" name="reset" value="Borrar todo"></td>
                <td class="col-lg-6 col-md-6 col-sm-12"><input class="btn btn-primary" type="submit" name="alta" value="Registrarse"></td>
            </tr>
        </table>
    </form>
    <p class="p">Los campos con * se deben rellenar obligatoriamente</p>
</div>

</body>
</html>