<?php
require_once('../../rutas.php');
require_once "../../". PERSISTENCIA . "Pedidos.php";
require_once "../../". PERSISTENCIA . "Clientes.php";
require_once "../../". PERSISTENCIA . "Productos.php";
require_once "../../". PERSISTENCIA . "LineasPedidos.php";
require_once "../../". POJOS . "LineaPedido.php";
require_once "../../". POJOS . "Pedido.php";
require_once "../../". POJOS . "Cliente.php";
require_once "../../". POJOS . "Producto.php";
session_start();
//print_r($_SESSION);
$idUCliente = $_SESSION['idCliente'];
$direccion = "Location:../../IndexInicial.php?principal=" . VISTA_USUARIO . "carrito.php";
$error = "";
$paypal = false;
if (isset($_SESSION['valido']) && $_SESSION['valido'] == "yes" && isset($_POST['method'])) {
    $fechaPedido = date("Y-m-d");
    $activo = 1;
    if ($_POST['method'] == "contrareembolso") {
        $fechaPago = "";
        $pagado = 0;
    } elseif ($_POST['method'] == "paypal") {
        $paypal = true;
        $fechaPago = date("Y-m-d");
        $pagado = 1;
    } else {
        $fechaPago = date("Y-m-d");
        $pagado = 1;
    }
    if (!$paypal) {
        /** @var Clientes $tCliente */
        $tCliente = Clientes::singletonClientes();
        $c = $tCliente ->getUnClienteByIDUsuario($idUCliente);
        $idCliente = $c ->getIdCliente();

        $idEmpresaTransporte = "";
        $idEmpleadoEmpaqueta = "";
        $fechaEnvio = "";
        $fechaEntrega = "";
        $facturado = 0;
        $fechaFactura = "";
        $metodoPago = $_POST['method'];
        $idPedido = "";
        $idFactura = 0;
        $activo = 1;
        $id = 1;
        $direccionP = $_POST['direccion'];
//Creo el objeto Pedido con los datos base
        $p = new Pedido($id, $idPedido,
            $idEmpleadoEmpaqueta,
            $idEmpresaTransporte,
            $fechaPedido, $fechaEnvio,
            $fechaEntrega, $facturado,
            $idFactura, $fechaFactura,
            $pagado, $fechaPago,
            $metodoPago, $idCliente, $activo, $direccionP);
        /** @var Pedidos $tPedido */
        $tPedido = Pedidos::singletonPedidos();
        /** @var Productos $tProductos */
        $tProductos = Productos::singletonProductos();
//Vamos a programar un algoritmo para codificar el idPedido
//Consideramos que el idPedido va a formarse por la concatenación del año
//actual (4 dígitos) + el id (autonumérico) con 6 dígitos
//(si el id tiene menos de 6 dígitos se rellenará con ceros a la izquierda)
//por ejemplo: 2019000015 (pedido número 15 de 2019). El primer pedido del
//año será el  2019000001

//Por lo tanto, cogeremos el año según la fecha del sistema. Buscaremos en la
//tabla pedidos si hay algún pedido para ese año.
//Si no hay, éste será el primero y se codificará como AAAA000001
//Si existe un pedido, se puede contar cuántos hay sumar una unidad para
//concatenar
        $anio = substr($fechaPedido, 0, 4); //saco el año a partir de la fecha
        $numeroPedido = $tPedido->contarPedidos($anio); // averiguo cuántos hay de ese año
        if ($numeroPedido != -1) {
            $nuevoPedido = $numeroPedido + 1; //sumo 1

            $codigoString = (string)$nuevoPedido;
            $numCaracteres = strlen($nuevoPedido);
            $resta = 6 - $numCaracteres;
            for ($i = 1; $i <= $resta; $i++) {
                $codigoString = '0' . $codigoString;
            }
            $idPedido = $anio . '-' . $codigoString;

            $p->setIdPedido($idPedido);
            $insertado = $tPedido->addPedido($p);
            if ($insertado) {
                //Para cada línea que tenga en la cesta, tengo que
                // construir un objeto de tipo Lineas_Pedidos e
                // insertarlo en la tabla lineas_pedidos
                /** @var LineasPedidos $tLineasPedidos */
                $tLineasPedidos = LineasPedidos::singletonLineasPedidos();
                foreach ($_SESSION['cesta'] as $lineaCesta) {
                    $lineaCesta = unserialize($lineaCesta);
                    //fabrico el objeto
                    $lc = new LineaPedido($id, $idPedido, $lineaCesta->getIdProducto(),
                        $lineaCesta->getUnidades(), $lineaCesta->getDescripcion(),
                        $lineaCesta->getPvp(), $lineaCesta->getTipoIva(), 1);

                    $insertado = $tLineasPedidos->addLineasPedidos($lc);
                    if ($insertado) {
                        $tProductos->quitarStock($lineaCesta->getIdProducto(), $lineaCesta->getUnidades());
                    }
                } //TODO transaccion aqui
                if ($insertado) {
                    unset($_SESSION['cesta']);
                    unset($_SESSION['valido']);
                    unset($_SESSION['urlAnterior']);
                    $direccion = "Location:../../IndexInicial.php?principal=" . INFORMATIVAS . "gracias.php";
                } else {
                    $error = "Ha%20habido%20un%20error%20";
                }
            }

            $_SESSION['idPedido'] = $idPedido; //Cargamos el idPedido en la sesión
            //para poder imprimirlo en pdf

        }
    }
} else {
    if (isset($_SESSION['idPedido'])) {
        $error = "&error=Ya%20se%20ha%20relaizado%20la%20compra";
    } else {
        $error = "&error=Direccion%20no%20valida";
    }
}
if ($paypal) {
    $_SESSION['paypalDireccion'] = $_POST['direccion'];

    echo '<html><head><script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script></head><body>
<div class="loading">Un momento, por favor</div>

<form id="realizarPago" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
    <input name="cmd" type="hidden" value="_cart" />
    <input name="upload" type="hidden" value="1" />
    <input name="business" type="hidden" value="sb-dhrpk777986@business.example.com" />
    <input name="shopping_url" type="hidden" value="http://192.168.1.50/IndexInicial.php?principal=' . VISTA_USUARIO . 'carrito.php' . '" />
    <input name="currency_code" type="hidden" value="EUR" />
    <input name="return" type="hidden" value="http://192.168.1.50/' .VISTA_USUARIO. 'paypalPayment.php" />
    <input name="rm" type="hidden" value="2" />';

    $contador = 1;
    foreach ($_SESSION['cesta'] as $lineaCesta) {
        /** @var LineaPedido $lineaCesta */
        $lineaCesta = unserialize($lineaCesta);
        $nameProduct = $lineaCesta->getIdProducto();
        $price = $lineaCesta->getPvp() * (1+($lineaCesta->getTipoIva())/100);
        $quantity = $lineaCesta->getUnidades();

        echo '<input name="item_number_'.$contador.'" type="hidden" value="' . $nameProduct . '" />
    <input name="item_name_'.$contador.'" type="hidden" value="' . $nameProduct . '" />
    <input name="amount_'.$contador.'" type="hidden" value="' . $price . '" />
    <input name="quantity_'.$contador.'" type="hidden" value="' . $quantity . '" />';
        $contador ++;

    }



    echo '</form>
<script>
    $(document).ready(function () {
        $("#realizarPago").submit();
    });
</script></body>
</html>';
} else {
    header($direccion . $error);
}