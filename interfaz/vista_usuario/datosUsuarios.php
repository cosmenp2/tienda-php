<?php

/** @var Clientes $tCliente */
$tCliente = Clientes::singletonClientes();
/** @var Usuarios $tUsuario */
$tUsuario = Usuarios::singletonUsuarios();
/** @var DireccionesCliente $tDireccionesCliente */
$tDireccionesCliente = DireccionesCliente::singletonDireccionesCliente();

$usuario = $tCliente->getUnClienteByIDUsuario($_SESSION['idCliente']);
$_SESSION['urlAnterior']='IndexInicial.php?principal=' . VISTA_USUARIO . 'datosUsuarios.php';
$errorPass = "";
$successPass = "";
$errorDir = "";
$successDir = "";

if (isset($_POST['oldPass'])) {
    if ($_POST['newPass'] == $_POST['confirmationPass']) {
        $newPass = hash("sha512", $_POST['newPass']);
        $oldPass = hash("sha512", $_POST['oldPass']);

        if ($tUsuario->changePassword($_SESSION['idCliente'], $newPass, $oldPass)) {
            $successPass = "Se ha cambiado la contraseña correctamente";
        } else {
            $errorPass = "Contraseña incorrecta";
        }

    } else {
        $errorPass = "Las contraseñas no son iguales";
    }

}

if (isset($_POST['idEliminar'])) {
    if ($tDireccionesCliente->desactivarDireccion($_POST['idEliminar'])) {
        $successDir = "La direccion se ha eliminado correctamente";
    } else {
        $errorDir = "No se ha podido eliminar la direccion";
    }
}

if (isset($_GET['successDir'])){
    $successDir = $_GET['successDir'];
}

if (isset($_GET['errorDir'])){
    $errorDir = $_GET['errorDir'];
}
$direcciones = $tDireccionesCliente->getDireccionesCliente($usuario->getIdCliente());
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Datos del usuario</title>
</head>
<body>

<div class="card">
    <div class="card-header">
        Datos generales
    </div>
    <div class="card-body">
        <blockquote class="blockquote mb-0">
            <table class="table">
                <tr>
                    <td class="col-lg-6">Nombre de usuario:</td>
                    <td class="col-lg-6"><?php echo $usuario->getIdUsuario() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">Nombre:</td>
                    <td class="col-lg-6"><?php echo $usuario->getNombre() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">1º Apellido:</td>
                    <td class="col-lg-6"><?php echo $usuario->getApellido1() ?></td>
                </tr>
                <tr>
                    <td class="col-lg-6">2º Apellido:</td>
                    <td class="col-lg-6"><?php echo $usuario->getApellido2() ?></td>
                </tr>
            </table>
        </blockquote>
    </div>
</div>
<div class="card">
    <div class="card-header">
        Cambiar contraseña
    </div>
    <div class="card-body">
        <?php
        if ($errorPass != "") {
            echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> " . $errorPass . "</div>";
        } elseif ($successPass != "") {
            echo "<div class=\"alert alert-success\"><strong>Información</strong> " . $successPass . "</div>";
        }
        ?>
        <blockquote class="blockquote mb-0">

            <form name="formularioChangePass" method="POST"
                  action=<?php echo '"IndexInicial.php?principal=' . VISTA_USUARIO . 'datosUsuarios.php"' ?>>
                <table class="table">
                    <tr>
                        <td class="col-lg-6">Antigua contraseña:</td>
                        <td class="col-lg-6"><input type="password" name="oldPass" min="3" max="64" required></td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">Nueva contraseña:</td>
                        <td class="col-lg-6"><input type="password" name="newPass" min="3" max="64" required></td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">Repite la contraseña:</td>
                        <td class="col-lg-6"><input type="password" name="confirmationPass" min="3" max="64" required>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-lg-6 col-md-6 col-sm-12"></td>
                        <td class="col-lg-6 col-md-6 col-sm-12"><input class="btn btn-primary" type="submit" name="changePass"
                                                                       value="Cambiar contraseña"></td>
                    </tr>
                </table>
            </form>
        </blockquote>
    </div>
</div>
<div class="card">
    <div class="card-header">
        Direcciones
    </div>
    <div class="card-body">
        <?php
        if ($errorDir != "") {
            echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> " . $errorDir . "</div>";
        } elseif ($successDir != "") {
            echo "<div class=\"alert alert-success\"><strong>Información</strong> " . $successDir . "</div>";
        }
        ?>
        <blockquote class="blockquote mb-0">
            <table class="table">
                <?php
                if (sizeof($direcciones) > 0) {
                    /** @var DireccionCliente $d */
                    foreach ($direcciones as $d) {
                        echo '<tr>
                    <td class="col-lg-6">' . $d->getDireccion() . '. ' . $d->getCodPostal() . ' ' . $d->getLocalidad() . ' (' .
                            $d->getProvincia() . ') ,' . $d->getPais() . '.</td>
                    <td class="col-lg-6">
                    <form name="formularioModDirection" method="POST"
                        action="IndexInicial.php?principal=' . VISTA_USUARIO . 'formularioDireccion.php" >
                        <input type="hidden" name="idDir" value="' . $d->getId() . '">
                        <input type="hidden" name="direccion" value="' . $d->getDireccion() . '">
                        <input type="hidden" name="codPostal" value="' . $d->getCodPostal() . '">
                        <input type="hidden" name="localidad" value="' . $d->getLocalidad() . '">
                        <input type="hidden" name="provincia" value="' . $d->getProvincia() . '">
                        <input type="hidden" name="pais" value="' . $d->getPais() . '">
                        <input class="btn btn-info" type="submit" name="editDirection" value="Editar">
                     </form>
                     <form name="formularioModDirection" method="POST"
                        action="IndexInicial.php?principal=' . VISTA_USUARIO . 'datosUsuarios.php" >
                        <input type="hidden" name="idEliminar" value="' . $d->getId() . '">
                        <input class="btn btn-danger" type="submit" name="removeDirection" value="Eliminar">
                      </form>
                      </td>
                    </tr>';
                    }
                }
                ?>
                <tr>
                    <td class="col-lg-6"></td>
                    <td class="col-lg-6">
                        <a class="btn btn-primary"
                            <?php echo 'href="IndexInicial.php?principal=' . VISTA_USUARIO . 'formularioDireccion.php"' ?>>Añadir
                            direccion</a>
                    </td>
                </tr>
            </table>
        </blockquote>
    </div>
</div>

</body>
</html>