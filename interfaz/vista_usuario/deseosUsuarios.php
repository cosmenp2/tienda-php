<?php

/** @var Deseos $tDeseos */
$tDeseos = Deseos::singletonDeseos();
/** @var Productos $tProductos */
$tProductos = Productos::singletonProductos();
/** @var Clientes $tCliente */
$tCliente = Clientes::singletonClientes();
$c = $tCliente ->getUnClienteByIDUsuario($_SESSION['idCliente']);
$idCliente = $c ->getIdCliente();

$errorDir = "";
$successDir = "";

if (isset($_POST['idProducto'])) {
    if ($tDeseos->bajaDeseo($idCliente, $_POST['idProducto'])) {
        $successDir = "Se ha eliminado el elemento de tu lista de deseos";
    } else {
        $errorDir = "No se ha podido eliminar el elemento";
    }
}

if (isset($_GET['successDir'])) {
    $successDir = $_GET['successDir'];
}

if (isset($_GET['errorDir'])) {
    $errorDir = $_GET['errorDir'];
}
$deseos = $tDeseos->deseosByIdCliente($idCliente);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Datos del usuario</title>
</head>
<body>
<div class="card">
    <div class="card-header">
        Lista de deseos
    </div>
    <div class="card-body">
        <?php
        if ($errorDir != "") {
            echo "<div class=\"alert alert-danger\"><strong>¡Error!</strong> " . $errorDir . "</div>";
        } elseif ($successDir != "") {
            echo "<div class=\"alert alert-success\"><strong>Información</strong> " . $successDir . "</div>";
        }
        ?>
        <blockquote class="blockquote mb-0">

            <?php
            if (sizeof($deseos) > 0) {
                echo "<table class=\"table\">
                        <tr>
                            <th>Foto</th>
                            <th>Id Producto</th>
                            <th>Descripcion</th>
                            <th>Precio</th>
                            <th>Stock</th>
                            <th>Estado</th>
                            <th>Eliminar</th>
                        </tr>";
                /** @var Deseo $d */
                foreach ($deseos as $d) {
                    $p = $tProductos->getProductoById($d->getIdProducto());
                    $estado = ($p->getActivo() == '1')?($p->getStockActual() > 0)?"Disponible":"Sin stock":"Descatalogado";
                    echo '<tr><td>
                    <img  class="card-img-top" src="' . FOTOS . $p->getRutaFoto() . '"
              alt="' . $p->getDescripcion() . '" style="width:80px;height: 80px">
              </td>
                    <td>'.$p->getIdProducto().'</td>
                    <td>'.$p->getDescripcion().'</td>
                    <td>'.$p->getPvp().'</td>
                    <td>'.$p->getStockActual().'</td>
                    <td>'.$estado.'</td>
                    <td class="col-lg-6">
                     <form name="formularioModDirection" method="POST"
                        action="IndexInicial.php?principal=' . VISTA_USUARIO . 'deseosUsuarios.php" >
                        <input type="hidden" name="idProducto" value="' . $d->getIdProducto() . '">
                        <input class="btn btn-danger" type="submit" name="removeDirection" value="X">
                      </form>
                      </td>
                    </tr>';
                }
                echo "<tr>
                    <td class=\"col-lg-6\"></td>
                    <td class=\"col-lg-6\"></td>
                    <td class=\"col-lg-6\"></td>
                    <td class=\"col-lg-6\"></td>
                    <td class=\"col-lg-6\"></td>
                    <td class=\"col-lg-6\"></td>
                    <td class=\"col-lg-6\">
                        <a class=\"btn btn-primary\"
                            href=\"IndexInicial.php?principal=" . VISTA_USUARIO . "catalogo_productos_carrito.php\">Añadir
                            elementos</a>
                    </td>
                </tr>
            </table>";
            } else {
                echo "<h1 class='display-4'>No tiene ningún elemento en la lista de deseos</h1>";
                echo "<a class=\"btn btn-primary\"
                            href=\"IndexInicial.php?principal=" . VISTA_USUARIO . "catalogo_productos_carrito.php\">Añadir
                            elementos</a>";
            }
            ?>

        </blockquote>
    </div>
</div>

</body>
</html>