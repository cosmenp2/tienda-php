<?php
ob_end_clean(); //libera el buffer de salida
//OJO!! Los bordes de las tablas no se imprimen a menos que se pongan
//los atributos entre comillas dobles ""
require_once  ('../../rutas.php');
require_once "../../". PERSISTENCIA.'Clientes.php';
require_once "../../". POJOS.'Cliente.php';
require_once "../../". LIBTCPDF;

//Tenemos que generar un html que contenga tod o lo que
//queremos imprimir
/*En la hoja de pedido tienen que aparecer los datos del cliente
     * así como los datos de cada uno de los productos,
     * por eso necesitamos tener las conexiones a cada una de
     * las tablas: clientes, productos, pedidos y lineas_pedidos
*/
/** @var Clientes $tClientes */
$tClientes = Clientes::singletonClientes();

$htmlIntro = "";
$htmlInfo = "";
$title = "";
$htmlIntro = $htmlIntro . "<p>Resultados de la busqueda de clientes: </p><br/>";
$title = "Clientes: ";
$total = 0;

$htmlInfo = "<table border=\"1\">
			<tr>
				<td>IdCliente</td>
				<td>Primer apellido</td>
				<td>Segundo apellido</td>
				<td>Nombre</td>
				<td>Nif</td>
				<td>Compras</td>
			</tr>";

foreach ($_POST['clientes'] as $impClientes) {
    $c = $tClientes->getUnCliente($impClientes);
    $totalCompras = $tClientes->getTotalComprasByID($c->getIdCliente());
    $total += $totalCompras;
    $htmlInfo = $htmlInfo . "<tr>"
        . "<td>" . $c->getIdCliente() . "</td>"
        . "<td>" . $c->getApellido1() . "</td>"
        . "<td>" . $c->getApellido2() . "</td>"
        . "<td>" . $c->getNombre() . "</td>"
        . "<td>" . $c->getNif() . "</td>"
        . "<td>" . $totalCompras . "</td>"
        . " </tr>";
}
$htmlInfo = $htmlInfo . "</table>";


// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT,
    PDF_PAGE_FORMAT, true, 'UTF-8', true);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Empresa, SL');
$pdf->SetTitle($title);

//En el archivo tcpdf_autoconfig.php se puede cambiar la ruta
//del logo de la empresa.
// set default header data

$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH - 12,
    "Empresa, S.L.", "Avda. Ramón y Cajal, s/n.\n "
    . "06001 Badajoz \n CIF: B-0611111 \n Tlf: 924010101");

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// ---------------------------------------------------------

// set font
$pdf->SetFont('Times', 'B', 16);

// add a page
$pdf->AddPage();

$pdf->Write(15, 'Detalles de la busqueda', '', 0, 'C', true, 0, false, false, 0);

$pdf->SetFont('helvetica', '', 8);
//$pdf->Write(16, '', '', 0, 'L', true, 0, false, false, 0);
// -----------------------------------------------------------------------------
$pdf->writeHTML($htmlIntro, true, false, false, false, '');
$pdf->SetFont('Times', 'B', 14);
$pdf->Write(15, $title, '', 0, 'C', true, 0, false, false, 0);
$pdf->SetFont('Times', 'B', 8);

$pdf->writeHTML($htmlInfo, true, false, false, false, '');
$pdf->Write(15, "Total comprado por estos clientes: $total", '', 0, 'R', true, 0, false, false, 0);

$pdf->SetFont('Times', 'B', 10);
$pdf->SetTextColor(0, 0, 255);
//Close and output PDF document
$pdf->lastPage();
//header('Content-type: application/pdf');
//header('Content-Disposition: attachment; filename="file.pdf"');
$pdf->Output('pedido.pdf');
//$pdf->Output('example_048.pdf', 'I');

//============================================================+
// END OF FILE
