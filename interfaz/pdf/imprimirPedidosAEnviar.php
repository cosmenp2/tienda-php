<?php
ob_end_clean();
require_once  '../../rutas.php';
require_once "../../". LIBTCPDF;
require_once "../../". POJOS.'Pedido.php';
require_once "../../". PERSISTENCIA.'Pedidos.php';
require_once "../../". POJOS.'LineaPedido.php';
require_once "../../". PERSISTENCIA.'LineasPedidos.php';
require_once "../../". POJOS.'Cliente.php';
require_once "../../". POJOS.'DireccionCliente.php';
require_once "../../". PERSISTENCIA.'Clientes.php';
require_once "../../". PERSISTENCIA.'DireccionesCliente.php';

$htmlInfo = "";
$title = "Pedidos a Enviar";

/** @var Pedidos $tP */
$tP = Pedidos::singletonPedidos();

/** @var LineasPedidos $tLinea */
$tLinea = LineasPedidos::singletonLineasPedidos();
/** @var Clientes $tC */
$tC = Clientes::singletonClientes();
/** @var DireccionesCliente $tDC */
$tDC = DireccionesCliente::singletonDireccionesCliente();


$tablaPedidos = $tP->getPedidosAEnviar();

if (sizeof($tablaPedidos)>0) {
    $htmlInfo .= "<table border=\"1\">
			<tr>
				<td>Número de pedido</td>
				<td>Cliente</td>
				<td>Primer apellido</td>
				<td>Segundo apellido</td>
				<td>Dirección</td>
			</tr>";
    /** @var Pedido $p */
    foreach ($tablaPedidos as $p) {
        $c = $tC->getUnCliente($p->getIdCliente());
        $dC = $tDC->getDireccionCliente($p->getIdCliente());
        $htmlInfo .= "<tr>"
            . "<td>" . $p->getIdPedido() . "</td>"
            . "<td>" . $p->getIdCliente() . "</td>"
            . "<td>" . $c->getApellido1() . "</td>"
            . "<td>" . $c->getApellido2() . "</td>"
            . "<td>" . $dC->getDireccion() . "</td>"
            . "</tr>";
    }
    $htmlInfo .= "</table>";
} else {
    $htmlInfo .= "<h1 style='color: #00ff00'>No se ha encontrado ningún pedido pendiente de enviar</h1>";
}


// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT,
    PDF_PAGE_FORMAT, true, 'UTF-8', true);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Empresa, SL');
$pdf->SetTitle($title);

//En el archivo tcpdf_autoconfig.php se puede cambiar la ruta
//del logo de la empresa.
// set default header data

$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH - 12,
    "Empresa, S.L.", "Avda. Ramón y Cajal, s/n.\n "
    . "06001 Badajoz \n CIF: B-0611111 \n Tlf: 924010101");

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// ---------------------------------------------------------

// set font
$pdf->SetFont('Times', 'B', 16);

// add a page
$pdf->AddPage();

$pdf->SetFont('helvetica', '', 8);
//$pdf->Write(16, '', '', 0, 'L', true, 0, false, false, 0);
// -----------------------------------------------------------------------------
$pdf->Write(15, $title, '', 0, 'C', true, 0, false, false, 0);
$pdf->SetFont('Times', 'B', 8);

$pdf->writeHTML($htmlInfo, true, false, false, false, '');
$pdf->SetFont('Times', 'B', 10);
$pdf->SetTextColor(0, 0, 255);
//Close and output PDF document
$pdf->lastPage();
//header('Content-type: application/pdf');
//header('Content-Disposition: attachment; filename="file.pdf"');
$pdf->Output('pedido.pdf');
//$pdf->Output('example_048.pdf', 'I');
