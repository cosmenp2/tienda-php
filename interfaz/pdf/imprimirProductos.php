<?php
require_once '../../rutas.php';
require_once "../../" . POJOS . 'Producto.php';
require_once "../../" . POJOS . 'FamiliaProducto.php';
require_once "../../" . PERSISTENCIA . 'Productos.php';
require_once "../../" . PERSISTENCIA . 'FamiliasProductos.php';
require_once "../../" . LIBTCPDF;


/** @var Productos $tP */
$tP = Productos::singletonProductos();
/** @var FamiliasProductos $tFamilia */
$tFamilia = FamiliasProductos::singletonFamiliasProductos();
$tablaProductos = $tP->getProductosTodos();
$mesActual = date("m/Y");

$htmlIntro = "<h1>Catálogo de nuestros productos ".$mesActual." </h1>";

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT,
    PDF_PAGE_FORMAT, true, 'UTF-8', true);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Empresa, SL');
$pdf->SetTitle("Catalogo");

//En el archivo tcpdf_autoconfig.php se puede cambiar la ruta
//del logo de la empresa.
// set default header data

$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH - 12,
    "Empresa, S.L.", "Avda. Ramón y Cajal, s/n.\n "
    . "06001 Badajoz \n CIF: B-0611111 \n Tlf: 924010101");

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// ---------------------------------------------------------

// set font
//$pdf->SetFont('Times', 'B', 16);

// add a page
$pdf->AddPage();


$pdf->SetFont('helvetica', '', 8);
//$pdf->Write(16, '', '', 0, 'L', true, 0, false, false, 0);
// -----------------------------------------------------------------------------
$pdf->writeHTML($htmlIntro, true, false, false, false, 'C');
$pdf->SetFont('Times', 'B', 14);
$pdf->SetFont('Times', 'B', 8);

if (!is_null($tablaProductos)) {
    $codFamiliaAnterior="";
    /** @var Producto $p */
    foreach ($tablaProductos as $p) {
        if ($p->getIdFamilia() != $codFamiliaAnterior){
            $pdf->AddPage();
            $codFamiliaAnterior = $p->getIdFamilia();
            $familia = $tFamilia->getFamiliaById($codFamiliaAnterior);
            $htmlInfo = "<h1>" . $familia->getNombre() . "</h1>";
            $pdf->writeHTML($htmlInfo, true, false, false, false, 'C');
        }
        $pdf->AddPage();
        $htmlInfo = ""
            . "<h1>" . $p->getIdProducto() . "</h1>"
            . "<table><tr>"
            . "<td>" . $p->getDescripcion() . "</td>"
            . "<td><img src=\"../../" . FOTOS . $p->getRutaFoto() . "\" border='0' width='150' height='100' /></td>"
            . "</tr></table>"
            . "<h2>Precio: " . $p->getPvp() . "€</h2>"
            . "<h3>IVA: " . $p->getTipoIva() . "%</h3>"
            . "<h4>Stock: " . $p->getStockActual() . " unidades</h4>";
        $pdf->writeHTML($htmlInfo, true, false, false, false, '');
    }
}

$pdf->SetFont('Times', 'B', 10);
$pdf->SetTextColor(0, 0, 255);
//Close and output PDF document
$pdf->lastPage();
//header('Content-type: application/pdf');
//header('Content-Disposition: attachment; filename="file.pdf"');
$pdf->Output('pedido.pdf');
//$pdf->Output('example_048.pdf', 'I');
