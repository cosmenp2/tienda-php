<?php
ob_end_clean();
require_once  ('../../rutas.php');
require_once "../../". LIBTCPDF;
require_once "../../". POJOS.'Pedido.php';
require_once "../../". PERSISTENCIA.'Pedidos.php';
require_once "../../". POJOS.'LineaPedido.php';
require_once "../../". PERSISTENCIA.'LineasPedidos.php';
$htmlIntro = "";
$htmlInfo = "";
$title = "Pedidos";

$fechaInicio = $_POST['fechaI'];
$fechaFinal = $_POST['fechaF'];
/** @var Pedidos $tP */
$tP = Pedidos::singletonPedidos();
/** @var LineasPedidos $tLinea */
$tLinea = LineasPedidos::singletonLineasPedidos();

if (empty($fechaInicio) && empty($fechaFinal)) {
    $tablaPedidos = $tP->getPedidosTodos();
    $htmlIntro = $htmlIntro . "<p>Listado de todos los pedidos</p><br/>";
} else {
    $tablaPedidos = $tP->getPedidosByDate($fechaInicio, $fechaFinal);

    $htmlIntro .= "<p>Listado de todos los pedidos";
    if (!empty($fechaInicio)) {
        $htmlIntro .= " desde el " . $fechaInicio;
    }
    if (!empty($fechaFinal)) {
        $htmlIntro .= " hasta el " . $fechaFinal;
    }
    $htmlIntro .= "</p><br/>";
}

if (!is_null($tablaPedidos)) {
    $htmlInfo .= "<table border=\"1\">
			<tr>
				<td>Número de factura</td>
				<td>Número de pedido</td>
				<td>Fecha de pedido</td>
				<td>Cliente</td>
				<td>Base imponible</td>
			</tr>";
    /** @var Pedido $p */
    foreach ($tablaPedidos as $p) {
        $baseImponible = $tLinea->getTotalUnPedido($p->getIdPedido());
        $htmlInfo .= "<tr>"
        . "<td>" . $p->getIdFactura() . "</td>"
        . "<td>" . $p->getIdPedido() . "</td>"
        . "<td>" . $p->getFechaPedido() . "</td>"
        . "<td>" . $p->getIdCliente() . "</td>"
        . "<td>" . $baseImponible . "</td>"
        . "</tr>";
    }
    $htmlInfo .= "</table>";
}


// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT,
    PDF_PAGE_FORMAT, true, 'UTF-8', true);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Empresa, SL');
$pdf->SetTitle($title);

//En el archivo tcpdf_autoconfig.php se puede cambiar la ruta
//del logo de la empresa.
// set default header data

$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH - 12,
    "Empresa, S.L.", "Avda. Ramón y Cajal, s/n.\n "
    . "06001 Badajoz \n CIF: B-0611111 \n Tlf: 924010101");

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// ---------------------------------------------------------

// set font
$pdf->SetFont('Times', 'B', 16);

// add a page
$pdf->AddPage();

$pdf->Write(15, 'Detalles de la busqueda', '', 0, 'C', true, 0, false, false, 0);

$pdf->SetFont('helvetica', '', 8);
//$pdf->Write(16, '', '', 0, 'L', true, 0, false, false, 0);
// -----------------------------------------------------------------------------
$pdf->writeHTML($htmlIntro, true, false, false, false, '');
$pdf->SetFont('Times', 'B', 14);
$pdf->Write(15, $title, '', 0, 'C', true, 0, false, false, 0);
$pdf->SetFont('Times', 'B', 8);

$pdf->writeHTML($htmlInfo, true, false, false, false, '');
$pdf->SetFont('Times', 'B', 10);
$pdf->SetTextColor(0, 0, 255);
//Close and output PDF document
$pdf->lastPage();
//header('Content-type: application/pdf');
//header('Content-Disposition: attachment; filename="file.pdf"');
$pdf->Output('pedido.pdf');
//$pdf->Output('example_048.pdf', 'I');
