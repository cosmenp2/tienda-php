<?php
session_start();
ob_end_clean(); //libera el buffer de salida
//OJO!! Los bordes de las tablas no se imprimen a menos que se pongan
//los atributos entre comillas dobles ""
if (isset($_POST['pdf'])) {
    include('../../rutas.php');
    require_once "../../". PERSISTENCIA . 'Pedidos.php';
    require_once "../../". PERSISTENCIA . 'Empleados.php';
    require_once "../../". POJOS . 'Pedido.php';
    require_once "../../". POJOS . 'Empleado.php';
    require_once "../../". LIBTCPDF;

    //Tenemos que generar un html que contenga tod o lo que
    //queremos imprimir
    /*En la hoja de pedido tienen que aparecer los datos del cliente
         * así como los datos de cada uno de los productos,
         * por eso necesitamos tener las conexiones a cada una de
         * las tablas: clientes, productos, pedidos y lineas_pedidos
*/
    /** @var Pedidos $tpedido */
    $tpedido = Pedidos::singletonPedidos();
    /** @var Empleados $tEmpleado */
    $tEmpleado = Empleados::singletonEmpleados();
    $htmlIntro = "";
    $htmlInfo = "";
    $title = "";
    if (isset($_POST['codPostal'])) {
        $htmlIntro = $htmlIntro . "<p>Resultados de la busqueda de todos los empleados con el codigo postal: " . $_POST['codPostal'] . ". </p><br/>";
        $empleados = $tEmpleado->getByCodPostal($_POST['codPostal']);
        $title = "Empleados por código postal: " . $_POST['codPostal'];
        $htmlInfo = $htmlInfo . "<table border=\"1\">
			<tr>
				<td>IdEmpleado</td>
				<td>IdUsusario</td>
				<td>Nif</td>
				<td>Nombre</td>
				<td>Primer apellido</td>
				<td>Segundo apellido</td>
				<td>Numcta</td>
				<td>movil</td>
				<td>foto</td>
				<td>Activo</td>
			</tr>";
        /** @var Empleado $e */
        foreach ($empleados as $e) {
            $htmlInfo = $htmlInfo . "<tr>"
                . "<td>" . $e->getIdEmpleado() . "</td>"
                . "<td>" . $e->getIdUsuario() . "</td>"
                . "<td>" . $e->getNif() . "</td>"
                . "<td>" . $e->getNombre() . "</td>"
                . "<td>" . $e->getApellido1() . "</td>"
                . "<td>" . $e->getApellido2() . "</td>"
                . "<td>" . $e->getNumcta() . "</td>"
                . "<td>" . $e->getMovil() . "</td>"
                . "<td><img src=\"../../" . FOTOS . $e->getRutaFoto() . "\" border='0' width='150' height='100' /></td>"
                . "<td>" . $e->getActivo() . "</td>"
                . " </tr>";
        }
        $htmlInfo = $htmlInfo . "</table>";
    } else {
        $idEmpleado = $_POST['idEmpleado'];
        $empleado = $tEmpleado->getEmpleado($idEmpleado);
        $pedidos = $tpedido->getPedidosEmpaquetados($idEmpleado);
        $title = "Pedidos empaquetados por el empleado: $idEmpleado";

        $htmlIntro = $htmlIntro . "<h4>Pedidos empaquetados por: </h4>" .
            "<table border=\"1\">
			<tr>
				<td>IdEmpleado</td>
				<td>IdUsusario</td>
				<td>Nif</td>
				<td>Nombre</td>
				<td>Primer apellido</td>
				<td>Segundo apellido</td>
				<td>Numcta</td>
				<td>movil</td>
				<td>foto</td>
				<td>Activo</td>
			</tr>"
            . "<tr>"
            . "<td>" . $empleado->getIdEmpleado() . "</td>"
            . "<td>" . $empleado->getIdUsuario() . "</td>"
            . "<td>" . $empleado->getNif() . "</td>"
            . "<td>" . $empleado->getNombre() . "</td>"
            . "<td>" . $empleado->getApellido1() . "</td>"
            . "<td>" . $empleado->getApellido2() . "</td>"
            . "<td>" . $empleado->getNumcta() . "</td>"
            . "<td>" . $empleado->getMovil() . "</td>"
            . "<td><img src=\"../../" .FOTOS. $empleado->getRutaFoto() . "\" border='0' width='150' height='100' /></td>"
            . "<td>" . $empleado->getActivo() . "</td>"
            . " </tr>" .
            "</table>";

        $htmlInfo = $htmlInfo . "<table border=\"1\">
			<tr>
				<td>Empresa de transporte</td>
				<td>Fecha del pedido</td>
				<td>Fecha de envio</td>
				<td>Fecha de entrega</td>
				<td>Facturado</td>
				<td>Factura</td>
				<td>Fecha factura</td>
				<td>Pagado</td>
				<td>Fecha pago</td>
				<td>Metodo de pago</td>
				<td>Cliente</td>
				<td>Activo</td>
			</tr>";

        foreach ($pedidos as $p) {
            $htmlInfo = $htmlInfo . "<tr>"
                . "<td>" . $p->getIdEmpresaTransporte() . "</td>"
                . "<td>" . $p->getFechaPedido() . "</td>"
                . "<td>" . $p->getFechaEnvio() . "</td>"
                . "<td>" . $p->getFechaEntrega() . "</td>"
                . "<td>" . (($p->getFacturado() == 1) ? "Si" : "No") . "</td>"
                . "<td>" . $p->getIdFactura() . "</td>"
                . "<td>" . $p->getFechaFactura() . "</td>"
                . "<td>" . (($p->getPagado() == 1) ? "Si" : "No") . "</td>"
                . "<td>" . $p->getFechaPago() . "</td>"
                . "<td>" . $p->getMetodoPago() . "</td>"
                . "<td>" . $p->getIdCliente() . "</td>"
                . "<td>" . (($p->getActivo() == 1) ? "Si" : "No") . "</td>"
                . " </tr>";
        }
        $htmlInfo = $htmlInfo . "</table>";

    }


// create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT,
        PDF_PAGE_FORMAT, true, 'UTF-8', true);

// set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Empresa, SL');
    $pdf->SetTitle($title);

//En el archivo tcpdf_autoconfig.php se puede cambiar la ruta
    //del logo de la empresa.
    // set default header data

    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH - 12,
        "Empresa, S.L.", "Avda. Ramón y Cajal, s/n.\n "
        . "06001 Badajoz \n CIF: B-0611111 \n Tlf: 924010101");

// set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// ---------------------------------------------------------

// set font
    $pdf->SetFont('Times', 'B', 16);

// add a page
    $pdf->AddPage();

    $pdf->Write(15, 'Detalles de la busqueda', '', 0, 'C', true, 0, false, false, 0);

    $pdf->SetFont('helvetica', '', 8);
    //$pdf->Write(16, '', '', 0, 'L', true, 0, false, false, 0);
    // -----------------------------------------------------------------------------
    $pdf->writeHTML($htmlIntro, true, false, false, false, '');
    $pdf->SetFont('Times', 'B', 14);
    $pdf->Write(15, $title, '', 0, 'C', true, 0, false, false, 0);
    $pdf->SetFont('Times', 'B', 8);

    $pdf->writeHTML($htmlInfo, true, false, false, false, '');
    $pdf->SetFont('Times', 'B', 10);
    $pdf->SetTextColor(0, 0, 255);
    //Close and output PDF document
    $pdf->lastPage();
//header('Content-type: application/pdf');
    //header('Content-Disposition: attachment; filename="file.pdf"');
    $pdf->Output('pedido.pdf');
//$pdf->Output('example_048.pdf', 'I');

//============================================================+
    // END OF FILE
    //============================================================+

}
