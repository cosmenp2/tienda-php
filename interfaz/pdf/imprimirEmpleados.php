<?php
session_start();
ob_end_clean(); //libera el buffer de salida
//OJO!! Los bordes de las tablas no se imprimen a menos que se pongan
//los atributos entre comillas dobles ""
require_once  ('../../rutas.php');
require_once "../../". PERSISTENCIA.'Pedidos.php';
require_once "../../". PERSISTENCIA.'Empleados.php';
require_once "../../". POJOS.'Pedido.php';
require_once "../../". POJOS.'Empleado.php';
require_once "../../". LIBTCPDF;
require_once "../../". LIBSVGGRAPH;

//Tenemos que generar un html que contenga tod o lo que
//queremos imprimir
/*En la hoja de pedido tienen que aparecer los datos del cliente
     * así como los datos de cada uno de los productos,
     * por eso necesitamos tener las conexiones a cada una de
     * las tablas: clientes, productos, pedidos y lineas_pedidos
*/
//
$settings = array(
    'back_colour' => 'white',
    'graph_title' => 'Pedidos realizados por los empleados',
    'stroke_width' => 0,
    'thousands' => ".",
    'decimal' => ","

);
$graph = new Goat1000\SVGGraph\SVGGraph(500, 200,$settings);

/** @var Pedidos $tPedido */
$tPedido = Pedidos::singletonPedidos();
/** @var Empleados $tEmpleado */
$tEmpleado = Empleados::singletonEmpleados();
$htmlIntro = "";
$htmlInfo = "";
$title = "";
$stats = array();

$htmlIntro = $htmlIntro . "<p>Listado de todos los empleados</p><br/>";
$empleados = $tEmpleado->getEmpleadosTodos(true,true);
$title = "Empleados";
$htmlInfo = $htmlInfo . "<table border=\"1\">
			<tr>
				<td>IdEmpleado</td>
				<td>Primer apellido</td>
				<td>Segundo apellido</td>
				<td>Nombre</td>
				<td>Nif</td>
				<td>Foto</td>
				<td>Pedidos tramitados</td>
			</tr>";
/** @var Empleado $e */
foreach ($empleados as $e) {
    $pedidosTramitados = sizeof($tPedido->getPedidosEmpaquetados($e->getIdEmpleado()));
    $htmlInfo = $htmlInfo . "<tr>"
        . "<td>" . $e->getIdEmpleado() . "</td>"
        . "<td>" . $e->getApellido1() . "</td>"
        . "<td>" . $e->getApellido2() . "</td>"
        . "<td>" . $e->getNombre() . "</td>"
        . "<td>" . $e->getNif() . "</td>"
        . "<td><img src=\"../../".FOTOS.$e->getRutaFoto() . "\" border='0' width='150' height='100' /></td>"
        . "<td>" . $pedidosTramitados . "</td>"
        . " </tr>";
    $stats[$e->getIdEmpleado()] =$pedidosTramitados;
}
$htmlInfo = $htmlInfo . "</table>";
$graph->values($stats);
$output =  $graph->fetch('BarGraph',false,false);


// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT,
    PDF_PAGE_FORMAT, true, 'UTF-8', true);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Empresa, SL');
$pdf->SetTitle($title);

//En el archivo tcpdf_autoconfig.php se puede cambiar la ruta
//del logo de la empresa.
// set default header data

$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH - 12,
    "Empresa, S.L.", "Avda. Ramón y Cajal, s/n.\n "
    . "06001 Badajoz \n CIF: B-0611111 \n Tlf: 924010101");

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// ---------------------------------------------------------

// set font
$pdf->SetFont('Times', 'B', 16);

// add a page
$pdf->AddPage();

$pdf->Write(15, 'Detalles de la busqueda', '', 0, 'C', true, 0, false, false, 0);

$pdf->SetFont('helvetica', '', 8);
//$pdf->Write(16, '', '', 0, 'L', true, 0, false, false, 0);
// -----------------------------------------------------------------------------
$pdf->writeHTML($htmlIntro, true, false, false, false, '');
$pdf->SetFont('Times', 'B', 14);
$pdf->Write(15, $title, '', 0, 'C', true, 0, false, false, 0);
$pdf->SetFont('Times', 'B', 8);

$pdf->writeHTML($htmlInfo, true, false, false, false, '');
$pdf->ImageSVG('@'.$output);
$pdf->SetFont('Times', 'B', 10);
$pdf->SetTextColor(0, 0, 255);
//Close and output PDF document
$pdf->lastPage();
//header('Content-type: application/pdf');
//header('Content-Disposition: attachment; filename="file.pdf"');
$pdf->Output('pedido.pdf');
//$pdf->Output('example_048.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+


