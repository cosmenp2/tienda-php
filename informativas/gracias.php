<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gracias por su compra</title>
</head>
<body>
<h1 class="display-4">Muchas gracias por su compra</h1>
<p>Si lo desea puede descargarse el pedido <a class="nav-link" <?php echo 'href="' . PDF . 'imprimirPedidoPDF.php"' ?>>
        aquí</a> o <a class="nav-link" <?php echo 'href="IndexInicial.php?principal=' . VISTA_USUARIO . 'pedidosUsuarios.php"' ?>>
        acceder a sus pedidos realizados</a>.</p>
<img <?php echo 'src=' . ICONOS.'me-gusta.png' ?> style="max-width:10%;width:auto;height:auto;" align="center" alt="Gracias!!!" >
</body>
</html>
