<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gracias por registrarse</title>
</head>
<body>
<h1 class="display-4">Muchas gracias por registrarse</h1>
<p>Ahora puede
    <a class="nav-link" <?php echo 'href="IndexInicial.php?principal=' . VISTA_USUARIO . 'catalogo_productos_carrito.php"' ?>>
        seguir comprando</a> o <a class="nav-link" <?php echo 'href="IndexInicial.php?principal=' . VISTA_USUARIO . 'datosUsuarios.php"' ?>>
    completar los datos de su cuenta</a>.
</body>
</html>