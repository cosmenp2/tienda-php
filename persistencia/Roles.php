<?php
require_once 'Conexion.php';

class Roles
{

    private static $instancia;
    /** @var  Conexion db */
    private $db;

    function __construct()
    {

        $this->db = Conexion::singleton_conexion();
    }

    public static function singletonRoles()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function addUnRol($r)
    {
        //var_dump($c);
        try {
            $consulta = "INSERT INTO roles (id,nombre) "
                . "values " . "(null,?)";

            $query = $this->db->preparar($consulta);
            /** @var Rol $r */
            @$query->bindParam(1, $r->getNombre());
            $insertado = $query->execute();

        } catch (Exception $ex) {
            $insertado = false;
        }

        return $insertado;
    }

    public function buscaRol($nombre)
    {
        //Conseguimos un cliente concreto que tenga en el id_cliente el
        //código que entra como parámetro en el método.
        try {
            $consulta = "SELECT * FROM roles WHERE nombre= ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $nombre);
            $query->execute();
            $tRoles = $query->fetchAll();
        } catch (Exception $ex) {
            echo "Se ha producido un error en buscaRol";
        }
        if (empty($tRoles)) {
            $r = null;
        } else {
            $r = new Rol($tRoles[0][0], $tRoles[0][1]);
        }
        return $r;
    }

    public function deleteRol($idRol)
    {
        $tRoles = array();
        try {
            $consulta = "DELETE FROM roles WHERE id= ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $idRol);
            $correcto = $query->execute();

        } catch (Exception $ex) {
            $correcto = false;
        }
        return $correcto;
    }

    public function buscaRolById($id)
    {
        try {
            $consulta = "SELECT * FROM roles WHERE id= ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $id);
            $query->execute();
            $tRoles = $query->fetchAll();
        } catch (Exception $ex) {
            echo "Se ha producido un error en buscaRolById";
        }
        if (empty($tRoles)) {
            $r = null;
        } else {
            $r = new Rol($tRoles[0][0], $tRoles[0][1]);
        }
        return $r;
    }

    public function getTodos()
    {
        $tablaRoles = array();
        try {
            $consulta = "SELECT * FROM roles ";

            $query = $this->db->preparar($consulta);
            $error = $query->execute();
            $tRoles = $query->fetchAll();

        } catch (Exception $ex) {
            $error = true;
            var_dump($ex);
        }

        foreach ($tRoles as $tRol) {
            $r = new Rol($tRol['id'], $tRol['nombre']);
            array_push($tablaRoles, $r);
        }

        return $tablaRoles;
    }
}