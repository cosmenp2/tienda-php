<?php
require_once 'Conexion.php';
require_once 'DireccionesEmpleado.php';

class Empleados
{
    private static $instancia;
    /** @var Conexion */
    private $db;

    function __construct()
    {
        $this->db = Conexion::singleton_conexion();
    }

    public static function singletonEmpleados()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;

        }

        return self::$instancia;
    }

    public function addUnEmpleado($e)
    {

        //var_dump($e);
        try {
            $consulta = "INSERT INTO empleados (id,id_empleado,id_usuario,nif,nombre,apellido1,apellido2,numcta,movil,ruta_foto,activo) "
                . "values " . "(null,?,?,?,?,?,?,?,?,?,?)";

            $query = $this->db->preparar($consulta);

            /** @var Empleado $e */
            @$query->bindParam(1, $e->getIdEmpleado());
            @$query->bindParam(2, $e->getIdUsuario());
            @$query->bindParam(3, $e->getNif());
            @$query->bindParam(4, $e->getNombre());
            @$query->bindParam(5, $e->getApellido1());
            @$query->bindParam(6, $e->getApellido2());
            @$query->bindParam(7, $e->getNumCta());
            @$query->bindParam(8, $e->getMovil());
            @$query->bindParam(9, $e->getRutaFoto());
            @$query->bindParam(10, $e->getActivo());
            $insertado = $query->execute();

        } catch (Exception $ex) {
            $insertado = false;
        }

        return $insertado;
    }

    public function getEmpleadosTodos($order = false, $completa = false)
    {

        $tablaEmpleado = array();
        try {
            $consulta = "SELECT id,id_empleado,id_usuario,nif,nombre,apellido1,apellido2,numcta,movil,ruta_foto,activo FROM empleados ";
            if ($order) {
                if ($completa)
                    $consulta = $consulta . "ORDER BY apellido1, apellido2, nombre ";
                else
                    $consulta = $consulta . "ORDER BY id_empleado";
            }

            $query = $this->db->preparar($consulta);
            $error = $query->execute();
            $tEmpleados = $query->fetchAll();

        } catch (Exception $ex) {
            $error = true;
            var_dump($ex);
        }

        foreach ($tEmpleados as $tEmpleado) {
            $e = new Empleado($tEmpleado['id'], $tEmpleado['id_empleado'], $tEmpleado['id_usuario'], $tEmpleado['nif'], $tEmpleado['nombre'], $tEmpleado['apellido1'], $tEmpleado['apellido2'], $tEmpleado['numcta'], $tEmpleado['movil'], $tEmpleado['ruta_foto'], $tEmpleado['activo']);
            array_push($tablaEmpleado, $e);
        }

        return $tablaEmpleado;
    }

    public function getEmpleadosActivos()
    {

        $tablaEmpleado = array();
        try {
            $consulta = "SELECT id,id_empleado,id_usuario,nif,nombre,apellido1,apellido2,numcta,movil,ruta_foto,activo FROM empleados WHERE activo = 1";

            $query = $this->db->preparar($consulta);
            $error = $query->execute();
            $tEmpleados = $query->fetchAll();

        } catch (Exception $ex) {
            $error = true;
            var_dump($ex);
        }

        foreach ($tEmpleados as $tEmpleado) {
            $e = new Empleado($tEmpleado['id'], $tEmpleado['id_empleado'], $tEmpleado['id_usuario'], $tEmpleado['nif'], $tEmpleado['nombre'], $tEmpleado['apellido1'], $tEmpleado['apellido2'], $tEmpleado['numcta'], $tEmpleado['movil'], $tEmpleado['ruta_foto'], $tEmpleado['activo']);
            array_push($tablaEmpleado, $e);
        }

        return $tablaEmpleado;
    }

    public function getByCodPostal($codPostal)
    {
        $tablaEmpleado = array();
        try {
            $consulta = "SELECT empleados.id,empleados.id_empleado,empleados.id_usuario,empleados.nif,empleados.nombre,empleados.apellido1,empleados.apellido2,empleados.numcta,empleados.movil,empleados.ruta_foto,empleados.activo FROM empleados INNER JOIN direcciones_empleados ON empleados.id_empleado = direcciones_empleados.id_empleado WHERE codpostal = $codPostal ORDER BY empleados.id_empleado";

            $query = $this->db->preparar($consulta);
            $error = $query->execute();
            $tEmpleados = $query->fetchAll();

        } catch (Exception $ex) {
            $error = true;
            var_dump($ex);
        }

        foreach ($tEmpleados as $tEmpleado) {
            $e = new Empleado($tEmpleado['id'], $tEmpleado['id_empleado'], $tEmpleado['id_usuario'], $tEmpleado['nif'], $tEmpleado['nombre'], $tEmpleado['apellido1'], $tEmpleado['apellido2'], $tEmpleado['numcta'], $tEmpleado['movil'], $tEmpleado['ruta_foto'], $tEmpleado['activo']);
            array_push($tablaEmpleado, $e);
        }

        return $tablaEmpleado;
        // o bien return $tProductos[0][0];

    }

    public function getUltimoEmpleado($codPostal)
    {

        try {
            $consulta = "SELECT id,id_empleado,id_usuario,nif,nombre,apellido1,apellido2,numcta,movil,ruta_foto,activo FROM empleados WHERE id_empleado = (SELECT MAX(empleados.id_empleado) as ultimo FROM empleados INNER JOIN direcciones_empleados ON empleados.id_empleado = direcciones_empleados.id_empleado WHERE codpostal = $codPostal)";

            $query = $this->db->preparar($consulta);
            $error = $query->execute();
            $tEmpleados = $query->fetchAll();

        } catch (Exception $ex) {
            $error = true;
            var_dump($ex);
        }

        return $tEmpleados[0];

    }

    public function getUltimoCodigo($codPostal)
    {

        try {
            $consulta = "SELECT MAX(direcciones_empleados.id_empleado) as ultimo FROM empleados INNER JOIN direcciones_empleados ON empleados.id_empleado = direcciones_empleados.id_empleado WHERE codpostal = $codPostal";

            $query = $this->db->preparar($consulta);

            $query->execute();
            $tProductos = $query->fetchAll();

        } catch (Exception $ex) {
            echo "Se ha producido un error en getUltimoProducto";
        }
        return $tProductos[0]['ultimo'] % 100;
        // o bien return $tProductos[0][0];

    }

    public function deleteEmpleado($idEmpleado)
    {
        $actualizado = false;
        try {
            $sql = "UPDATE empleados SET activo=0 WHERE id_empleado=?";

            $query = $this->db->preparar($sql);
            @$query->bindParam(1, $idEmpleado);
            $actualizado = $query->execute();
        } catch (Exception $ex) {
        }
        return $actualizado;
    }

    /**
     * @param $e Empleado
     * @return bool
     */
    public function actualizarEmpleado($e)
    {
        $actualizado = false;
        try {
            $consulta = "UPDATE empleados SET ";

            $consulta = (is_null($e->getNombre())) ? $consulta : $consulta . " nombre = ?, ";

            $consulta = (is_null($e->getApellido1())) ? $consulta : $consulta . " apellido1 = ?, ";

            $consulta = (is_null($e->getApellido2())) ? $consulta : $consulta . " apellido2 = ?, ";

            $consulta = (is_null($e->getNif())) ? $consulta : $consulta . " nif = ?, ";

            $consulta = (is_null($e->getNumCta())) ? $consulta : $consulta . " numcta = ?, ";

            $consulta = (is_null($e->getMovil())) ? $consulta : $consulta . " movil = ?, ";

            $consulta = (is_null($e->getRutaFoto())) ? $consulta : $consulta . " ruta_foto = ?, ";

            $consulta = (is_null($e->getActivo())) ? $consulta : $consulta . " activo = ?, ";

            //Para las comas
            $consulta = $consulta . " id_empleado = ? ";

            $consulta = $consulta . " WHERE id_empleado = ?";

            $query = $this->db->preparar($consulta);
            $contador = 1;

            if (!is_null($e->getNombre())) {
                @$query->bindParam($contador, $e->getNombre());
                $contador++;
            }

            if (!is_null($e->getApellido1())) {
                @$query->bindParam($contador, $e->getApellido1());
                $contador++;
            }
            if (!is_null($e->getApellido2())) {
                @$query->bindParam($contador, $e->getApellido2());
                $contador++;
            }
            if (!is_null($e->getNif())) {
                @$query->bindParam($contador, $e->getNif());
                $contador++;
            }
            if (!is_null($e->getNumcta())) {
                @$query->bindParam($contador, $e->getNumcta());
                $contador++;
            }
            if (!is_null($e->getMovil())) {
                @$query->bindParam($contador, $e->getMovil());
                $contador++;
            }
            if (!is_null($e->getRutaFoto())) {
                @$query->bindParam($contador, $e->getRutaFoto());
                $contador++;
            }
            if (!is_null($e->getActivo())) {
                @$query->bindParam($contador, $e->getActivo());
                $contador++;
            }
            @$query->bindParam($contador, $e->getIdEmpleado());
            @$query->bindParam($contador + 1, $e->getIdEmpleado());

            $actualizado = $query->execute();

        } catch (Exception $ex) {
        }
        return $actualizado;
    }

    /**
     * @param $idEmpleado
     * @return Empleado
     */
    public function getEmpleado($idEmpleado)
    {
        $empleado = null;
        try {
            $consulta = "SELECT * FROM empleados WHERE id_empleado = ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $idEmpleado);
            $query->execute();
            $tEmpleado = $query->fetchAll();

            //obtiene todas las tuplas de la tabla y devuelve el array $tClientes

            $empleado = new Empleado($tEmpleado[0]['id'], $tEmpleado[0]['id_empleado'], $tEmpleado[0]['id_usuario'], $tEmpleado[0]['nif'],
                $tEmpleado[0]['nombre'], $tEmpleado[0]['apellido1'], $tEmpleado[0]['apellido2'], $tEmpleado[0]['numcta'],
                $tEmpleado[0]['movil'], $tEmpleado[0]['ruta_foto'], $tEmpleado[0]['activo']);


        } catch (Exception $ex) {
            echo "Se ha producido un error en getEmpleado";
        }

        return $empleado;
    }

    public function getEmpleadoByNif($nif)
    {
        $empleado = null;
        try {
            $consulta = "SELECT * FROM empleados WHERE nif = ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $nif);
            $query->execute();
            $tEmpleado = $query->fetchAll();

            //obtiene todas las tuplas de la tabla y devuelve el array $tClientes
            if (empty($tEmpleado)) {
                $empleado = null;
            } else {
                $empleado = new Empleado($tEmpleado[0]['id'], $tEmpleado[0]['id_empleado'], $tEmpleado[0]['id_usuario'], $tEmpleado[0]['nif'],
                    $tEmpleado[0]['nombre'], $tEmpleado[0]['apellido1'], $tEmpleado[0]['apellido2'], $tEmpleado[0]['numcta'],
                    $tEmpleado[0]['movil'], $tEmpleado[0]['ruta_foto'], $tEmpleado[0]['activo']);
            }
        } catch (Exception $ex) {
            echo "Se ha producido un error en getEmpleadoByNif";
        }

        return $empleado;
    }

    public function getEmpleadosWithoutUser()
    {
        $tablaEmpleado = array();
        try {
            $consulta = "SELECT id,id_empleado,id_usuario,nif,nombre,apellido1,apellido2,numcta,movil,ruta_foto,activo FROM empleados WHERE id_usuario = \"0\" ";

            $query = $this->db->preparar($consulta);
            $error = $query->execute();
            $tEmpleados = $query->fetchAll();

        } catch (Exception $ex) {
            $error = true;
            var_dump($ex);
        }

        foreach ($tEmpleados as $tEmpleado) {
            $e = new Empleado($tEmpleado['id'], $tEmpleado['id_empleado'], $tEmpleado['id_usuario'], $tEmpleado['nif'], $tEmpleado['nombre'], $tEmpleado['apellido1'], $tEmpleado['apellido2'], $tEmpleado['numcta'], $tEmpleado['movil'], $tEmpleado['ruta_foto'], $tEmpleado['activo']);
            array_push($tablaEmpleado, $e);
        }

        return $tablaEmpleado;
    }

}
