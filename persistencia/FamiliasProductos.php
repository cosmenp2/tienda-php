<?php
require_once 'Conexion.php';

class FamiliasProductos
{
    private static $instancia;
    /** @var Conexion */
    private $db;

    private function __construct()
    {
        $this->db = Conexion::singleton_conexion();

    }

    public static function singletonFamiliasProductos()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;

        }

        return self::$instancia;
    }

    /**
     * @param FamiliaProducto $p
     * @return bool
     */
    public function altaFamiliaProducto(FamiliaProducto $p)
    {
        try {

            //Para insertar una copia del id de la tabla en id_producto, tendríamos
            //que averiguar cuál es el último introducido y sumarle 1
            //esto puede hacerse aquí o en el script principal
            //Optamos por el script principal
            //
            //$ultimo=$this->getUltimo()+1; //último código introducido +1
            $consulta = "INSERT INTO familias_productos " .
                "(id,id_familia,nombre,descripcion,"
                . "activo)  "
                . "values"
                . "(null,?,?,?,?)";

            $query = $this->db->preparar($consulta);

            @$query->bindParam(1, $p->getIdFamilia());
            @$query->bindParam(2, $p->getNombre());
            @$query->bindParam(3, $p->getDescripcion());
            @$query->bindParam(4, $p->getActivo());

            $query->execute();
            $insertado = true;

        } catch (Exception $ex) {
            $insertado = false;
        }

        return $insertado;
    }

    public function bajaFamiliaProducto($idFamiliaProducto)
    {
        try {
            $consulta = "UPDATE familias_productos SET activo=0 WHERE"
                . " id_familia="
                . $idFamiliaProducto . ";";

            $query = $this->db->preparar($consulta);

            $query->execute();
            $desactivado = true;

        } catch (Exception $ex) {
            $desactivado = false;
        }

        return $desactivado;
    }

    public function getFamiliaProductoTodos()
    {
        try {
            $tablaFamiliaProducto = array(); //
            $consulta = "SELECT * FROM familias_productos where activo=1";
            $query = $this->db->preparar($consulta);
            $query->execute();
            $tFamilias = $query->fetchAll();
            foreach ($tFamilias as $pr) {
                @$p = new FamiliaProducto($pr[0], $pr[1], $pr[2],
                    $pr[3], $pr[4]);
                array_push($tablaFamiliaProducto, $p); //Agregamos la familia ala tabla
            }
        } catch (Exception $ex) {
            echo "Se ha producido un error en getProductosTodos";
        }

        return $tablaFamiliaProducto;
    }

    public function generarID()
    {
        $total = 0;
        try {
            $consulta = "SELECT COUNT(*) FROM familias_productos";

            $query = $this->db->preparar($consulta);
            $query->execute();
            $tFamilias = $query->fetchAll();
        } catch (Exception $ex) {
            echo "Se ha producido un error en generarIDFamilia";
        }
        if (!empty($tFamilias)) {
            $total = $tFamilias[0][0];
        }

        if ($total == 0) {
            $newID = "0001";
        } elseif ($total < 9) {
            $newID = "000" . ($total + 1);
        } elseif ($total < 99) {
            $newID = "00" . ($total + 1);
        } elseif ($total < 999) {
            $newID = "0" . ($total + 1);
        } else {
            $newID = ($total + 1);
        }
        return $newID;
    }

    public function getFamiliaById($idFamilia)
    {
        try {
            $consulta = "SELECT * FROM familias_productos WHERE id_familia= ? AND activo=1";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $idFamilia);
            $query->execute();
            $tFamilias = $query->fetchAll();
        } catch (Exception $ex) {
            echo "Se ha producido un error en getUnCliente";
        }
        if (empty($tFamilias)) {
            //Si no existe ese numCliente
            //$c=new Cliente(); no funciona
            $f = null;
        } else {
            $f = new FamiliaProducto($tFamilias[0][0],
                $tFamilias[0][1],
                $tFamilias[0][2], $tFamilias[0][3],
                $tFamilias[0][4]);
        }
        return $f;
    }

    public function modificarFamilia(FamiliaProducto $f)
    {
        try {
            $consulta = "UPDATE familias_productos SET nombre=?, descripcion=? WHERE id_familia=?";

            $query = $this->db->preparar($consulta);
            /** @var FamiliaProducto $f */
            @$query->bindParam(1, $f->getNombre());
            @$query->bindParam(2, $f->getDescripcion());
            @$query->bindParam(3, $f->getIdFamilia());
            $insertado = $query->execute();

        } catch (Exception $ex) {
            $insertado = false;
        }
        return $insertado;
    }

    public function getByName($nombre)
    {
        $filtro = '%'.$nombre.'%';
        try {
            $tablaFamiliaProducto = array(); //
            $consulta = "SELECT * FROM familias_productos where nombre like ?";
            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $filtro);
            $query->execute();
            $tFamilias = $query->fetchAll();
            foreach ($tFamilias as $pr) {
                @$p = new FamiliaProducto($pr[0], $pr[1], $pr[2],
                    $pr[3], $pr[4]);
                array_push($tablaFamiliaProducto, $p); //Agregamos la familia ala tabla
            }
        } catch (Exception $ex) {
            echo "Se ha producido un error en getProductosTodos";
        }

        return $tablaFamiliaProducto;
    }
}
