<?php
require_once 'Conexion.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LineasPedidos
 *
 * @author guijarro
 */
class LineasPedidos {

	private static $instancia;
	/** @var Conexion */
	private $db;

	function __construct() {
		$this->db = Conexion::singleton_conexion();
	}

	public static function singletonLineasPedidos() {
		if (!isset(self::$instancia)) {
			$miclase = __CLASS__;
			self::$instancia = new $miclase;

		}

		return self::$instancia;
	}

	public function addLineasPedidos(LineaPedido $lp) {
		try
		{

			$consulta = "INSERT INTO lineas_pedidos (id,id_pedido,"
				. "id_producto,unidades,"
				. "descripcion,"
				. "pvp,tipo_iva,activo)  "
				. "values"
				. "(null,?,?,?,?,?,?,?)";

			$query = $this->db->preparar($consulta);
			@$query->bindParam(1, $lp->getIdPedido());
			@$query->bindParam(2, $lp->getIdProducto());
			@$query->bindParam(3, $lp->getUnidades());
			@$query->bindParam(4, $lp->getDescripcion());
			@$query->bindParam(5, $lp->getPvp());
			@$query->bindParam(6, $lp->getTipoIva());
			@$query->bindParam(7, $lp->getActivo());

			$query->execute();
			$insertado = true;

			//Para el proyecto final, contemplar la posibilidad de
			//que se produzca error en la
			// inserción y haya que recuperar el estado anterior
			// de la tabla
		} catch (Exception $ex) {
			$insertado = false;
		}

		return $insertado;
	}

	public function getLineasUnPedido($numPedido) {
		$lineas=array();
		try {
			$consulta = "SELECT * FROM lineas_pedidos WHERE id_pedido='"
				. $numPedido . "' and activo!=0";
			
			$query = $this->db->preparar($consulta);
			$query->execute();
			$tPedidos = $query->fetchAll();
		} catch (Exception $ex) {
			echo "Se ha producido un error en getLineasUnPedidos";
		}
		if (empty($tPedidos)) {
					
			$lineas = array();
		} else {
			foreach ($tPedidos as $t) {
				$lp = new LineaPedido($t[0], $t[1],$t[2], $t[3],$t[4], $t[5],$t[6],$t[7]);
				array_push($lineas, $lp);
			}
		}

		return $lineas;
	}

    public function getTotalUnPedido($IdPedido)
    {
        $total = 0;
        try {
            $consulta = "SELECT * FROM lineas_pedidos WHERE id_pedido=? and activo!=0";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $IdPedido);
            $query->execute();
            $tPedidos = $query->fetchAll();
        } catch (Exception $ex) {
            echo "Se ha producido un error en getLineasUnPedidos";
        }
        if (!empty($tPedidos)) {
            foreach ($tPedidos as $t) {
                $total += $t['pvp'] * $t['unidades'];
            }
        }
        return $total;
    }

}
