<?php
require_once 'Conexion.php';

class Usuarios
{
    private static $instancia;
    /** @var  Conexion db */
    private $db;

    function __construct()
    {

        $this->db = Conexion::singleton_conexion();
    }

    public static function singletonUsuarios()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;

        }

        return self::$instancia;
    }

    public function addUnUsuario($u)
    {
        //var_dump($c);
        try {
            $consulta = "INSERT INTO usuarios (id,id_usuario,id_rol,login,password,activo) "
                . "values " . "(null,?,?,?,?,?)";

            $query = $this->db->preparar($consulta);
            /** @var Usuario $u */
            @$query->bindParam(1, $u->getIdUsuario());
            @$query->bindParam(2, $u->getIdRol());
            @$query->bindParam(3, $u->getLogin());
            @$query->bindParam(4, $u->getPassword());
            @$query->bindParam(5, $u->getActivo());
            $insertado = $query->execute();

        } catch (Exception $ex) {
            $insertado = false;
        }

        return $insertado;
    }

    public function buscaUsuario($login)
    {
        //Conseguimos un cliente concreto que tenga en el id_cliente el
        //código que entra como parámetro en el método.
        try {
            $consulta = "SELECT * FROM usuarios WHERE login= ? and activo= 1";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $login);
            $query->execute();
            $tUsuarios = $query->fetchAll();
        } catch (Exception $ex) {
            echo "Se ha producido un error en buscaUsuario";
        }
        if (empty($tUsuarios)) {
            //Si no existe ese numCliente
            //$c=new Cliente(); no funciona
            $u = null;
        } else {
            $u = new Usuario($tUsuarios[0][0],
                $tUsuarios[0][1],
                $tUsuarios[0][2], $tUsuarios[0][3],
                $tUsuarios[0][4], $tUsuarios[0][5]);
        }
        return $u;
    }

    public function login($login, $password)
    {
        //Conseguimos un cliente concreto que tenga en el id_cliente el
        //código que entra como parámetro en el método.
        try {
            $consulta = "SELECT * FROM usuarios WHERE login= ? AND password= ? and activo = 1";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $login);
            @$query->bindParam(2, $password);
            $query->execute();
            $tUsuarios = $query->fetchAll();
        } catch (Exception $ex) {
            echo "Se ha producido un error en login";
        }
        if (empty($tUsuarios)) {
            //Si no existe ese numCliente
            //$c=new Cliente(); no funciona
            $u = null;
        } else {
            $u = new Usuario($tUsuarios[0][0],
                $tUsuarios[0][1],
                $tUsuarios[0][2], $tUsuarios[0][3],
                $tUsuarios[0][4], $tUsuarios[0][5]);
        }
        return $u;
    }

    public function changePassword($login, $newPass, $oldPass)
    {
        $tUsuarios = array();
        try {
            $consulta = "UPDATE usuarios SET password = ? WHERE login= ? AND password= ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $newPass);
            @$query->bindParam(2, $login);
            @$query->bindParam(3, $oldPass);
            $query->execute();
            $consulta = "SELECT password FROM usuarios WHERE login= ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $login);
            $query->execute();
            $tUsuarios = $query->fetchAll();

        } catch (Exception $ex) {
            $correcto = false;
        }

        if (!empty($tUsuarios)) {
            if ($tUsuarios[0][0] == $newPass) {
                //Si no existe ese numCliente
                //$c=new Cliente(); no funciona
                $correcto = true;
            } else {
                $correcto = false;
            }
        } else {
            $correcto = false;
        }
        return $correcto;
    }

    public function existAdmin()
    {
        $exist = true;
        try {
            $consulta = "SELECT * FROM usuarios WHERE id_rol = 1";

            $query = $this->db->preparar($consulta);
            $query->execute();
            $tUsuarios = $query->fetchAll();
        } catch (Exception $ex) {
            echo "Se ha producido un error en existAdmin";
        }
        if (empty($tUsuarios)) {
            $exist = false;
        }
        return $exist;
    }

    public function getUsuariosTodos()
    {
        $tablaUsuarios = array();
        try {
            $consulta = "SELECT id,id_usuario,id_rol,login,password,activo FROM usuarios ";

            $query = $this->db->preparar($consulta);
            $error = $query->execute();
            $tUsuarios = $query->fetchAll();

        } catch (Exception $ex) {
            $error = true;
        }
        foreach ($tUsuarios as $tUsuario) {
            $u = new Usuario($tUsuario['id'], $tUsuario['id_usuario'], $tUsuario['id_rol'], $tUsuario['login'],
                $tUsuario['password'], $tUsuario['activo']);
            array_push($tablaUsuarios, $u);
        }
        return $tablaUsuarios;
    }

    public function desactivarUsuario($idUsuario)
    {
        try {
            $consulta = "UPDATE usuarios SET activo = 0 WHERE id_usuario= ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $idUsuario);
            $correcto = $query->execute();

        } catch (Exception $ex) {
            $correcto = false;
        }
        return $correcto;
    }

    public function activarUsuario($idUsuario)
    {
        try {
            $consulta = "UPDATE usuarios SET activo = 1 WHERE id_usuario= ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $idUsuario);
            $correcto = $query->execute();

        } catch (Exception $ex) {
            $correcto = false;
        }
        return $correcto;
    }

    public function buscaUsuarioWithoutFilter($login)
    {
        try {
            $consulta = "SELECT * FROM usuarios WHERE login= ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $login);
            $query->execute();
            $tUsuarios = $query->fetchAll();
        } catch (Exception $ex) {
            echo "Se ha producido un error en buscaUsuario";
        }
        if (empty($tUsuarios)) {
            //Si no existe ese numCliente
            //$c=new Cliente(); no funciona
            $u = null;
        } else {
            $u = new Usuario($tUsuarios[0][0],
                $tUsuarios[0][1],
                $tUsuarios[0][2], $tUsuarios[0][3],
                $tUsuarios[0][4], $tUsuarios[0][5]);
        }
        return $u;
    }

    public function getUsuariosByRol($idRol)
    {
        $tablaUsuarios = array();
        try {
            $consulta = "SELECT id,id_usuario,id_rol,login,password,activo FROM usuarios WHERE id_rol = ? ";

            $query = $this->db->preparar($consulta);
            $query->bindParam(1, $idRol);
            $error = $query->execute();
            $tUsuarios = $query->fetchAll();

        } catch (Exception $ex) {
            $error = true;
        }
        foreach ($tUsuarios as $tUsuario) {
            $u = new Usuario($tUsuario['id'], $tUsuario['id_usuario'], $tUsuario['id_rol'], $tUsuario['login'],
                $tUsuario['password'], $tUsuario['activo']);
            array_push($tablaUsuarios, $u);
        }
        return $tablaUsuarios;
    }
}