<?php
require_once 'Conexion.php';

class Productos
{
    private static $instancia;
    /** @var Conexion */
    private $db;

    private function __construct()
    {
        $this->db = Conexion::singleton_conexion();

    }

    public static function singletonProductos()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;

        }

        return self::$instancia;
    }

    public function getProductosTodos()
    {
        try {
            $tablaObjetosProductos = array(); //
            $consulta = "SELECT * FROM productos where activo='1' ORDER BY id_familia";
            $query = $this->db->preparar($consulta);
            $query->execute();
            $tProductos = $query->fetchAll();
            foreach ($tProductos as $pr) {
                @$p = new Producto($pr[0], $pr[1], $pr[2],
                    $pr[3], $pr[4], $pr[5], $pr[6], $pr[7],
                    $pr[8], $pr[9], $pr[10], $pr[11], $pr[12],
                    $pr[13]);
                array_push($tablaObjetosProductos, $p); //Agregamos la familia ala tabla
            }
        } catch (Exception $ex) {
            echo "Se ha producido un error en getProductosTodos";
        }

        return $tablaObjetosProductos;
    }

    public function getUltimoProducto($familia)
    {
        //Devuelve un numero q es el id último producto introducido
        try {
            $consulta = "SELECT MAX(id_producto) as ultimo FROM productos WHERE id_familia = $familia";

            $query = $this->db->preparar($consulta);

            $query->execute();
            $tProductos = $query->fetchAll();

        } catch (Exception $ex) {
            echo "Se ha producido un error en getUltimoProducto";
        }
        echo "<br>Veo que lleva:<br>";
        var_dump($tProductos); //para ver qué devuelve;
        return $tProductos[0]['ultimo'] % 100000;
        // o bien return $tProductos[0][0];

    }

    public function altaProducto(Producto $p)
    {
        try {

            //Para insertar una copia del id de la tabla en id_producto, tendríamos
            //que averiguar cuál es el último introducido y sumarle 1
            //esto puede hacerse aquí o en el script principal
            //Optamos por el script principal
            //
            //$ultimo=$this->getUltimo()+1; //último código introducido +1
            $consulta = "INSERT INTO productos " .
                "(id,id_producto,id_familia,tipo_iva,precio_coste,"
                . "pvp,descripcion,codigo_barras,id_proveedor,"

                . "stock_actual,stock_minimo,stock_maximo,"
                . "ruta_foto,activo)  "
                . "values"
                . "(null,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            $query = $this->db->preparar($consulta);

            @$query->bindParam(1, $p->getIdProducto());
            @$query->bindParam(2, $p->getIdFamilia());
            @$query->bindParam(3, $p->getTipoIva());
            @$query->bindParam(4, $p->getPrecioCoste());

            @$query->bindParam(5, $p->getPvp());
            @$query->bindParam(6, $p->getDescripcion());
            @$query->bindParam(7, $p->getCodigoBarras());
            @$query->bindParam(8, $p->getIdProveedor());
            @$query->bindParam(9, $p->getStockActual());
            @$query->bindParam(10, $p->getStockMinimo());
            @$query->bindParam(11, $p->getStockMaximo());
            @$query->bindParam(12, $p->getRutaFoto());
            @$query->bindParam(13, $p->getActivo());

            $query->execute();
            $insertado = true;

        } catch (Exception $ex) {
            $insertado = false;
        }

        return $insertado;
    }

    public function bajaProducto($idProducto)
    {
        try {
            $consulta = "UPDATE productos SET activo=0 WHERE"
                . " id_producto="
                . $idProducto . ";";

            $query = $this->db->preparar($consulta);

            $query->execute();
            $desactivado = true;

        } catch (Exception $ex) {
            $desactivado = false;
        }

        return $desactivado;
    }

    public function totalProductos()
    {
        try {
            $total = array(); //
            $consulta = "SELECT COUNT(*) FROM productos where activo='1'";
            $query = $this->db->preparar($consulta);
            $query->execute();
            $tProductos = $query->fetchAll();
        } catch (Exception $ex) {
        }
        if (empty($tProductos)) {
            $total = 0;
        } else {
            $total = $tProductos[0][0];
        }

        return $total;
    }

    public function quitarStock($idProducto, $unidades)
    {
        try {
            $consulta = "UPDATE productos SET stock_actual=stock_actual - ? WHERE id_producto = ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $unidades);
            @$query->bindParam(2, $idProducto);

            $modificado = $query->execute();

        } catch (Exception $ex) {
            $modificado = false;
        }

        return $modificado;

    }

    public function getProductoById($idProducto)
    {
        try {
            $consulta = "SELECT * FROM productos WHERE id_producto= ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $idProducto);
            $query->execute();
            $tProductos = $query->fetchAll();
        } catch (Exception $ex) {
            echo "Se ha producido un error en getUnCliente";
        }
        if (empty($tProductos)) {
            //Si no existe ese numCliente
            //$c=new Cliente(); no funciona
            $p = null;
        } else {
            $p = new Producto($tProductos[0][0], $tProductos[0][1], $tProductos[0][2], $tProductos[0][3],
                $tProductos[0][4], $tProductos[0][5], $tProductos[0][6], $tProductos[0][7], $tProductos[0][8],
                $tProductos[0][9], $tProductos[0][10], $tProductos[0][11], $tProductos[0][12], $tProductos[0][13]);
        }
        return $p;
    }

    public function actualizarStock($idProducto, $stockActual)
    {
        try {
            $consulta = "UPDATE productos SET stock_actual=? WHERE id_producto = ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $stockActual);
            @$query->bindParam(2, $idProducto);

            $modificado = $query->execute();

        } catch (Exception $ex) {
            $modificado = false;
        }

        return $modificado;
    }

    public function actualizarProducto(Producto $p)
    {
        try {
            $consulta = "UPDATE productos SET id_familia=? , tipo_iva=?, precio_coste=?, pvp=?,descripcion=?,codigo_barras=?,
id_proveedor=?,stock_actual=?,stock_minimo=?,stock_maximo=?,ruta_foto=? WHERE id_producto=?";

            $query = $this->db->preparar($consulta);
            /** @var Cliente $c */
            @$query->bindParam(1, $p->getIdFamilia());
            @$query->bindParam(2, $p->getTipoIva());
            @$query->bindParam(3, $p->getPrecioCoste());
            @$query->bindParam(4, $p->getPvp());
            @$query->bindParam(5, $p->getDescripcion());
            @$query->bindParam(6, $p->getCodigoBarras());
            @$query->bindParam(7, $p->getIdProveedor());
            @$query->bindParam(8, $p->getStockActual());
            @$query->bindParam(9, $p->getStockMinimo());
            @$query->bindParam(10, $p->getStockMaximo());
            @$query->bindParam(11, $p->getRutaFoto());
            @$query->bindParam(12, $p->getIdProducto());
            $insertado = $query->execute();

        } catch (Exception $ex) {
            $insertado = false;
        }
        return $insertado;
    }

    public function searchProductosById($idProducto)
    {
        $filtro = "%".$idProducto."%";
        try {
            $tablaObjetosProductos = array(); //
            $consulta = "SELECT * FROM productos where id_producto like ? ORDER BY id_familia";
            $query = $this->db->preparar($consulta);
            $query->bindParam(1, $filtro);
            $query->execute();
            $tProductos = $query->fetchAll();
            foreach ($tProductos as $pr) {
                @$p = new Producto($pr[0], $pr[1], $pr[2],
                    $pr[3], $pr[4], $pr[5], $pr[6], $pr[7],
                    $pr[8], $pr[9], $pr[10], $pr[11], $pr[12],
                    $pr[13]);
                array_push($tablaObjetosProductos, $p); //Agregamos la familia ala tabla
            }
        } catch (Exception $ex) {
            echo "Se ha producido un error en searchProductosById";
        }

        return $tablaObjetosProductos;
    }

    public function getProductosByFamilia($familia)
    {
        try {
            $tablaObjetosProductos = array(); //
            $consulta = "SELECT * FROM productos where id_familia = ? ORDER BY id_familia";
            $query = $this->db->preparar($consulta);
            $query->bindParam(1, $familia);
            $query->execute();
            $tProductos = $query->fetchAll();
            foreach ($tProductos as $pr) {
                @$p = new Producto($pr[0], $pr[1], $pr[2],
                    $pr[3], $pr[4], $pr[5], $pr[6], $pr[7],
                    $pr[8], $pr[9], $pr[10], $pr[11], $pr[12],
                    $pr[13]);
                array_push($tablaObjetosProductos, $p); //Agregamos la familia ala tabla
            }
        } catch (Exception $ex) {
            echo "Se ha producido un error en searchProductosByFamilia";
        }

        return $tablaObjetosProductos;
    }

    public function searchProductosByCodBarras($codBarras)
    {
        $filtro = "%".$codBarras."%";
        try {
            $tablaObjetosProductos = array(); //
            $consulta = "SELECT * FROM productos where codigo_barras like ? ORDER BY id_familia";
            $query = $this->db->preparar($consulta);
            $query->bindParam(1, $filtro);
            $query->execute();
            $tProductos = $query->fetchAll();
            foreach ($tProductos as $pr) {
                @$p = new Producto($pr[0], $pr[1], $pr[2],
                    $pr[3], $pr[4], $pr[5], $pr[6], $pr[7],
                    $pr[8], $pr[9], $pr[10], $pr[11], $pr[12],
                    $pr[13]);
                array_push($tablaObjetosProductos, $p); //Agregamos la familia ala tabla
            }
        } catch (Exception $ex) {
            echo "Se ha producido un error en searchProductosById";
        }

        return $tablaObjetosProductos;
    }
}
