<?php
require_once 'Conexion.php';

class Clientes
{
    private static $instancia;
    /** @var  Conexion db */
    private $db;

    function __construct()
    {

        $this->db = Conexion::singleton_conexion();
    }

    public static function singletonClientes()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;

        }

        return self::$instancia;
    }

    public function addUnCliente($c)
    {
        //var_dump($c);
        try {
            $consulta = "INSERT INTO clientes (id,id_cliente,id_usuario,nombre,apellido1,apellido2,nif,varon,numcta,como_nos_conocio,activo) "
                . "values " . "(null,?,?,?,?,?,?,?,?,?,?)";

            $query = $this->db->preparar($consulta);
            /** @var Cliente $c */
            @$query->bindParam(1, $c->getIdCliente());
            @$query->bindParam(2, $c->getIdUsuario());
            @$query->bindParam(3, $c->getNombre());
            @$query->bindParam(4, $c->getApellido1());
            @$query->bindParam(5, $c->getApellido2());
            @$query->bindParam(6, $c->getNif());
            @$query->bindParam(7, $c->getVaron());
            @$query->bindParam(8, $c->getNumCta());
            @$query->bindParam(9, $c->getComoNosConocio());
            @$query->bindParam(10, $c->getActivo());
            $insertado = $query->execute();

        } catch (Exception $ex) {
            $insertado = false;
        }

        return $insertado;
    }

    public function getClientesTodos($order = null)
    {

        $tablaCliente = array();
        try {
            $consulta = "SELECT id,id_cliente,id_usuario,nombre,apellido1,apellido2,nif,varon,numcta,como_nos_conocio,activo FROM clientes WHERE activo= 1";

            if (!is_null($order)) {
                $consulta = $consulta . $this->order($order);
            }

            $query = $this->db->preparar($consulta);
            $error = $query->execute();
            $tClientes = $query->fetchAll();

        } catch (Exception $ex) {
            $error = true;
            var_dump($ex);
        }

        foreach ($tClientes as $tCliente) {
            $c = new Cliente($tCliente['id'], $tCliente['id_cliente'], $tCliente['id_usuario'], $tCliente['nombre'], $tCliente['apellido1'], $tCliente['apellido2'], $tCliente['nif'], $tCliente['varon'], $tCliente['numcta'], $tCliente['como_nos_conocio'], $tCliente['activo']);
            array_push($tablaCliente, $c);
        }

        return $tablaCliente;
    }

    /**
     * @param $c Cliente
     * @param null $order
     * @return array
     */
    public function buscarCliente($c, $order = null)
    {

        $tablaCliente = array();
        try {
            $consulta = "SELECT id,id_cliente,id_usuario,nombre,apellido1,apellido2,nif,varon,numcta,como_nos_conocio,activo FROM clientes WHERE 1 ";
            $consulta = (is_null($c->getNombre())) ? $consulta : $consulta . " AND nombre like \"%" . $c->getNombre() . "%\" ";

            $consulta = (is_null($c->getApellido1())) ? $consulta : $consulta . " AND apellido1 like \"%" . $c->getApellido1() . "%\" ";

            $consulta = (is_null($c->getApellido2())) ? $consulta : $consulta . " AND apellido2 like \"%" . $c->getApellido2() . "%\" ";

            $consulta = (is_null($c->getNif())) ? $consulta : $consulta . " AND nif like \"%" . $c->getNif() . "%\" ";

            $consulta = (is_null($c->getVaron())) ? $consulta : $consulta . " AND varon like \"%" . $c->getVaron() . "%\" ";

            $consulta = (is_null($c->getNumCta())) ? $consulta : $consulta . " AND numcta like \"%" . $c->getNumCta() . "%\" ";

            $consulta = (is_null($c->getComoNosConocio())) ? $consulta : $consulta . " AND como_nos_conocio like \"%" . $c->getComoNosConocio() . "%\" ";

            $consulta = (is_null($c->getActivo())) ? $consulta : $consulta . " AND activo like \"%" . $c->getActivo() . "%\" ";

            if (!is_null($order)) {
                $consulta = $consulta . $this->order($order);
            }

            $query = $this->db->preparar($consulta);
            $error = $query->execute();
            $tClientes = $query->fetchAll();

        } catch (Exception $ex) {
            $error = true;
            var_dump($ex);
        }

        foreach ($tClientes as $tCliente) {
            $c = new Cliente($tCliente['id'], $tCliente['id_cliente'], $tCliente['id_usuario'], $tCliente['nombre'],
                $tCliente['apellido1'], $tCliente['apellido2'], $tCliente['nif'], $tCliente['varon'],
                $tCliente['numcta'], $tCliente['como_nos_conocio'], $tCliente['activo']);
            array_push($tablaCliente, $c);
        }

        return $tablaCliente;
    }

    private function order($campos)
    {
        $order = "ORDER BY ";
        $first = true;
        foreach ($campos as $campo) {
            if ($first) {
                $order = $order . $campo . " ";
                $first = false;
            } else {
                $order = $order . ", " . $campo . " ";
            }
        }
        return $order;
    }

    public function getUnCliente($numCliente)
    {
        //Conseguimos un cliente concreto que tenga en el id_cliente el
        //código que entra como parámetro en el método.
        try {
            $consulta = "SELECT * FROM clientes WHERE id_cliente= ? and activo!=0";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $numCliente);
            $query->execute();
            $tClientes = $query->fetchAll();
        } catch (Exception $ex) {
            echo "Se ha producido un error en getUnCliente";
        }
        if (empty($tClientes)) {
            //Si no existe ese numCliente
            //$c=new Cliente(); no funciona
            $c = null;
        } else {
            $c = new Cliente($tClientes[0][0],
                $tClientes[0][1],
                $tClientes[0][2], $tClientes[0][3],
                $tClientes[0][4], $tClientes[0][5],
                $tClientes[0][6],
                $tClientes[0][7], $tClientes[0][8],
                $tClientes[0][9], $tClientes[0][10]);
        }
        return $c;
    }

    public function getUnClienteByIDUsuario($id)
    {
        //Conseguimos un cliente concreto que tenga en el id_cliente el
        //código que entra como parámetro en el método.
        try {
            $consulta = "SELECT * FROM clientes WHERE id_usuario= ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $id);
            $query->execute();
            $tClientes = $query->fetchAll();
        } catch (Exception $ex) {
            echo "Se ha producido un error en getUnCliente";
        }
        if (empty($tClientes)) {
            //Si no existe ese numCliente
            //$c=new Cliente(); no funciona
            $c = null;
        } else {
            $c = new Cliente($tClientes[0][0],
                $tClientes[0][1],
                $tClientes[0][2], $tClientes[0][3],
                $tClientes[0][4], $tClientes[0][5],
                $tClientes[0][6],
                $tClientes[0][7], $tClientes[0][8],
                $tClientes[0][9], $tClientes[0][10]);
        }
        return $c;
    }

    public function getUnClienteByNif($nif)
    {
        //Conseguimos un cliente concreto que tenga en el id_cliente el
        //código que entra como parámetro en el método.
        try {
            $consulta = "SELECT * FROM clientes WHERE nif= ? AND activo=1";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $nif);
            $query->execute();
            $tClientes = $query->fetchAll();
        } catch (Exception $ex) {
            echo "Se ha producido un error en getUnCliente";
        }
        if (empty($tClientes)) {
            //Si no existe ese numCliente
            //$c=new Cliente(); no funciona
            $c = null;
        } else {
            $c = new Cliente($tClientes[0][0],
                $tClientes[0][1],
                $tClientes[0][2], $tClientes[0][3],
                $tClientes[0][4], $tClientes[0][5],
                $tClientes[0][6],
                $tClientes[0][7], $tClientes[0][8],
                $tClientes[0][9], $tClientes[0][10]);
        }
        return $c;
    }

    public function getTotalComprasByID($id)
    {
        try {
            $consulta = "SELECT COUNT(*) FROM pedidos WHERE id_cliente= ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $id);
            $query->execute();
            $tClientes = $query->fetchAll();
        } catch (Exception $ex) {
            echo "Se ha producido un error en getUnCliente";
        }
        if (empty($tClientes)) {
            //Si no existe ese numCliente
            //$c=new Cliente(); no funciona
            $total = 0;
        } else {
            $total = $tClientes[0][0];
        }
        return $total;
    }

    public function generarID()
    {
        $fecha = date("Ym");
        $total = 0;
        $newID = $fecha;
        try {
            $parametro = $fecha . '%';
            $consulta = "SELECT COUNT(*) FROM clientes WHERE id_cliente like ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $parametro);
            $query->execute();
            $tClientes = $query->fetchAll();
        } catch (Exception $ex) {
            echo "Se ha producido un error en getUnCliente";
        }
        if (!empty($tClientes)) {
            $total = $tClientes[0][0];
        }

        if ($total == 0) {
            $newID .= "01";
        } elseif ($total < 9) {
            $newID .= "0" . ($total + 1);
        } else {
            $newID .= ($total + 1);
        }
        return $newID;
    }

    public function modificarCliente(Cliente $c)
    {
        try {
            $consulta = "UPDATE clientes SET id_usuario=? , nombre=?, apellido1=?, apellido2=?,nif=?,varon=?,
numcta=?,como_nos_conocio=?,activo=? WHERE id_cliente=?";

            $query = $this->db->preparar($consulta);
            /** @var Cliente $c */
            @$query->bindParam(1, $c->getIdUsuario());
            @$query->bindParam(2, $c->getNombre());
            @$query->bindParam(3, $c->getApellido1());
            @$query->bindParam(4, $c->getApellido2());
            @$query->bindParam(5, $c->getNif());
            @$query->bindParam(6, $c->getVaron());
            @$query->bindParam(7, $c->getNumCta());
            @$query->bindParam(8, $c->getComoNosConocio());
            @$query->bindParam(9, $c->getActivo());
            @$query->bindParam(10, $c->getIdCliente());
            $insertado = $query->execute();

        } catch (Exception $ex) {
            $insertado = false;
        }
        return $insertado;
    }

    public function getClientesByProductos($idProducto)
    {
        $tablaClientes = array();
        try {
            $consulta = "SELECT * FROM clientes INNER JOIN pedidos ON clientes.id_cliente = pedidos.id_cliente
    INNER JOIN lineas_pedidos ON pedidos.id_pedido = lineas_pedidos.id_pedido 
WHERE id_producto = ? GROUP BY clientes.id_cliente;";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $idProducto);
            $query->execute();
            $tClientes = $query->fetchAll();

            //obtiene todas las tuplas de la tabla y devuelve el array $tClientes
            foreach ($tClientes as $tCliente) {
                $c = new Cliente($tCliente['id'], $tCliente['id_cliente'], $tCliente['id_usuario'], $tCliente['nombre'],
                    $tCliente['apellido1'], $tCliente['apellido2'], $tCliente['nif'], $tCliente['varon'],
                    $tCliente['numcta'], $tCliente['como_nos_conocio'], $tCliente['activo']);
                array_push($tablaClientes, $c);
            }

        } catch (Exception $ex) {
            echo "Se ha producido un error en getPedidosTodos";
        }

        return $tablaClientes;
    }

    public function getClientesWithoutUser()
    {
        $tablaClientes = array();
        try {
            $consulta = "SELECT * FROM clientes WHERE id_usuario = \"0\" AND activo = 1 ;";

            $query = $this->db->preparar($consulta);
            $query->execute();
            $tClientes = $query->fetchAll();

            //obtiene todas las tuplas de la tabla y devuelve el array $tClientes
            foreach ($tClientes as $tCliente) {
                $c = new Cliente($tCliente['id'], $tCliente['id_cliente'], $tCliente['id_usuario'], $tCliente['nombre'],
                    $tCliente['apellido1'], $tCliente['apellido2'], $tCliente['nif'], $tCliente['varon'],
                    $tCliente['numcta'], $tCliente['como_nos_conocio'], $tCliente['activo']);
                array_push($tablaClientes, $c);
            }

        } catch (Exception $ex) {
            echo "Se ha producido un error en getPedidosTodos";
        }

        return $tablaClientes;
    }

}