<?php
require_once 'Conexion.php';

class Deseos
{
    private static $instancia;
    /** @var Conexion */
    private $db;

    private function __construct()
    {
        $this->db = Conexion::singleton_conexion();

    }

    public static function singletonDeseos()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;

        }

        return self::$instancia;
    }

    public function deseosByIdCliente($idCliente)
    {
        try {
            $tablaDeseos = array();
            $consulta = "SELECT * FROM deseos WHERE activo='1' AND id_cliente = ?";
            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $idCliente);
            $query->execute();
            $tDeseos = $query->fetchAll();
            if (!empty($tDeseos)) {
                foreach ($tDeseos as $de) {
                    $d = new Deseo($de[0], $de[1], $de[2], $de[3], $de[4]);
                    array_push($tablaDeseos, $d); //Agregamos la familia ala tabla
                }
            }
        } catch (Exception $ex) {
        }
        return $tablaDeseos;
    }

    public function altaDeseo(Deseo $d)
    {
        try {

            //Para insertar una copia del id de la tabla en id_producto, tendríamos
            //que averiguar cuál es el último introducido y sumarle 1
            //esto puede hacerse aquí o en el script principal
            //Optamos por el script principal
            //
            //$ultimo=$this->getUltimo()+1; //último código introducido +1
            $consulta = "INSERT INTO deseos " .
                "(id,id_cliente,id_producto,fecha,activo)  "
                . "values"
                . "(null,?,?,?,?)";

            $query = $this->db->preparar($consulta);

            @$query->bindParam(1, $d->getIdCliente());
            @$query->bindParam(2, $d->getIdProducto());
            @$query->bindParam(3, $d->getFecha());
            @$query->bindParam(4, $d->getActivo());

            $query->execute();
            $insertado = true;

        } catch (Exception $ex) {
            $insertado = false;
        }
        return $insertado;
    }

    public function bajaDeseo($idCliente,$idProducto)
    {
        try {
            $consulta = "UPDATE deseos SET activo=0 WHERE"
                . " id_cliente=? AND id_producto=?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $idCliente);
            @$query->bindParam(2, $idProducto);

            $query->execute();
            $desactivado = true;

        } catch (Exception $ex) {
            $desactivado = false;
        }

        return $desactivado;
    }
}
