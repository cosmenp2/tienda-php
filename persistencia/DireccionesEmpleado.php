<?php
require_once 'Conexion.php';

class DireccionesEmpleado
{
    private static $instancia;
    /** @var Conexion */
    private $db;

    function __construct()
    {
        $this->db = Conexion::singleton_conexion();
    }

    public static function singletonDireccionesEmpleado()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;

        }

        return self::$instancia;
    }

    public function addUnaDireccion(DireccionEmpleado $de)
    {
        //var_dump($c);
        try {
            $consulta = "INSERT INTO direcciones_empleados (id,id_empleado,direccion,codpostal,localidad,provincia,pais,activo) "
                . "values " . "(null,?,?,?,?,?,?,?)";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $de->getIdEmpleado());
            @$query->bindParam(2, $de->getDireccion());
            @$query->bindParam(3, $de->getCodPostal());
            @$query->bindParam(4, $de->getLocalidad());
            @$query->bindParam(5, $de->getProvincia());
            @$query->bindParam(6, $de->getPais());
            @$query->bindParam(7, $de->getActivo());

            $insertado = $query->execute();

        } catch (Exception $ex) {
            $insertado = false;
        }

        return $insertado;
    }

    public function getDireccionesEmpleados($codPostal)
    {

        $tablaDireccionEmpleado = array();
        try {
            $consulta = "SELECT id,id_empleado,direccion,localidad,codpostal,provincia,activo,pais FROM direcciones_empleados WHERE codpostal = ? ";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $codPostal);
            $error = $query->execute();
            $tDireccionesEmpleado = $query->fetchAll();

        } catch (Exception $ex) {
            $error = true;
            var_dump($ex);
        }

        foreach ($tDireccionesEmpleado as $tDireccionEmpleado) {
            $d = new DireccionEmpleado($tDireccionEmpleado['id'], $tDireccionEmpleado['id_empleado'], $tDireccionEmpleado['direccion'], $tDireccionEmpleado['localidad'], $tDireccionEmpleado['codpostal'], $tDireccionEmpleado['provincia'], $tDireccionEmpleado['activo'], $tDireccionEmpleado['pais']);
            array_push($tablaDireccionEmpleado, $d);
        }
        return $tablaDireccionEmpleado;
    }


    public function desactivarDireccion($idEliminar)
    {
        try {
            $consulta = "UPDATE direcciones_empleados SET activo = 0 WHERE id=?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $idEliminar);

            $eliminado = $query->execute();

        } catch (Exception $ex) {
            $eliminado = false;
        }

        return $eliminado;
    }

    public function getDireccionesByEmpleado($idEmpleado)
    {
        $tablaDireccionEmpleado = array();
        try {
            $consulta = "SELECT id,id_empleado,direccion,localidad,codpostal,provincia,activo,pais FROM direcciones_empleados WHERE id_empleado = ? AND activo = 1";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $idEmpleado);
            $error = $query->execute();
            $tDireccionesEmpleados = $query->fetchAll();

        } catch (Exception $ex) {
            $error = true;
        }
        if (!empty($tDireccionesEmpleados)) {
            foreach ($tDireccionesEmpleados as $tDireccionEmpleado) {
                $d = new DireccionEmpleado($tDireccionEmpleado['id'], $tDireccionEmpleado['id_empleado'], $tDireccionEmpleado['direccion'], $tDireccionEmpleado['localidad'], $tDireccionEmpleado['codpostal'], $tDireccionEmpleado['provincia'], $tDireccionEmpleado['activo'], $tDireccionEmpleado['pais']);
                array_push($tablaDireccionEmpleado, $d);
            }
        }
        return $tablaDireccionEmpleado;
    }

    public function getDireccionByID($id)
    {
        try {
            $consulta = "SELECT id,id_empleado,direccion,localidad,codpostal,provincia,activo,pais FROM direcciones_empleados WHERE id = ? ";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $id);
            $error = $query->execute();
            $tDireccionesEmpleados = $query->fetchAll();
            $d = new DireccionEmpleado($tDireccionesEmpleados[0]['id'], $tDireccionesEmpleados[0]['id_empleado'],
                $tDireccionesEmpleados[0]['direccion'], $tDireccionesEmpleados[0]['localidad'], $tDireccionesEmpleados[0]['codpostal'],
                $tDireccionesEmpleados[0]['provincia'], $tDireccionesEmpleados[0]['activo'], $tDireccionesEmpleados[0]['pais']);

        } catch (Exception $ex) {
            $error = true;
            $d = null;
        }
        return $d;
    }

    public function editUnaDireccion(DireccionEmpleado $dc)
    {
        try {
            $consulta = "UPDATE direcciones_empleados SET id_empleado=?,direccion=?,codpostal=?,localidad=?,provincia=?,pais=? WHERE id = ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $dc->getIdEmpleado());
            @$query->bindParam(2, $dc->getDireccion());
            @$query->bindParam(3, $dc->getCodPostal());
            @$query->bindParam(4, $dc->getLocalidad());
            @$query->bindParam(5, $dc->getProvincia());
            @$query->bindParam(6, $dc->getPais());
            @$query->bindParam(7, $dc->getId());

            $modificado = $query->execute();

        } catch (Exception $ex) {
            $modificado = false;
        }

        return $modificado;
    }


}