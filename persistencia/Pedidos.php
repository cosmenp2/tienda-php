<?php
require_once 'Conexion.php';

class Pedidos
{
    private static $instancia;
    /** @var Conexion */
    private $db;

    function __construct()
    {
        $this->db = Conexion::singleton_conexion();
    }

    public static function singletonPedidos()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;

        }

        return self::$instancia;
    }

    /**
     * @param Pedido $p
     * @return bool
     */
    public function addPedido(Pedido $p)
    {
        try {

            $consulta = "INSERT INTO pedidos (id,id_pedido,"
                . "id_empleado_empaqueta,id_empresa_transporte,"
                . "fecha_pedido,fecha_envio,fecha_entrega,facturado,"
                . "id_factura,fecha_factura,pagado,fecha_pago,metodo_pago,id_cliente,activo,direccion) "
                . "values "
                . "(null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $p->getIdPedido());
            @$query->bindParam(2, $p->getIdEmpleadoQueEmpaqueta());
            @$query->bindParam(3, $p->getIdEmpresaTransporte());
            @$query->bindParam(4, $p->getFechaPedido());
            @$query->bindParam(5, $p->getFechaEnvio());
            @$query->bindParam(6, $p->getFechaEntrega());
            @$query->bindParam(7, $p->getFacturado());
            @$query->bindParam(8, $p->getIdFactura());
            @$query->bindParam(9, $p->getFechaFactura());
            @$query->bindParam(10, $p->getPagado());
            @$query->bindParam(11, $p->getFechaPago());
            @$query->bindParam(12, $p->getMetodoPago());
            @$query->bindParam(13, $p->getIdCliente());
            @$query->bindParam(14, $p->getActivo());
            @$query->bindParam(15, $p->getDireccion());
            $query->execute();
            $insertado = true;

            //Para el proyecto final, contemplar la posibilidad de
            //que se produzca error en la
            // inserción y haya que recuperar el estado anterior
            // de la tabla
        } catch (Exception $ex) {
            $insertado = false;
        }

        return $insertado;
    }

    //// Ahora toca hacer las funciones de consulta

    public function getPedidosTodos()
    {
        $tablaPedidos = array();
        try {
            $consulta = "SELECT * FROM pedidos";

            $query = $this->db->preparar($consulta);
            $query->execute();
            $tPedidos = $query->fetchAll();
            ////obtiene todas las tuplas de la
            //tabla y devuelve el array $tClientes
            foreach ($tPedidos as $tPedido) {
                $p = new Pedido($tPedido['id'], $tPedido['id_pedido'], $tPedido['id_empleado_empaqueta'], $tPedido['id_empresa_transporte'],
                    $tPedido['fecha_pedido'], $tPedido['fecha_envio'], $tPedido['fecha_entrega'], $tPedido['facturado'],
                    $tPedido['id_factura'], $tPedido['fecha_factura'], $tPedido['pagado'], $tPedido['fecha_pago'],
                    $tPedido['metodo_pago'], $tPedido['id_cliente'], $tPedido['activo'], $tPedido['direccion']);
                array_push($tablaPedidos, $p);
            }
        } catch (Exception $ex) {
            echo "Se ha producido un error en getPedidosTodos";
        }

        return $tablaPedidos;
    }

    public function getUnPedido($numPedido)
    {

        try {
            $consulta = "SELECT * FROM pedidos WHERE id_pedido='"
                . $numPedido . "' and activo!=0";

            $query = $this->db->preparar($consulta);
            $query->execute();
            $tPedidos = $query->fetchAll();
        } catch (Exception $ex) {
            echo "Se ha producido un error en getUnCliente";
        }
        if (empty($tPedidos)) {
            //Si no existe ese numPedido
            //$c=new Pedido(); no funciona
            $p = null;
        } else {
            $p = new Pedido($tPedidos[0][0],
                $tPedidos[0][1],
                $tPedidos[0][2], $tPedidos[0][3],
                $tPedidos[0][4], $tPedidos[0][5],
                $tPedidos[0][6],
                $tPedidos[0][7], $tPedidos[0][8],
                $tPedidos[0][9], $tPedidos[0][10], $tPedidos[0][11],
                $tPedidos[0][12], $tPedidos[0][13], $tPedidos[0][14], $tPedidos[0][15]);
        }
        return $p;
    }

    public function contarPedidos($anio)
    {
        //cuenta los pedidos que hay de un año concreto
        try {
            $consulta = "SELECT COUNT(*) as numero FROM `pedidos` WHERE id_pedido like '$anio%'";

            $query = $this->db->preparar($consulta);
            $query->execute();
            $tPedidos = $query->fetchAll();
        } catch (Exception $ex) {
            echo "Se ha producido un error en contarPedidos";
            return -1;
        }
        if (empty($tPedidos)) {
            $numero = 0;

        } else {
            $numero = $tPedidos[0][0];
        }

        return $numero;

    }

    /**
     * @param $idEmpleado
     * @return array<Pedido>
     */
    public function getPedidosEmpaquetados($idEmpleado)
    {
        $tablaPedidos = array();
        try {
            $consulta = "SELECT * FROM pedidos WHERE id_empleado_empaqueta = ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $idEmpleado);
            $query->execute();
            $tPedidos = $query->fetchAll();

            //obtiene todas las tuplas de la tabla y devuelve el array $tClientes
            foreach ($tPedidos as $tPedido) {
                $p = new Pedido($tPedido['id'], $tPedido['id_pedido'], $tPedido['id_empleado_empaqueta'], $tPedido['id_empresa_transporte'],
                    $tPedido['fecha_pedido'], $tPedido['fecha_envio'], $tPedido['fecha_entrega'], $tPedido['facturado'],
                    $tPedido['id_factura'], $tPedido['fecha_factura'], $tPedido['pagado'], $tPedido['fecha_pago'],
                    $tPedido['metodo_pago'], $tPedido['id_cliente'], $tPedido['activo'], $tPedido['direccion']);
                array_push($tablaPedidos, $p);
            }

        } catch (Exception $ex) {
            echo "Se ha producido un error en getPedidosTodos";
        }

        return $tablaPedidos;
    }

    public function getPedidosByDate($fechaInicio, $fechaFinal)
    {
        $tablaPedidos = array();
        try {
            $consulta = "SELECT * FROM pedidos ";
            $condicion = "";

            if (empty($fechaInicio)){
                $condicion .= " WHERE fecha_pedido <= ?";
                $consulta .= $condicion;
                $query = $this->db->preparar($consulta);
                @$query->bindParam(1, $fechaFinal);
            } elseif (empty($fechaFinal)){
                $condicion .= " WHERE fecha_pedido >= ?";
                $consulta .= $condicion;
                $query = $this->db->preparar($consulta);
                @$query->bindParam(1, $fechaInicio);
            } else {
                $condicion .= " WHERE fecha_pedido BETWEEN ? AND ? ";
                $consulta .= $condicion;
                $query = $this->db->preparar($consulta);
                @$query->bindParam(1, $fechaInicio);
                @$query->bindParam(2, $fechaFinal);
            }

            $query->execute();
            $tPedidos = $query->fetchAll();
            ////obtiene todas las tuplas de la
            //tabla y devuelve el array $tClientes
            foreach ($tPedidos as $tPedido) {
                $p = new Pedido($tPedido['id'], $tPedido['id_pedido'], $tPedido['id_empleado_empaqueta'], $tPedido['id_empresa_transporte'],
                    $tPedido['fecha_pedido'], $tPedido['fecha_envio'], $tPedido['fecha_entrega'], $tPedido['facturado'],
                    $tPedido['id_factura'], $tPedido['fecha_factura'], $tPedido['pagado'], $tPedido['fecha_pago'],
                    $tPedido['metodo_pago'], $tPedido['id_cliente'], $tPedido['activo'], $tPedido['direccion']);
                array_push($tablaPedidos, $p);
            }

        } catch (Exception $ex) {
            echo "Se ha producido un error en getPedidosByFecha";
        }

        return $tablaPedidos;
    }

    public function getPedidosAEmpaquetar(){
        $tablaPedidos = array();
        try {
            $consulta = "SELECT * FROM pedidos WHERE id_empleado_empaqueta = 0";

            $query = $this->db->preparar($consulta);
            $query->execute();
            $tPedidos = $query->fetchAll();
            ////obtiene todas las tuplas de la
            //tabla y devuelve el array $tClientes
            foreach ($tPedidos as $tPedido) {
                $p = new Pedido($tPedido['id'], $tPedido['id_pedido'], $tPedido['id_empleado_empaqueta'], $tPedido['id_empresa_transporte'],
                    $tPedido['fecha_pedido'], $tPedido['fecha_envio'], $tPedido['fecha_entrega'], $tPedido['facturado'],
                    $tPedido['id_factura'], $tPedido['fecha_factura'], $tPedido['pagado'], $tPedido['fecha_pago'],
                    $tPedido['metodo_pago'], $tPedido['id_cliente'], $tPedido['activo'], $tPedido['direccion']);
                array_push($tablaPedidos, $p);
            }
        } catch (Exception $ex) {
            echo "Se ha producido un error en getPedidosTodos";
        }

        return $tablaPedidos;
    }

    public function getPedidosAEnviar(){
        $tablaPedidos = array();
        try {
            $consulta = "SELECT * FROM pedidos WHERE id_empleado_empaqueta != 0 AND fecha_envio IS NULL";

            $query = $this->db->preparar($consulta);
            $query->execute();
            $tPedidos = $query->fetchAll();
            ////obtiene todas las tuplas de la
            //tabla y devuelve el array $tClientes
            foreach ($tPedidos as $tPedido) {
                $p = new Pedido($tPedido['id'], $tPedido['id_pedido'], $tPedido['id_empleado_empaqueta'], $tPedido['id_empresa_transporte'],
                    $tPedido['fecha_pedido'], $tPedido['fecha_envio'], $tPedido['fecha_entrega'], $tPedido['facturado'],
                    $tPedido['id_factura'], $tPedido['fecha_factura'], $tPedido['pagado'], $tPedido['fecha_pago'],
                    $tPedido['metodo_pago'], $tPedido['id_cliente'], $tPedido['activo'], $tPedido['direccion']);
                array_push($tablaPedidos, $p);
            }
        } catch (Exception $ex) {
            echo "Se ha producido un error en getPedidosTodos";
        }

        return $tablaPedidos;
    }

    public function getPedidosByCliente($idCliente)
    {
        $tablaPedidos = array();
        try {
            $consulta = "SELECT * FROM pedidos WHERE id_cliente = ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $idCliente);
            $query->execute();
            $tPedidos = $query->fetchAll();

            //obtiene todas las tuplas de la tabla y devuelve el array $tClientes
            foreach ($tPedidos as $tPedido) {
                $p = new Pedido($tPedido['id'], $tPedido['id_pedido'], $tPedido['id_empleado_empaqueta'], $tPedido['id_empresa_transporte'],
                    $tPedido['fecha_pedido'], $tPedido['fecha_envio'], $tPedido['fecha_entrega'], $tPedido['facturado'],
                    $tPedido['id_factura'], $tPedido['fecha_factura'], $tPedido['pagado'], $tPedido['fecha_pago'],
                    $tPedido['metodo_pago'], $tPedido['id_cliente'], $tPedido['activo'], $tPedido['direccion']);
                array_push($tablaPedidos, $p);
            }

        } catch (Exception $ex) {
            echo "Se ha producido un error en getPedidosTodos";
        }

        return $tablaPedidos;
    }

    public function getPedidosByFamilia($idFamilia)
    {
        $tablaPedidos = array();
        try {
            $consulta = "SELECT * FROM pedidos INNER JOIN lineas_pedidos ON pedidos.id_pedido = lineas_pedidos.id_pedido 
WHERE id_producto in (SELECT id_producto FROM productos WHERE id_familia=?) GROUP BY pedidos.id_pedido;";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $idFamilia);
            $query->execute();
            $tPedidos = $query->fetchAll();

            //obtiene todas las tuplas de la tabla y devuelve el array $tClientes
            foreach ($tPedidos as $tPedido) {
                $p = new Pedido($tPedido['id'], $tPedido['id_pedido'], $tPedido['id_empleado_empaqueta'], $tPedido['id_empresa_transporte'],
                    $tPedido['fecha_pedido'], $tPedido['fecha_envio'], $tPedido['fecha_entrega'], $tPedido['facturado'],
                    $tPedido['id_factura'], $tPedido['fecha_factura'], $tPedido['pagado'], $tPedido['fecha_pago'],
                    $tPedido['metodo_pago'], $tPedido['id_cliente'], $tPedido['activo'], $tPedido['direccion']);
                array_push($tablaPedidos, $p);
            }

        } catch (Exception $ex) {
            echo "Se ha producido un error en getPedidosTodos";
        }

        return $tablaPedidos;
    }

    public function getPedidosByProductos($idProducto)
    {
        $tablaPedidos = array();
        try {
            $consulta = "SELECT * FROM pedidos INNER JOIN lineas_pedidos ON pedidos.id_pedido = lineas_pedidos.id_pedido 
WHERE id_producto = ? GROUP BY pedidos.id_pedido;";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $idProducto);
            $query->execute();
            $tPedidos = $query->fetchAll();

            //obtiene todas las tuplas de la tabla y devuelve el array $tClientes
            foreach ($tPedidos as $tPedido) {
                $p = new Pedido($tPedido['id'], $tPedido['id_pedido'], $tPedido['id_empleado_empaqueta'], $tPedido['id_empresa_transporte'],
                    $tPedido['fecha_pedido'], $tPedido['fecha_envio'], $tPedido['fecha_entrega'], $tPedido['facturado'],
                    $tPedido['id_factura'], $tPedido['fecha_factura'], $tPedido['pagado'], $tPedido['fecha_pago'],
                    $tPedido['metodo_pago'], $tPedido['id_cliente'], $tPedido['activo'], $tPedido['direccion']);
                array_push($tablaPedidos, $p);
            }

        } catch (Exception $ex) {
            echo "Se ha producido un error en getPedidosTodos";
        }

        return $tablaPedidos;
    }

    public function getPedidosByTransport($idTransport)
    {
        $tablaPedidos = array();
        try {
            $consulta = "SELECT * FROM pedidos WHERE id_empresa_transporte = ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $idTransport);
            $query->execute();
            $tPedidos = $query->fetchAll();

            //obtiene todas las tuplas de la tabla y devuelve el array $tClientes
            foreach ($tPedidos as $tPedido) {
                $p = new Pedido($tPedido['id'], $tPedido['id_pedido'], $tPedido['id_empleado_empaqueta'], $tPedido['id_empresa_transporte'],
                    $tPedido['fecha_pedido'], $tPedido['fecha_envio'], $tPedido['fecha_entrega'], $tPedido['facturado'],
                    $tPedido['id_factura'], $tPedido['fecha_factura'], $tPedido['pagado'], $tPedido['fecha_pago'],
                    $tPedido['metodo_pago'], $tPedido['id_cliente'], $tPedido['activo'], $tPedido['direccion']);
                array_push($tablaPedidos, $p);
            }

        } catch (Exception $ex) {
            echo "Se ha producido un error en getPedidosTranspor";
        }

        return $tablaPedidos;
    }

    public function facturarPedido($idPedido)
    {
        $modificado = false;
        $fechaFactura = date("Y-m-d");
        $anio = substr($fechaFactura, 0, 4); //saco el año a partir de la fecha
        $numeroFactura = $this->contarFacturas($anio);
        if ($numeroFactura != -1) {
            $nuevaFactura = $numeroFactura + 1; //sumo 1

            $codigoString = (string)$nuevaFactura;
            $numCaracteres = strlen($nuevaFactura);
            $resta = 6 - $numCaracteres;
            for ($i = 1; $i <= $resta; $i++) {
                $codigoString = '0' . $codigoString;
            }
            $idFactura = $anio . '-' . $codigoString;

            try {
                $consulta = "UPDATE pedidos SET id_factura = ?,facturado=1,fecha_factura=? WHERE id_pedido = ?";

                $query = $this->db->preparar($consulta);
                $query->bindParam(1, $idFactura);
                $query->bindParam(2, $fechaFactura);
                $query->bindParam(3, $idPedido);
                $query->execute();
                $modificado = true;

                //Para el proyecto final, contemplar la posibilidad de
                //que se produzca error en la
                // inserción y haya que recuperar el estado anterior
                // de la tabla
            } catch (Exception $ex) {
                $modificado = false;
            }
        }
        return $modificado;
    }

    public function contarFacturas($anio)
    {
        //cuenta los pedidos que hay de un año concreto
        try {
            $consulta = "SELECT COUNT(*) as numero FROM `pedidos` WHERE id_factura like '$anio%'";

            $query = $this->db->preparar($consulta);
            $query->execute();
            $tPedidos = $query->fetchAll();
        } catch (Exception $ex) {
            echo "Se ha producido un error en contarPedidos";
            return -1;
        }
        if (empty($tPedidos)) {
            $numero = 0;

        } else {
            $numero = $tPedidos[0][0];
        }

        return $numero;

    }

}
