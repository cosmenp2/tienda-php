<?php
require_once 'Conexion.php';

class DireccionesCliente
{
    private static $instancia;
    /** @var Conexion */
    private $db;

    function __construct()
    {
        $this->db = Conexion::singleton_conexion();
    }

    public static function singletonDireccionesCliente()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;

        }

        return self::$instancia;
    }

    /**
     * @param $dc DireccionCliente
     * @return bool
     */
    public function addUnaDireccion($dc)
    {
        //var_dump($c);
        try {
            $consulta = "INSERT INTO direcciones_clientes (id,id_cliente,direccion,codpostal,localidad,provincia,pais,activo) "
                . "values " . "(null,?,?,?,?,?,?,?)";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $dc->getIdCliente());
            @$query->bindParam(2, $dc->getDireccion());
            @$query->bindParam(3, $dc->getCodPostal());
            @$query->bindParam(4, $dc->getLocalidad());
            @$query->bindParam(5, $dc->getProvincia());
            @$query->bindParam(6, $dc->getPais());
            @$query->bindParam(7, $dc->getActivo());

            $insertado = $query->execute();

        } catch (Exception $ex) {
            $insertado = false;
        }

        return $insertado;
    }

    public function getDireccionesClientes($codPostal)
    {

        $tablaDireccionCliente = array();
        try {
            $consulta = "SELECT id,id_cliente,direccion,localidad,codpostal,provincia,activo,pais FROM direcciones_clientes WHERE codpostal = ? ";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $codPostal);
            $error = $query->execute();
            $tDireccionesClientes = $query->fetchAll();

        } catch (Exception $ex) {
            $error = true;
        }

        foreach ($tDireccionesClientes as $tDireccionCliente) {
            $d = new DireccionCliente($tDireccionCliente['id'], $tDireccionCliente['id_cliente'], $tDireccionCliente['direccion'], $tDireccionCliente['localidad'], $tDireccionCliente['codpostal'], $tDireccionCliente['provincia'], $tDireccionCliente['activo'], $tDireccionCliente['pais']);
            array_push($tablaDireccionCliente, $d);
        }
        return $tablaDireccionCliente;
    }

    /**
     * @param $idCliente
     * @return DireccionCliente
     */
    public function getDireccionCliente($idCliente)
    {
        try {
            $consulta = "SELECT id,id_cliente,direccion,localidad,codpostal,provincia,activo,pais FROM direcciones_clientes WHERE id_cliente = ? ";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $idCliente);
            $error = $query->execute();
            $tDireccionesClientes = $query->fetchAll();
            $d = new DireccionCliente($tDireccionesClientes[0]['id'], $tDireccionesClientes[0]['id_cliente'],
                $tDireccionesClientes[0]['direccion'], $tDireccionesClientes[0]['localidad'], $tDireccionesClientes[0]['codpostal'],
                $tDireccionesClientes[0]['provincia'], $tDireccionesClientes[0]['activo'], $tDireccionesClientes[0]['pais']);

        } catch (Exception $ex) {
            $error = true;
            $d = null;
        }
        return $d;
    }

    public function getDireccionesCliente($idCliente)
    {
        $tablaDireccionCliente = array();
        try {
            $consulta = "SELECT id,id_cliente,direccion,localidad,codpostal,provincia,activo,pais FROM direcciones_clientes WHERE id_cliente = ? AND activo = 1";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $idCliente);
            $error = $query->execute();
            $tDireccionesClientes = $query->fetchAll();

        } catch (Exception $ex) {
            $error = true;
        }
        if (!empty($tDireccionesClientes)) {
            foreach ($tDireccionesClientes as $tDireccionCliente) {
                $d = new DireccionCliente($tDireccionCliente['id'], $tDireccionCliente['id_cliente'], $tDireccionCliente['direccion'], $tDireccionCliente['localidad'], $tDireccionCliente['codpostal'], $tDireccionCliente['provincia'], $tDireccionCliente['activo'], $tDireccionCliente['pais']);
                array_push($tablaDireccionCliente, $d);
            }
        }
        return $tablaDireccionCliente;
    }

    public function desactivarDireccion($idEliminar)
    {
        try {
            $consulta = "UPDATE direcciones_clientes SET activo = 0 WHERE id=?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $idEliminar);

            $eliminado = $query->execute();

        } catch (Exception $ex) {
            $eliminado = false;
        }

        return $eliminado;
    }

    public function editUnaDireccion(DireccionCliente $dc)
    {
        try {
            $consulta = "UPDATE direcciones_clientes SET id_cliente=?,direccion=?,codpostal=?,localidad=?,provincia=?,pais=? WHERE id = ?";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $dc->getIdCliente());
            @$query->bindParam(2, $dc->getDireccion());
            @$query->bindParam(3, $dc->getCodPostal());
            @$query->bindParam(4, $dc->getLocalidad());
            @$query->bindParam(5, $dc->getProvincia());
            @$query->bindParam(6, $dc->getPais());
            @$query->bindParam(7, $dc->getId());

            $modificado = $query->execute();

        } catch (Exception $ex) {
            $modificado = false;
        }

        return $modificado;
    }

    public function getDireccionByID($idDireccion)
    {
        try {
            $consulta = "SELECT id,id_cliente,direccion,localidad,codpostal,provincia,activo,pais FROM direcciones_clientes WHERE id = ? ";

            $query = $this->db->preparar($consulta);
            @$query->bindParam(1, $idDireccion);
            $error = $query->execute();
            $tDireccionesClientes = $query->fetchAll();
            $d = new DireccionCliente($tDireccionesClientes[0]['id'], $tDireccionesClientes[0]['id_cliente'],
                $tDireccionesClientes[0]['direccion'], $tDireccionesClientes[0]['localidad'], $tDireccionesClientes[0]['codpostal'],
                $tDireccionesClientes[0]['provincia'], $tDireccionesClientes[0]['activo'], $tDireccionesClientes[0]['pais']);

        } catch (Exception $ex) {
            $error = true;
            $d = null;
        }
        return $d;
    }

}