-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-01-2020 a las 18:06:39
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_sge_nieto_perez`
--
CREATE DATABASE IF NOT EXISTS `bd_sge_nieto_perez` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bd_sge_nieto_perez`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `id_cliente` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `id_usuario` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido1` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido2` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `nif` varchar(9) COLLATE utf8_spanish2_ci NOT NULL,
  `varon` tinyint(1) NOT NULL,
  `numcta` varchar(24) COLLATE utf8_spanish2_ci NOT NULL,
  `como_nos_conocio` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `id_cliente`, `id_usuario`, `nombre`, `apellido1`, `apellido2`, `nif`, `varon`, `numcta`, `como_nos_conocio`, `activo`) VALUES
(1, '001', '0', 'Maria', 'Rodrigez', 'Santos', '12213123', 0, 'kj', 'Por enternet', 1),
(2, '060010003', '0', 'kjlk', 'jlk', 'kj', 'lkj', 0, 'jj', 'Por enternet', 1),
(13, '20200102', 'cosmenp', 'Cosme José', 'Nieto', 'Pérez', '80100374E', 1, '', 'Soy el que hizo la pagina', 1),
(15, '20200101', 'pepe', 'pepe', 'p', 'p', '123987123', 1, '123421341', '', 1),
(16, '20200103', 'prueba', 'prueba', 'primer', 'primer', '987987987', 0, '123421341', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deseos`
--

CREATE TABLE `deseos` (
  `id` int(11) NOT NULL,
  `id_cliente` varchar(20) DEFAULT NULL,
  `id_producto` varchar(9) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `activo` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `deseos`
--

INSERT INTO `deseos` (`id`, `id_cliente`, `id_producto`, `fecha`, `activo`) VALUES
(1, '20200102', '000300001', '2019-12-21', 0),
(2, '20200102', '000200001', '2019-12-21', 1),
(3, '20200102', '000300001', '2019-12-21', 0),
(4, '20200102', '000300001', '2020-01-10', 1),
(5, '20200101', '000200004', '2020-01-11', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direcciones_clientes`
--

CREATE TABLE `direcciones_clientes` (
  `id` int(11) NOT NULL,
  `id_cliente` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `direccion` varchar(40) COLLATE utf8_spanish2_ci NOT NULL,
  `codpostal` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `localidad` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `provincia` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `pais` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `direcciones_clientes`
--

INSERT INTO `direcciones_clientes` (`id`, `id_cliente`, `direccion`, `codpostal`, `localidad`, `provincia`, `pais`, `activo`) VALUES
(1, '001', 'lk', 'jl', 'jlk', 'jlk', 'jlk', 0),
(2, '060010003', 'lk', 'jl', 'jlk', 'jlk', 'jlk', 1),
(3, '060010003', 'kjh', 'kjh', 'khk', 'jh', 'kjhasd', 0),
(14, '0', 'lkj', '06007', 'lkj', 'j', 'jlkj', 1),
(15, '20200102', 'Enrique Sanchez de León portal 14 1ºD', '06009', 'Badajoz', 'Badajoz', 'España', 0),
(16, '20200102', 'Enrique Sanchez de León portal 14 1ºD', '06009', 'Badajoz', 'Badajoz', 'España', 1),
(18, '20200101', 'p', '06007', 'p', 'p', 'p', 1),
(19, '060010003', 'Pepe', '06004', 'kjh', 'kjh', 'asdf', 1),
(20, '001', 'JUAN', '09008', 'klj', 'Casere', 'asdf', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direcciones_empleados`
--

CREATE TABLE `direcciones_empleados` (
  `id` int(11) NOT NULL,
  `id_empleado` varchar(11) COLLATE utf8_spanish2_ci NOT NULL,
  `direccion` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `codpostal` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `localidad` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `provincia` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `pais` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `direcciones_empleados`
--

INSERT INTO `direcciones_empleados` (`id`, `id_empleado`, `direccion`, `codpostal`, `localidad`, `provincia`, `pais`, `activo`) VALUES
(5, '06007001', 'j', '06007', 'jk', 'jlk', 'ljl', 1),
(6, '06007002', 'j', '06007', 'jk', 'jlk', 'ljl', 1),
(7, '06009001', 'lk', '06009', 'lkjlk', 'jk', 'lkj', 1),
(8, '06007001', 'Enrique', '06008', 'Badajoz', 'Badajoz', 'España', 1),
(9, '08007001', 'Los Pesa', '08007', 'lkjlk', 'lkjlkj', 'lkjas', 1),
(10, '08007001', 'lkj', '12345', 'lkj', 'lkj', 'lkj', 0),
(11, '08007001', 'lkjlkj', '98979', 'lkjlkjlj', 'lkjlkj', 'kljlkj', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `id` int(11) NOT NULL,
  `id_empleado` varchar(11) COLLATE utf8_spanish2_ci NOT NULL,
  `id_usuario` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `nif` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido1` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido2` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `numcta` varchar(24) COLLATE utf8_spanish2_ci NOT NULL,
  `movil` varchar(12) COLLATE utf8_spanish2_ci NOT NULL,
  `ruta_foto` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`id`, `id_empleado`, `id_usuario`, `nif`, `nombre`, `apellido1`, `apellido2`, `numcta`, `movil`, `ruta_foto`, `activo`) VALUES
(15, '06007001', '0', 'kj', 'Pepito', 'Perez', 'Lopez', 'lkj', 'lkjjkj', 'empleados/emp_06007001.jpg', 1),
(16, '06007002', '0', 'jkl', 'klj', 'lkj', 'lklkj', 'lkj', 'lkjjkj', 'empleados/emp_06007002.jpg', 1),
(17, '06009001', '0', 'lkj', 'jlkj', 'lkjk', 'j', 'j', 'lkj', 'empleados/emp_06009001.jpg', 0),
(18, '08007001', '0', '8787234', 'Rodolfo', 'Suares', 'Suares', '098098', '2342344', 'empleados/emp_08007001.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `familias_productos`
--

CREATE TABLE `familias_productos` (
  `id` int(11) NOT NULL,
  `id_familia` char(4) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `familias_productos`
--

INSERT INTO `familias_productos` (`id`, `id_familia`, `nombre`, `descripcion`, `activo`) VALUES
(1, '0001', 'Congelados', 'congelados', 1),
(2, '0002', 'Verduras', '', 1),
(3, '0003', 'Panaderia', '', 1),
(4, '0004', 'Muebles', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lineas_pedidos`
--

CREATE TABLE `lineas_pedidos` (
  `id` int(11) NOT NULL,
  `id_pedido` varchar(11) COLLATE utf8_spanish2_ci NOT NULL,
  `id_producto` varchar(11) COLLATE utf8_spanish2_ci NOT NULL,
  `unidades` int(11) NOT NULL,
  `descripcion` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `pvp` double NOT NULL,
  `tipo_iva` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `lineas_pedidos`
--

INSERT INTO `lineas_pedidos` (`id`, `id_pedido`, `id_producto`, `unidades`, `descripcion`, `pvp`, `tipo_iva`, `activo`) VALUES
(14, '2019-000001', '000200001', 1, 'oiu', 78, 7, 1),
(16, '2019-000002', '000200001', 1, 'oiu', 78, 7, 1),
(17, '2019-000003', '000200001', 3, 'oiu', 78, 7, 1),
(72, '2019-000004', '000200002', 1, 'oiu', 78, 7, 1),
(73, '2019-000005', '000100001', 1, 'sfdg', 23, 2, 1),
(74, '2019-000005', '000100002', 1, 'qwe', 11, 1, 1),
(75, '2019-000006', '000300001', 1, 'qwe', 11, 1, 1),
(76, '2020-000001', '000200004', 2, 'sfdg', 23, 2, 1),
(77, '2020-000002', '000200003', 1, 'sfdg', 23, 2, 1),
(78, '2020-000003', '000200003', 1, 'sfdg', 23, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `id` int(11) NOT NULL,
  `id_pedido` varchar(11) COLLATE utf8_spanish2_ci NOT NULL,
  `id_empleado_empaqueta` int(11) NOT NULL,
  `id_empresa_transporte` int(11) NOT NULL,
  `fecha_pedido` date NOT NULL,
  `fecha_envio` date NOT NULL,
  `fecha_entrega` date NOT NULL,
  `facturado` tinyint(1) NOT NULL,
  `id_factura` varchar(12) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_factura` date NOT NULL,
  `pagado` tinyint(1) NOT NULL,
  `fecha_pago` date NOT NULL,
  `metodo_pago` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `id_cliente` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `direccion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`id`, `id_pedido`, `id_empleado_empaqueta`, `id_empresa_transporte`, `fecha_pedido`, `fecha_envio`, `fecha_entrega`, `facturado`, `id_factura`, `fecha_factura`, `pagado`, `fecha_pago`, `metodo_pago`, `id_cliente`, `activo`, `direccion`) VALUES
(28, '2019-000001', 6007001, 0, '2019-12-21', '0000-00-00', '0000-00-00', 0, '0', '0000-00-00', 1, '2019-12-21', 'numCta', '20200102', 1, 16),
(29, '2019-000002', 0, 0, '2019-12-22', '0000-00-00', '0000-00-00', 0, '0', '0000-00-00', 0, '0000-00-00', 'contrareembolso', '20200102', 1, 16),
(30, '2019-000003', 0, 0, '2019-12-22', '0000-00-00', '0000-00-00', 0, '0', '0000-00-00', 0, '0000-00-00', 'contrareembolso', '20200102', 1, 16),
(31, '2019-000004', 6007001, 0, '2019-12-22', '0000-00-00', '0000-00-00', 1, '2020-000003', '2020-01-21', 1, '2019-12-22', 'numCta', '20200102', 1, 16),
(32, '2019-000005', 0, 0, '2019-12-22', '0000-00-00', '0000-00-00', 0, '0', '0000-00-00', 1, '2019-12-22', 'paypal', '20200102', 1, 16),
(33, '2019-000006', 0, 0, '2019-12-25', '0000-00-00', '0000-00-00', 0, '0', '0000-00-00', 0, '0000-00-00', 'contrareembolso', '20200102', 1, 16),
(34, '2020-000001', 6007001, 0, '2020-01-10', '0000-00-00', '0000-00-00', 1, '2020-000002', '2020-01-21', 0, '0000-00-00', 'contrareembolso', '20200102', 1, 16),
(35, '2020-000002', 0, 0, '2020-01-11', '0000-00-00', '0000-00-00', 0, '0', '0000-00-00', 0, '0000-00-00', 'contrareembolso', '20200101', 1, 18),
(36, '2020-000003', 0, 0, '2020-01-18', '0000-00-00', '0000-00-00', 1, '2020-000001', '2020-01-21', 1, '2020-01-18', 'numCta', '20200102', 1, 16);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `id_producto` varchar(9) COLLATE utf8_spanish2_ci NOT NULL,
  `id_familia` char(4) COLLATE utf8_spanish2_ci NOT NULL,
  `tipo_iva` decimal(10,2) NOT NULL,
  `precio_coste` decimal(10,2) NOT NULL,
  `pvp` decimal(10,2) NOT NULL,
  `descripcion` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `codigo_barras` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `stock_actual` int(11) NOT NULL,
  `stock_minimo` int(11) NOT NULL,
  `stock_maximo` int(11) NOT NULL,
  `ruta_foto` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `id_producto`, `id_familia`, `tipo_iva`, `precio_coste`, `pvp`, `descripcion`, `codigo_barras`, `id_proveedor`, `stock_actual`, `stock_minimo`, `stock_maximo`, `ruta_foto`, `activo`) VALUES
(15, '000100001', '0001', '7.00', '78.00', '78.00', 'oiu', '123139128', 3, 39, 5, 18, 'productos/pro_000100001.jpg', 1),
(16, '000200001', '0002', '7.00', '78.00', '78.00', 'oiu', '123139128', 3, 38, 5, 18, 'productos/pro_000200001.jpg', 1),
(17, '000200002', '0002', '7.00', '78.00', '78.00', 'oiu', '123139128', 3, 40, 5, 18, 'productos/pro_000200002.jpg', 1),
(18, '000300001', '0003', '1.00', '11.00', '11.00', 'qwe', '123139128', 3, 18, 5, 18, 'productos/pro_000300001.jpg', 1),
(19, '000100002', '0001', '1.00', '11.00', '11.00', 'qwe', '123139128', 3, 39, 5, 18, 'productos/pro_000100002.jpg', 1),
(20, '000100003', '0001', '1.00', '11.00', '11.00', 'qwe', '123139128', 3, 40, 5, 18, 'productos/pro_000100003.jpg', 1),
(21, '000200003', '0002', '2.00', '23.00', '23.00', 'sfdg', '123139128', 3, 36, 5, 18, 'productos/pro_000200003.jpg', 1),
(22, '000200004', '0002', '2.00', '23.00', '23.00', 'sfdg', '123139128', 3, 35, 5, 18, 'productos/pro_000200004.jpg', 1),
(23, '000400001', '0004', '23.00', '30.50', '50.95', 'Cuadro del juego The Witcher', '060049932784', 0, 35, 5, 50, 'productos/pro_000400001.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre`) VALUES
(1, 'Administrador'),
(2, 'Empleado'),
(3, 'Cliente'),
(5, 'transporte');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `id_usuario` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `id_rol` int(11) NOT NULL,
  `login` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `id_usuario`, `id_rol`, `login`, `password`, `activo`) VALUES
(3, 'cosmenp', 3, 'cosmenp', '4dc2ce6493d0bc9fb06961c9e3e4e16de02e81d618444176a34d71be6576f6d726b70098a851c02137bc768d32ebb64271a2b5d7cdd461e6ddfbd3b82ec0c902', 1),
(5, 'admin', 1, 'admin', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec', 1),
(6, 'pepe', 3, 'pepe', '0dd3e512642c97ca3f747f9a76e374fbda73f9292823c0313be9d78add7cdd8f72235af0c553dd26797e78e1854edee0ae002f8aba074b066dfce1af114e32f8', 1),
(7, 'prueba', 3, 'prueba', 'ae3b5f6d1ef7d3b013d368271001b963b91f4a1ebd33168294ceef97f2bf67abf6c3fa35a613aa83f208ff00671fa2d72e1531db9a07ab1a5155265c4289347a', 1),
(8, 'admin2', 1, 'admin2', '661bb43140229ad4dc3e762e7bdd68cc14bb9093c158c386bd989fea807acd9bd7f805ca4736b870b6571594d0d8fcfc57b98431143dfb770e083fa9be89bc72', 1),
(9, 'rodolfo', 2, 'rodolfo', '8bbf158a2f64ed50aa7a082c85764e56bfa9c13c8bb2eb0d8de398ef999ce26d02c374b34c93d64a42637f5ac939ca7b6403f0153b276c17512d45addcd57c3d', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `deseos`
--
ALTER TABLE `deseos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `direcciones_clientes`
--
ALTER TABLE `direcciones_clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `direcciones_empleados`
--
ALTER TABLE `direcciones_empleados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `familias_productos`
--
ALTER TABLE `familias_productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lineas_pedidos`
--
ALTER TABLE `lineas_pedidos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `deseos`
--
ALTER TABLE `deseos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `direcciones_clientes`
--
ALTER TABLE `direcciones_clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `direcciones_empleados`
--
ALTER TABLE `direcciones_empleados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `familias_productos`
--
ALTER TABLE `familias_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `lineas_pedidos`
--
ALTER TABLE `lineas_pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
