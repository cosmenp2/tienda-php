<?php
require_once('./rutas.php');
require_once PERSISTENCIA . "Productos.php";
require_once PERSISTENCIA . "Pedidos.php";
require_once PERSISTENCIA . "Usuarios.php";
require_once PERSISTENCIA . "LineasPedidos.php";
require_once PERSISTENCIA . "FamiliasProductos.php";
require_once PERSISTENCIA . "DireccionesEmpleado.php";
require_once PERSISTENCIA . "DireccionesCliente.php";
require_once PERSISTENCIA . "Clientes.php";
require_once PERSISTENCIA . "Deseos.php";
require_once PERSISTENCIA . "Empleados.php";
require_once POJOS . "Usuario.php";
require_once POJOS . "Producto.php";
require_once POJOS . "Pedido.php";
require_once POJOS . "LineaPedido.php";
require_once POJOS . "FamiliaProducto.php";
require_once POJOS . "DireccionEmpleado.php";
require_once POJOS . "DireccionCliente.php";
require_once POJOS . "Cliente.php";
require_once POJOS . "Deseo.php";
require_once POJOS . "Empleado.php";
$isFirstTime = false;

function initSesion()
{
    session_start();
    $cerrada = false;
    // No permitir usar un ID de sesión demasiado antiguo
    if (!empty($_SESSION['deleted_time']) && $_SESSION['deleted_time'] < time() - 7200) {
        setcookie("idSesion", "", time() - 3600);
        session_destroy();
        $cerrada = true;
//        session_start();
    }
    if (empty($_SESSION['deleted_time'])) {
        setcookie("idSesion", "", time() - 3600);
        session_destroy();
        $cerrada = true;
//        session_start();
    }
    return $cerrada;
}

function regenerateSesion()
{
    // Llamar a session_create_id() mientras la sesión está activa para
    // asegurarse de que no haya colisiones.
//    if (session_status() != PHP_SESSION_ACTIVE) {
//        session_start();
//    }
    // ADVERTENCIA: ¡No utilizar nunca cadenas confidenciales para el prefijo!

    do {
        $valida = true;
        $idSesion = session_create_id('castelar');
        setcookie('nom', 'pepe');
        // Establecer la marca de tiempo de borrado. Los datos de la sesión no deben eliminarse inmediatamente.
        // Finalizar la sesión
//    session_commit();
        // Asegurarse de aceptar ID de sesiones definidas por el usuario
        // NOTA: Se debe habilitar 'use_strict_mode' para operaciones normales.
        ini_set('session.use_strict_mode', 1);
        // Establecer el nuevo ID de sesión personalizado
        session_id($idSesion);
        // Empezar la sesión con el ID de sesión personalizado
        session_start();
        if (sizeof($_SESSION) > 0) {
            session_commit();
            $valida = false;
        }
    } while (!$valida);

    $_SESSION['deleted_time'] = time();
    setcookie('idSesion', session_id());
}

ini_set('session.use_strict_mode', 1);
if (isset($_COOKIE['idSesion'])) {
    session_id($_COOKIE['idSesion']);
    if (initSesion()) {
        regenerateSesion();
    } else {
        $_SESSION['deleted_time'] = time();
    }
} else {
    regenerateSesion();
}

/** @var Productos $tProductos */
$tProductos = Productos::singletonProductos();
/** @var Usuarios $tUsuario */
$tUsuario = Usuarios::singletonUsuarios();
$usuario = null;
$nombre = "Invitado";
if (isset($_SESSION['idCliente'])) {
    $usuario = $tUsuario->buscaUsuario($_SESSION['idCliente']);
    if (is_null($usuario)) {
        $_SESSION['idCliente'] = "";
    } else {
        $_SESSION['time'] = time();
        $nombre = $usuario->getLogin();
    }
} elseif(isset($_SESSION['idAdmin'])) {
    unset($_SESSION['idAdmin']);
} else {
    if (!$tUsuario->existAdmin()) {
        $isFirstTime = true;
    }
}

$totalProductos = $tProductos->totalProductos();
//Voy a controlar si se ha pinchado un cierre de sesión en algún sitio
//Iconos de la web https://icon-icons.com/
?>
<!doctype html>
<html lang="es">
<head> <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Cosme José Nieto Pérez">


    <!-- Bootstrap CSS en la web-->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


    <link rel="stylesheet" href="estilos.css">

    <!--  <link href="css/bootstrap.min.css" rel="stylesheet">
  -->
    <title> ERP IES Castelar</title>
</head>


<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" <?php echo 'href=' . LOGO ?>> <img <?php echo 'src=' . LOGO ?> width="30" height="30"
                                                                                           alt="">
    </a>

    <a class="navbar-brand pb-2" <?php echo 'href="IndexInicial.php?principal=' . INFORMATIVAS . 'inicio.html"' ?>>IES
        Castlelar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">


            <li class="nav-item">

                <a class="nav-link" <?php echo 'href="IndexInicial.php?principal=' . INFORMATIVAS . 'inicio.html"' ?>>Inicio</a>
            </li>
            <li class="nav-item">
                <!--                    TODO: Habrá que implementar esto en posteriores retos-->
                <a class="nav-link" <?php echo 'href="IndexInicial.php?principal=' . INFORMATIVAS . 'inicio.html"' ?>>Ofertas</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false"> Productos </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <li><a class="dropdown-item"
                            <?php echo 'href="IndexInicial.php?principal=' . VISTA_USUARIO . 'catalogo_productos_carrito.php"' ?>>Ver
                            todos
                            <span class="badge"><?php echo $totalProductos ?></span></a></a></li>
                    <!--TODO: Las familias para mas adelante-->
                    <!--                        <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle"-->
                    <!--                                                        href="IndexInicial.php?principal=informativas/inicio.html">Categorías</a>-->
                    <!--                            <ul class="dropdown-menu">-->
                    <!--                                <li><a class="dropdown-item" href="IndexInicial.php?principal=informativas/inicio.html">Fruta</a>-->
                    <!--                                </li>-->
                    <!--                                <li><a class="dropdown-item" href="IndexInicial.php?principal=informativas/inicio.html">Pescados</a>-->
                    <!--                                </li>-->
                    <!--                                <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle"-->
                    <!--                                                                href="IndexInicial.php?principal=informativas/inicio.html">Carnes</a>-->
                    <!--                                    <ul class="dropdown-menu">-->
                    <!--                                        <li><a class="dropdown-item"-->
                    <!--                                               href="IndexInicial.php?principal=informativas/inicio.html">Ternera</a>-->
                    <!--                                        </li>-->
                    <!--                                        <li><a class="dropdown-item"-->
                    <!--                                               href="IndexInicial.php?principal=informativas/inicio.html">Cerdo</a></li>-->
                    <!--                                        <li><a class="dropdown-item"-->
                    <!--                                               href="IndexInicial.php?principal=informativas/inicio.html">Cordero</a>-->
                    <!--                                        </li>-->
                    <!--                                    </ul>-->
                    <!--                                </li>-->
                    <!--                                <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle"-->
                    <!--                                                                href="#">Postres</a>-->
                    <!--                                    <ul class="dropdown-menu">-->
                    <!--                                        <li><a class="dropdown-item"-->
                    <!--                                               href="IndexInicial.php?principal=informativas/inicio.html">Helados</a>-->
                    <!--                                        </li>-->
                    <!--                                        <li><a class="dropdown-item"-->
                    <!--                                               href="IndexInicial.php?principal=informativas/inicio.html">Tartas</a>-->
                    <!--                                        </li>-->
                    <!--                                    </ul>-->
                    <!--                                </li>-->
                    <!--                            </ul>-->
                    <!--                        </li>-->
                </ul>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false"> Sobre nosotros </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <li class="nav-item">
                        <a class="dropdown-item" <?php echo 'href="IndexInicial.php?principal=' . INFORMATIVAS . 'nuestrosEmpleados.php"' ?>>Nuestros
                            Empleados</a>
                    </li>
                    <li class="nav-item">
                        <a class="dropdown-item" <?php echo 'href="IndexInicial.php?principal=' . INFORMATIVAS . 'nuestrosAlmacenes.php"' ?>>Nuestros
                            almacenes</a>
                    </li>
                    <li class="nav-item">
                        <a class="dropdown-item" <?php echo 'href="IndexInicial.php?principal=' . INFORMATIVAS . 'garantia.html"' ?>>Garantía</a>
                    </li>
                    <li>
                        <a class="dropdown-item" <?php echo 'href="IndexInicial.php?principal=' . INFORMATIVAS . 'contacta.php"' ?>>
                            Contacta </a>
                    </li>
                    <li>
                        <a class="dropdown-item" <?php echo 'href="IndexInicial.php?principal=' . INFORMATIVAS . 'dondeEstamos.html"' ?>>
                            Dónde
                            estamos </a>
                    </li>
                    <li>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" <?php echo 'href="IndexInicial.php?principal=' . INFORMATIVAS . 'acercade.php"' ?>>
                            Acerca de
                            nosotros </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false"> <?php echo $nombre ?> </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <?php
                    if (is_null($usuario)) {
                        echo '<li><a class="dropdown-item"
                            href="IndexInicial.php?principal=' . VISTA_USUARIO . 'formInicioSesion.php" >Iniciar
                            Sesión</a>
                    </li>
                    <li><a class="dropdown-item"
                            href="IndexInicial.php?principal=' . VISTA_USUARIO . 'formularioRegistro.php">Registro</a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="IndexInicial.php?principal=' . VISTA_USUARIO . 'carrito.php">Mi cesta</a>
                    </li>';
                    } else {
                        echo '<li>
                        <a class="dropdown-item" href="IndexInicial.php?principal=' . VISTA_USUARIO . 'datosUsuarios.php">Mi cuenta</a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="IndexInicial.php?principal=' . VISTA_USUARIO . 'deseosUsuarios.php">Mi lista de deseos</a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="IndexInicial.php?principal=' . VISTA_USUARIO . 'carrito.php">Mi cesta</a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="IndexInicial.php?principal=' . VISTA_USUARIO . 'pedidosUsuarios.php">Mis pedidos</a></li>
                    <li>
                        <a class="dropdown-item" href="' . INFORMATIVAS . 'cerrarSesion.php">Cerrar
                            Sesión</a>
                    </li>';

                    }
                    ?>

                </ul>
            </li>

        </ul>
    </div>
</nav>

<div class="container p-3">
    <!--contenedor principal-->


    <!-----------Otra barra con submenús, pero hace uso de una hoja de estilos externa (estilos.css)

    https://www.codeply.com/go/ji5ijk6yJ4/bootstrap-4-dropdown-submenu-on-hover-(navbar)
        ------->

    <!--navbar-expand-md para que aparzca el botón de las tres barras horizontales a partir de la tablet, pero no
        antes ni con la tablet-->


    <!--Vamos a programar ahora la parte por debajo de las barras de navegación

    Empezaremos por un contenedor que tendrá dos div's: uno que defina la parte izquierda, y otro que se encargue de la parte derecha
    -->


    <!------------------------Parte derecha de la pantalla (section)     ------------------>

    <div class="p-2">
        <!--Parte derecha de la pantalla-->
        <?php


        if ($isFirstTime) {
            require_once VISTA_ADMIN . "primerAlta.php";
        } elseif (isset($_GET['principal'])) {
            require_once $_GET['principal'];
        } else {
            require_once INFORMATIVAS . "inicio.html";
        }
        ?>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
</div> <!--contenedor principal-->
<footer class="py-3 bg-info">
    <div class="container">
        <p class="m-0 text-center text-white">Cosme José Nieto Pérez</p>
        <p class="m-0 text-right text-dark">IES Castelar(Badajoz)</p>
    </div>

</footer>

</body>
</html> 