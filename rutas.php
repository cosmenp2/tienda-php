<?php

//@define('ROOT_PATH', obtainRoot() . 'ERP');
@define('FOTOS', './interfaz/fotos/');
@define('PERSISTENCIA', './persistencia/');
@define('POJOS', './pojos/');
@define('VISTA_ADMIN', './interfaz/vista_admin/');
@define('VISTA_USUARIO', './interfaz/vista_usuario/');
@define('PDF', './interfaz/pdf/');
@define('LIBTCPDF', './lib/tcpdf/tcpdf.php');
@define('LIBSVGGRAPH', './lib/SVGGraph/autoloader.php');
@define('ICONOS', './iconos/');
@define('LOGO', './logo/logo_castelar.jpg');
@define('INFORMATIVAS', './informativas/');
//
//function obtainRoot()
//{
//    $path = $_SERVER['DOCUMENT_ROOT'];
//    if (sizeof(explode(":", $path)) > 1) {
//        $path = explode(":", $path)[1];
//    } else {
//        $path = explode(":", $path)[0];
//    }
//    return $path;
//}

/*
@define('GESTIONPEDIDOS',ROOT_PATH.'/gestionPedidos');

@define('GESTIONEMPLEADOS',ROOT_PATH.'/gestionEmpleados');
@define('GESTIONPRODUCTOS',ROOT_PATH.'/gestionProductos');*/

/* Este archivo hay que dejarlo en el htdocs (directorio raiz dl servidor)
 y luego cuando se necesite incluir, por ejemplo los archivos Cliente.php y Clientes.php 
en algún archivo, hay que hacer:

include_once ($_SERVER['DOCUMENT_ROOT'].'/rutas.php');
include (POJOS . "Cliente.php");
include (PERSISTENCIA . "Clientes.php");

*/

